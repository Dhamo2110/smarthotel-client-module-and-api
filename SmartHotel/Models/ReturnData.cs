﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartHotelAPI.Models
{
    public class ReturnData
    {
        public bool status;
        public string message;
        public object data;
    }
}