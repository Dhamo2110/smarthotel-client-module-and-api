﻿using System;
using System.Web.Http;
using SmartHotelDataAccessLayer;
using SmartHotelAPI.Models;
using System.Web;
using System.Net.Http;
using System.Net;
using SmartHotel.Controllers;
using System.Globalization;
using System.Data.Entity.Validation;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using appKeys = System.Configuration.ConfigurationManager;

namespace SmartHotelAPI.Controllers
{
    public class SmartHotelAPIController : ApiController
    {
        #region login Logout
        [System.Web.Http.HttpPost]
        public object login()
        {
            string ab = "";
            try
            {
                //string ab="";
            string userid = HttpContext.Current.Request.Params["userid"]; //collection["userid"].ToString();
                string password = HttpContext.Current.Request.Params["password"]; //collection["password"].ToString();
                ReturnData returnObj = new ReturnData();
                if (!string.IsNullOrEmpty(userid))
                {
                   // ab += "a";
                    //returnObj.data = new { usernaid = userid, password =password };
                    //return returnObj;
                    SmartHotelRepository repository = new SmartHotelRepository();
                    //ab += "b";
                    int empid = repository.getEmployeeIdFromUserName(userid);
                    bool status = repository.loginCheck(empid,Encryptdata( password));
                    //ab += "c";
                    if (status )
                    {
                        if (repository.isEmployeeActive(Convert.ToInt32(empid)))
                        {

                            //  ab += "d";
                            returnObj.status = true;
                            returnObj.data = repository.getEmployeeDetails(empid);
                            //ab += "e";
                            returnObj.message = "Login Successfull";
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Username or password"));// + ab + " " + empid);//(empid as Exception).Message+"##"+ (empid as Exception).Source+"##"+ (empid as Exception).StackTrace);
                    }


                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));// +ab);
                }
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "Login");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));// +ab+" "+ex.Message+"##"+ex.Source+"@@"+ex.StackTrace);
            }
            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));
        }

        public ReturnData FailedReturnData(string msg)
        {
            ReturnData retObj = new ReturnData();
            retObj.status = false;
            retObj.data = new { };
            retObj.message = msg;
            return retObj;
        }
        [System.Web.Http.HttpPost]
        public object Logout()
        {
            try
            {
                int userid = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                    ReturnData returnObj = new ReturnData();
                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.Lougout(userid))
                    {
                        returnObj.status = true;
                        returnObj.data = new { };
                        returnObj.message = "Logout Success";
                        return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                    }
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Logout Failed"));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Logout Failed"));
            }
            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Logout Failed"));
        }

        #endregion

        #region TimeCard
        [HttpPost]
        public object TimeCardRequest()
        {
            //string abc = "";
            try
            {
                int employeeId = Convert.ToInt32( HttpContext.Current.Request.Params["employeeId"]);
                int businessId=Convert.ToInt32( HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"],formats, new CultureInfo("en-US"), DateTimeStyles.None);
                string requestType = HttpContext.Current.Request.Params["requestType"];
                string requestTime = HttpContext.Current.Request.Params["requestTime"];
                
                if(employeeId>0 && businessId>0 && !string.IsNullOrEmpty(employeeToken) && requestDate!=null && !string.IsNullOrEmpty(requestType)
                    && !string.IsNullOrEmpty(requestTime))
                {
                    
                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";
                        bool status = false;
                        status=repository.TimeCardRequest(employeeId, businessId, requestDate, requestType, requestTime);
                        //abc += "# d #";
                        if (status)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data =new { };
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                       //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Unable to Update "+requestType));
                        }
                    }
                    else
                    {
                     //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                   // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (DbEntityValidationException e)
            {
                string msg = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    msg+=string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        msg+=string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                ExeptionLoginning.SendErrorToText(e, "TimeCardRequest"+msg);
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data ex "/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex,"");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data ex "/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }
        [HttpPost]
        public object getTimeCardDetails()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);
               
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && requestDate != null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";
                        
                        object tObj = repository.getTimeCardDetails(employeeId, businessId, requestDate);
                        //abc += "# d #";
                        if (tObj!=null)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = tObj;
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }
        [HttpPost]
        public object getLastWeekTimeCardDetails()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && requestDate != null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";

                        object tObj = repository.getLastWeekTimeCardDetails(employeeId, businessId, requestDate);
                        //abc += "# d #";
                        if (tObj != null)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = tObj;
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }
        [HttpPost]
        public object addTimeOffRequest()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string reason = HttpContext.Current.Request.Params["reason"];
                string path = HttpContext.Current.Request.Params["path"];
                string[] formats = { "MM/dd/yyyy hh:mm tt" };
                DateTime StartDate = DateTime.ParseExact(HttpContext.Current.Request.Params["StartDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);
                DateTime EndDate = DateTime.ParseExact(HttpContext.Current.Request.Params["EndDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);
                DateTime CreatedAt = DateTime.ParseExact(HttpContext.Current.Request.Params["CreatedAt"], formats, new CultureInfo("en-US"), DateTimeStyles.None);
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && StartDate!=null && EndDate != null && CreatedAt != null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        bool tObj = repository.addTimeOffRequest(employeeId, businessId, StartDate,EndDate,CreatedAt,reason,path);
                        if (tObj)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = new { };
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Add TimeOffRequest Failed"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object getTimeOffRequest()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {
                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = repository.getTimeOffRequest(employeeId, businessId);
                        if(returnObj.data==null)
                        {
                            returnObj.message = "No data found";
                            returnObj.data =new { };
                        }
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }
        #endregion

        #region user Profile
        public object ChangePassword()
        {
            //string abc = "";
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string oldpassword = HttpContext.Current.Request.Params["oldPassword"]; //collection["password"].ToString();
                string newpassword = HttpContext.Current.Request.Params["newPassword"];

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && !string.IsNullOrEmpty(oldpassword) && !string.IsNullOrEmpty(newpassword))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        bool status = repository.userCheck(employeeId,Encryptdata(oldpassword));
                        //ab += "c";
                        if (status)
                        {
                            if (repository.changePassword(employeeId,Encryptdata(newpassword)))
                            {
                                ReturnData returnObj = new ReturnData();
                                //  ab += "d";
                                returnObj.status = true;
                                returnObj.data = new { };
                                //ab += "e";
                                returnObj.message = "Change password Successfull";
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Unable to update new password"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Wrong old password"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data ex "/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        public object UpdateEmployeeProfile()
        {
            //string abc = "";
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string Name = HttpContext.Current.Request.Params["Name"]; //collection["userid"].ToString();
                string Email = HttpContext.Current.Request.Params["Email"]; //collection["password"].ToString();
                string Phone = HttpContext.Current.Request.Params["Phone"];
                string ImagePath = HttpContext.Current.Request.Params["ImagePath"];

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        
                            if (repository.updateEmployeeProfile(employeeId,Name,Email,Phone,ImagePath))
                            {
                                ReturnData returnObj = new ReturnData();
                                //  ab += "d";
                                returnObj.status = true;
                                returnObj.data = new { };
                                //ab += "e";
                                returnObj.message = "Update Profile Successfull";
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Unable to update user profile"));
                            }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        #endregion

        #region Tasks
        [HttpPost]
        public object getTasksDetails()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && requestDate != null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";

                        object tObj = repository.getTasksDetails(employeeId, businessId, requestDate);
                        //abc += "# d #";
                        if (tObj != null)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = tObj;
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object updateTasksDetails()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                DateTime TaskTime=new DateTime();
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["taskTime"].ToString()))
                {
                    string[] formats = { "MM/dd/yyyy hh:mm:ss tt" };
                    TaskTime = DateTime.ParseExact(HttpContext.Current.Request.Params["taskTime"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                }
                string note = HttpContext.Current.Request.Params["note"];
                string imgpath = HttpContext.Current.Request.Params["image"];
                string Type = HttpContext.Current.Request.Params["type"];
                string Status = HttpContext.Current.Request.Params["status"];
                int TaskId = Convert.ToInt32(HttpContext.Current.Request.Params["taskId"]);
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && !string.IsNullOrEmpty(Type) && !string.IsNullOrEmpty(Status) && TaskId>0)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";
                        
                            object tObj = repository.updateTasksDetails(employeeId, businessId, TaskId, Type, Status, TaskTime,note,imgpath);
                        
                        //abc += "# d #";
                        if (tObj != null)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = tObj;
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object addMainenanceRequest()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                int roomId = Convert.ToInt32(HttpContext.Current.Request.Params["roomId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string note = HttpContext.Current.Request.Params["note"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                DateTime requestDateTime = new DateTime();
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["requestDateTime"].ToString()))
                {
                    string[] formats = { "MM/dd/yyyy hh:mm:ss tt" };
                    requestDateTime = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDateTime"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                }
                string Type = HttpContext.Current.Request.Params["type"];
                string image1 = HttpContext.Current.Request.Params["picture1"];
                string image2 = HttpContext.Current.Request.Params["picture2"];
                string image3 = HttpContext.Current.Request.Params["picture3"];

                if (employeeId > 0 && businessId > 0 && roomId>0 && !string.IsNullOrEmpty(employeeToken) && !string.IsNullOrEmpty(Type)&& requestDateTime!=null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";

                        object tObj = repository.addMainenanceRequest(employeeId, businessId, roomId, requestDateTime, Type, image1, image2,image3, note);
                        //abc += "# d #";
                        if (tObj != null)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = new { };
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object addAlertRequest()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                int roomId = Convert.ToInt32(HttpContext.Current.Request.Params["roomId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string note = HttpContext.Current.Request.Params["note"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                DateTime requestDateTime = new DateTime();
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["requestDateTime"].ToString()))
                {
                    string[] formats = { "MM/dd/yyyy hh:mm:ss tt" };
                    requestDateTime = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDateTime"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                }
                string Type = HttpContext.Current.Request.Params["type"];
                string image1 = HttpContext.Current.Request.Params["picture1"];
                string image2 = HttpContext.Current.Request.Params["picture2"];
                string image3 = HttpContext.Current.Request.Params["picture3"];

                if (employeeId > 0 && businessId > 0 && roomId > 0 && !string.IsNullOrEmpty(employeeToken) && !string.IsNullOrEmpty(Type) && requestDateTime != null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";
                        bool st = false;
                        if (Type.Equals("LOST AND FOUND"))
                        {
                            lostfound addLostFoundObj = new lostfound();
                            addLostFoundObj.BusinessID = businessId;

                            addLostFoundObj.AddedBy = employeeId;
                            addLostFoundObj.CreatedAt = requestDateTime;
                            addLostFoundObj.RoomID = roomId;
                            addLostFoundObj.LostFoundStatusID = repository.getLostAndFoundStatusByStatus("OPEN");
                            addLostFoundObj.PriorityID = repository.getProrityIdFromPriority("LOW");
                            if (!string.IsNullOrEmpty(note))
                                addLostFoundObj.Notes = note;
                            if (!string.IsNullOrEmpty(image1))
                                addLostFoundObj.Picture1 = image1;
                            if (!string.IsNullOrEmpty(image2))
                                addLostFoundObj.Picture2 = image2;
                            if (!string.IsNullOrEmpty(image3))
                                addLostFoundObj.Picture3 = image3;
                            st = repository.addLostFoundRequest(addLostFoundObj);
                        }
                        else
                        {
                            alert addAlertObj = new alert();
                            addAlertObj.BusinessID = businessId;

                            addAlertObj.CreatedBy = employeeId;
                            addAlertObj.CreatedAt = requestDateTime;
                            addAlertObj.RoomId = roomId;
                            if (!string.IsNullOrEmpty(Type))
                                addAlertObj.AlertTypeID = repository.getAlertTypeIdFromAlertType(Type);
                            addAlertObj.AlertStatusID = repository.getAlertStatusIdFromAlertStatus("OPEN");
                            if (!string.IsNullOrEmpty(note))
                                addAlertObj.Notes = note;
                            if (!string.IsNullOrEmpty(image1))
                                addAlertObj.Attachment1 = image1;
                            if (!string.IsNullOrEmpty(image2))
                                addAlertObj.Attachment2 = image2;
                            if (!string.IsNullOrEmpty(image3))
                                addAlertObj.Attachment3 = image3;
                            st = repository.addAlerts(addAlertObj);
                        }
                        //abc += "# d #";
                        if (st)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = new { };
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

       
        #endregion

        #region Laundry
        [HttpPost]
        public object getLaundryDetails()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && requestDate != null)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";

                        object tObj = repository.getLaundryDetails(employeeId, businessId, requestDate);
                        //abc += "# d #";
                        if (tObj != null)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = tObj;
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getLaundryDetails");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object updateLaundryDetails()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                int LaundryId = Convert.ToInt32(HttpContext.Current.Request.Params["LaundryId"]);
                
                //DateTime requestDate= DateTime.Parse(HttpContext.Current.Request.Params["requestDate"]);
                DateTime TaskTime = new DateTime();
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["taskTime"].ToString()))
                {
                    string[] formats = { "MM/dd/yyyy hh:mm:ss tt" };
                    TaskTime = DateTime.ParseExact(HttpContext.Current.Request.Params["taskTime"], formats, new CultureInfo("en-US"), DateTimeStyles.None);

                }
                string Status = HttpContext.Current.Request.Params["status"];
                if (employeeId > 0 && businessId > 0 && LaundryId>0 && !string.IsNullOrEmpty(employeeToken) && !string.IsNullOrEmpty(Status) )
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        //abc += "# c #";

                        bool tObj = repository.updateLaundryDetails(LaundryId,Status, TaskTime);
                        //abc += "# d #";
                        if (tObj)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = tObj;
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            //     abc += "# e #";
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        //   abc += "# f #";
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        #endregion

        #region Schedule
        [HttpPost]
        public object getSchedules()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                int requestMonth = Convert.ToInt32(HttpContext.Current.Request.Params["requestMonth"]);
                int requestYear = Convert.ToInt32(HttpContext.Current.Request.Params["requestYear"]);
                
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken) && requestMonth>0 && requestYear>0)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    //abc += "# b #";
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            //abc += "# c #";

                            object tObj = repository.getScheduleDetails(employeeId, businessId, requestMonth,requestYear);
                            //abc += "# d #";
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                //     abc += "# e #";
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            //   abc += "# f #";
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        } 
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }
        #endregion

        #region Message
        [HttpPost]
        public object getMessageCount()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            object tObj = repository.getEmployeeMessageCounter(businessId, employeeId);
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object getAllmessage()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                int msgemployeeId = Convert.ToInt32(HttpContext.Current.Request.Params["msgEmployeeId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            object tObj = repository.getAllmessageAPI(businessId, employeeId, msgemployeeId);
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object addMessage()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                int msgemployeeId = Convert.ToInt32(HttpContext.Current.Request.Params["msgEmployeeId"]);
                string msgTime = HttpContext.Current.Request.Params["msgTime"];
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string messageText = HttpContext.Current.Request.Params["messageText"];

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            string[] formats = { "MM/dd/yyyy hh:mm:ss tt" };
                            DateTime requestDate = DateTime.ParseExact(msgTime, formats, new CultureInfo("en-US"), DateTimeStyles.None);

                            allmessage addMsgObj = new allmessage();
                            addMsgObj.BusinessID = businessId;

                            addMsgObj.MessageSentByID = employeeId;
                            addMsgObj.CreatedAt = requestDate;
                            addMsgObj.messageText = messageText;
                            addMsgObj.MessageRecievedByID = msgemployeeId;
                            addMsgObj.isRead = 0;
                            bool tObj = repository.addMessage(addMsgObj);
                            if (tObj)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = new { };
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        #endregion

        #region Inspection
        [HttpPost]
        public object getInspectingToList()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];

                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            object tObj = repository.getAllHKMPEmployee(businessId);
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object getInspectionAssignedRooms()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);
                int selectedEmployeeId = Convert.ToInt32(HttpContext.Current.Request.Params["selectedEmployeeId"]);
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            object tObj = repository.getInspectionAssignedRooms(businessId);
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object getInspectionRoomDuties()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                if (employeeId > 0 && businessId > 0 && !string.IsNullOrEmpty(employeeToken))
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId))
                    {
                        if (repository.getEmployeeToken(employeeId).Equals(employeeToken))
                        {
                            object tObj = repository.getAllDutiesForAPI();
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated please contact your hotel manager"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }


        [HttpPost]
        public object addInspection()
        {
            try
            {
                int employeeId = Convert.ToInt32(HttpContext.Current.Request.Params["employeeId"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string employeeToken = HttpContext.Current.Request.Params["employeeToken"];
                string[] formats = { "MM/dd/yyyy hh:mm tt" };
                DateTime requestDate = DateTime.ParseExact(HttpContext.Current.Request.Params["requestDate"], formats, new CultureInfo("en-US"), DateTimeStyles.None);
                int selectedEmployeeId = Convert.ToInt32(HttpContext.Current.Request.Params["selectedEmployeeId"]);
                string inspectedByName = HttpContext.Current.Request.Params["inspectedByName"];
                int roomId = Convert.ToInt32(HttpContext.Current.Request.Params["roomId"]);
                int roomDutyId = Convert.ToInt32(HttpContext.Current.Request.Params["roomDutyId"]);
                float result =float.Parse(HttpContext.Current.Request.Params["result"]);
                string comment = HttpContext.Current.Request.Params["comment"];
                string signPath = HttpContext.Current.Request.Params["signPath"];
                if (employeeId > 0 && businessId > 0 && selectedEmployeeId >0 && !string.IsNullOrEmpty(employeeToken) && requestDate != null &&
                    roomId>0 && roomDutyId>0)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isEmployeeActive(employeeId) && repository.getEmployeeToken(employeeId).Equals(employeeToken))
                    {
                        bool tObj = repository.addInspection(employeeId, businessId, selectedEmployeeId, inspectedByName, requestDate, roomId, roomDutyId, result, comment, signPath);
                        if (tObj)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = new { };
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Add Inspection Failed"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("Invalid Unauthorized"));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }
        #endregion

        #region Guest
        [System.Web.Http.HttpPost]
        public object Guestlogin()
        {
            string ab = "";
            try
            {
                //string ab="";
                string token = HttpContext.Current.Request.Params["Token"]; //collection["userid"].ToString();
                ReturnData returnObj = new ReturnData();
                if (!string.IsNullOrEmpty(token))
                {
                    // ab += "a";
                    //returnObj.data = new { usernaid = userid, password =password };
                    //return returnObj;
                    SmartHotelRepository repository = new SmartHotelRepository();
                    //ab += "b";
                    guestbusiness guestObj = repository.GuestLogin(token);
                    //ab += "c";
                    if (guestObj!=null)
                    {
                            returnObj.status = true;
                            returnObj.data = new
                            {
                                GuestID=guestObj.GuestID,
                                GuestBusinessID=guestObj.GuestBusinessID,
                                BusinessID = guestObj.BusinessID,
                                Name =guestObj.guest.Name,
                                Phone=guestObj.guest.phone,
                                Email=guestObj.guest.Email,
                                RoomID=guestObj.RoomID,
                                RoomNumber = guestObj.room.RoomNumber,
                                City = guestObj.business.City,
                                BusinessName = guestObj.business.BusinessName,
                                StayDateStart = guestObj.StayDateStart!=null?((DateTime) guestObj.StayDateStart).ToString("MM/dd/yyyy hh:mm tt") :"",
                                StayDateEnd= guestObj.StayDateEnd != null ? ((DateTime)guestObj.StayDateEnd).ToString("MM/dd/yyyy hh:mm tt") : "",
                                Note=guestObj.Note
                            };
                            //ab += "e";
                            returnObj.message = "Login Successfull";
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);                       
                    }
                    else
                    {
                        Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Token"));// + ab + " " + empid);//(empid as Exception).Message+"##"+ (empid as Exception).Source+"##"+ (empid as Exception).StackTrace);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));// +ab);
                }
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "Guest Login");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));// +ab+" "+ex.Message+"##"+ex.Source+"@@"+ex.StackTrace);
            }
            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));
        }

        [HttpPost]
        public object getAllGuestmessage()
        {
            try
            {
                int id = Convert.ToInt32(HttpContext.Current.Request.Params["GuestBusinessID"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);

                if (id > 0 && businessId > 0)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isGuestActive(id, businessId))
                    {
                            object tObj = repository.getAllGuestmessageAPI(businessId, id);
                            if (tObj != null)
                            {
                                ReturnData returnObj = new ReturnData();
                                returnObj.status = true;
                                returnObj.message = "success";
                                returnObj.data = tObj;
                                return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                            }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated or deleted by Hotel management"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object addGuestmessage()
        {
            try
            {
                int id = Convert.ToInt32(HttpContext.Current.Request.Params["GuestBusinessID"]);
                int businessId = Convert.ToInt32(HttpContext.Current.Request.Params["businessId"]);
                string msgTime = HttpContext.Current.Request.Params["msgTime"];
                string messageText = HttpContext.Current.Request.Params["messageText"];
                if (id > 0 && businessId > 0)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    if (repository.isGuestActive(id, businessId))
                    {
                        string[] formats = { "MM/dd/yyyy hh:mm:ss tt" };
                        DateTime requestDate = DateTime.ParseExact(msgTime, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                        guestmessage addMsgObj = new guestmessage();
                        addMsgObj.BusinessID = Convert.ToInt32(businessId);

                        addMsgObj.MessageSentByID = Convert.ToInt32(id);
                        addMsgObj.CreatedAt = requestDate;
                        addMsgObj.messageText = messageText;
                        addMsgObj.isRead = 0;
                        addMsgObj.messageTtype = "from";
                        bool st = repository.addGuestMessage(addMsgObj);
                        if (st)
                        {
                            ReturnData returnObj = new ReturnData();
                            returnObj.status = true;
                            returnObj.message = "success";
                            returnObj.data = new { };
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, FailedReturnData("You have been deactivated or deleted by Hotel management"));
                    }
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }

        [HttpPost]
        public object getGuestHistory()
        {
            string ab = "";
            try
            {
                //string ab="";
                string guestId = HttpContext.Current.Request.Params["guestId"]; //collection["userid"].ToString();
                ReturnData returnObj = new ReturnData();
                if (!string.IsNullOrEmpty(guestId))
                {
                    // ab += "a";
                    //returnObj.data = new { usernaid = userid, password =password };
                    //return returnObj;
                    SmartHotelRepository repository = new SmartHotelRepository();
                    
                        returnObj.status = true;
                        returnObj.data = repository.getGuestHistory(Convert.ToInt32(guestId));
                        //ab += "e";
                        returnObj.message = "success";
                        return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                    
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));// +ab);
                }
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "Guest Login");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));// +ab+" "+ex.Message+"##"+ex.Source+"@@"+ex.StackTrace);
            }
            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Login Failed"));
        }

        [HttpPost]
        public object getAllAmenties()
        {
            string ab = "";
            try
            {
                //string ab="";
                string businessId = HttpContext.Current.Request.Params["businessId"]; //collection["userid"].ToString();
                ReturnData returnObj = new ReturnData();
                SmartHotelRepository repository = new SmartHotelRepository();
                if (!string.IsNullOrEmpty(businessId))
                {
                    // ab += "a";
                    //returnObj.data = new { usernaid = userid, password =password };
                    //return returnObj;

                    object tObj = repository.getAllAmentiesAPI(Convert.ToInt32(businessId));
                    if (tObj != null)
                    {
                        returnObj.status = true;
                        returnObj.message = "success";
                        returnObj.data = tObj;
                        return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("No Data Found"));
                    }
                    
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));// +ab);
                }
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getAllAmenties");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));// +ab+" "+ex.Message+"##"+ex.Source+"@@"+ex.StackTrace);
            }
            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
        }

        [HttpPost]
        public object UpdateGuestProfile()
        {
            //string abc = "";
            try
            {
                int guestId = Convert.ToInt32(HttpContext.Current.Request.Params["guestId"]);
                string Name = HttpContext.Current.Request.Params["Name"]; //collection["userid"].ToString();
                string Email = HttpContext.Current.Request.Params["Email"]; //collection["password"].ToString();
                string Phone = HttpContext.Current.Request.Params["Phone"];

                if (guestId > 0)
                {

                    SmartHotelRepository repository = new SmartHotelRepository();
                    
                        if (repository.updateGuestProfile(guestId, Name, Email, Phone))
                        {
                            ReturnData returnObj = new ReturnData();
                            //  ab += "d";
                            returnObj.status = true;
                            returnObj.data = new { };
                            //ab += "e";
                            returnObj.message = "Update Profile Successfull";
                            return Request.CreateResponse(HttpStatusCode.OK, returnObj);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Unable to update guest profile"));
                        }
                    
                }
                else
                {
                    // abc += "# g #";
                    return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"));
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "UpdateGuestProfile");
                return Request.CreateResponse(HttpStatusCode.OK, FailedReturnData("Invalid Request Data"/*+abc+
                    "#Data#" + ex.Data+ "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite */));
            }
        }


        #endregion

        public static string Encryptdata(string password)
        {
            return Cipher.Encrypt(password, appKeys.AppSettings["CipherKey"]);

        }
        public static string Decryptdata(string encryptpwd)
        {
            return Cipher.Decrypt(encryptpwd, appKeys.AppSettings["CipherKey"]);
        }
    }
    public static class Cipher
    {
        /// <summary>
        /// Encrypt a string.
        /// </summary>
        /// <param name="plainText">String to be encrypted</param>
        /// <param name="password">Password</param>
        public static string Encrypt(string plainText, string password)
        {
            if (plainText == null)
            {
                return null;
            }

            if (password == null)
            {
                password = String.Empty;
            }

            // Get the bytes of the string
            var bytesToBeEncrypted = Encoding.UTF8.GetBytes(plainText);
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesEncrypted = Cipher.Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(bytesEncrypted);
        }

        /// <summary>
        /// Decrypt a string.
        /// </summary>
        /// <param name="encryptedText">String to be decrypted</param>
        /// <param name="password">Password used during encryption</param>
        /// <exception cref="FormatException"></exception>
        public static string Decrypt(string encryptedText, string password)
        {
            if (encryptedText == null)
            {
                return null;
            }

            if (password == null)
            {
                password = String.Empty;
            }

            // Get the bytes of the string
            var bytesToBeDecrypted = Convert.FromBase64String(encryptedText);
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesDecrypted = Cipher.Decrypt(bytesToBeDecrypted, passwordBytes);

            return Encoding.UTF8.GetString(bytesDecrypted);
        }

        private static byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }

                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        private static byte[] Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }

                    decryptedBytes = ms.ToArray();
                }
            }

           return decryptedBytes;
        }
    }
}
