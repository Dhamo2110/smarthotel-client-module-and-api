﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartHotelClient.Models
{
    public class RoomLists
    {
        public int RoomListID { get; set; }
        public int RoomID { get; set; }
        public int RoomNumber { get; set; }
        public int RoomStatusID { get; set; }
        public int RoomDutiesID { get; set; }
        public int PriorityID { get; set; }
        public int EmployeeID { get; set; }
        public string Notes { get; set; }
        public Nullable<System.TimeSpan> StartTime { get; set; }
        public Nullable<System.TimeSpan> EndTime { get; set; }
        public Nullable<float> TotalTime { get; set; }
        public bool IsAssigned { get; set; }
        public Nullable<int> HousekeeperID { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public int BusinessID { get; set; }
        public Nullable<int> HeadHousekeeperID { get; set; }
    }
}