﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartHotelClient.Models
{
    public class business
    {
        public int BusinessID { get; set; }
        public int BusinessNumber { get; set; }
        public Nullable<int> BusinessGroupNumber { get; set; }
        public string BusinessName { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessEmail { get; set; }
        public string ManagerName { get; set; }
        public string ManagerPhone { get; set; }
        public string ManagerEmail { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public string PersonalNotes { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> TotalRooms { get; set; }
        public string Status { get; set; }
        public string PaymentPlan { get; set; }
        public string PricingType { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public sbyte IsVerified { get; set; }
        public string BusinessType { get; set; }
    }
}