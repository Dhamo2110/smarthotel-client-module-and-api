﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartHotelClient.Models
{
    public class timecard
    {
        public int TimeCardID { get; set; }
        public int BusinessID { get; set; }
        public int EmployeeID { get; set; }
        public System.DateTime WorkDate { get; set; }
        public Nullable<System.TimeSpan> ClockInTime { get; set; }
        public Nullable<System.TimeSpan> LunchInTime { get; set; }
        public Nullable<System.TimeSpan> LunchOutTime { get; set; }
        public Nullable<System.TimeSpan> ClockOutTime { get; set; }
        public string TotalLunchTime { get; set; }
        public string TotalWorkedHour { get; set; }
        public string Notes { get; set; }

        public virtual business business { get; set; }
        public virtual employee employee { get; set; }
    }
}