﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartHotelClient.Models
{
    public class employee
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int JobTitleID { get; set; }
        public string JobTitle { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int BusinessID { get; set; }
        public string IsActive { get; set; }
    }
}