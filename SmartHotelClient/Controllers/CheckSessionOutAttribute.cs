﻿using SmartHotelDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SmartHotelClient.Controllers
{
    public class CheckSessionOutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SmartHotelRepository repObj = new SmartHotelRepository();
            var context = filterContext.HttpContext;
            
            

            if (context.Session["BusinessID"] == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.ClearContent();
                    filterContext.HttpContext.Items["AjaxPermissionDenied"] = true;
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/Client/Login");
                    //return;
                }
                
            }
            else
            {
                string modules = "";
                foreach (var item in (List<string>)repObj.getBusinessModuleName(Convert.ToInt32(context.Session["BusinessID"])))
                {
                    modules += item + ";";
                }
                context.Session["SubscribedModules"] = modules;
            }
        }
    }
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }
}