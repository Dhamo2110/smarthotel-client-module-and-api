﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SmartHotelClient.Controllers
{
    public class ExceptionLogining
    {

            private static String ErrorlineNo, Errormsg, extype, exurl, hostIp, ErrorLocation, HostAdd;

            public static void SendErrorToText(Exception ex ,string usermsg)
            {
                var line = Environment.NewLine + Environment.NewLine;

                ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                Errormsg = ex.GetType().Name.ToString();
                extype = ex.GetType().ToString();
                //exurl = context.Current.Request.Url.ToString();
                ErrorLocation = ex.Message.ToString();

                try
                {
                    string filepath = "C://SmartHotel/Client/Log";
                //Path.Combine(Server.MapPath("~/Content/img/EmployeeImages"),
                //Path.GetFileName("log.txt"))  //Text File Path

                if (!Directory.Exists(filepath))
                    {
                        Directory.CreateDirectory(filepath);

                    }
                    filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + ".txt";   //Text File Name
                    if (!File.Exists(filepath))
                    {


                        File.Create(filepath).Dispose();

                    }
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Error Line No :" + " " + ErrorlineNo + line + "User Message:" + " " + usermsg + line + "Error Message:" + " " + Errormsg + line + "Error InnerException:" + " " + ex.InnerException + line + "Error Source:" + " " + ex.Source + line + "Error TargetSite:" + " " + ex.TargetSite + line + "Exception Type:" + " " + extype + line + "Error Location :" + " " + ErrorLocation + line  + "User Host IP:" + " " + hostIp + line;
                        sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                        sw.WriteLine("-------------------------------------------------------------------------------------");
                        sw.WriteLine(line);
                        sw.WriteLine(error);
                        sw.WriteLine("--------------------------------*End*------------------------------------------");
                        sw.WriteLine(line);
                        sw.Flush();
                        sw.Close();

                    }

                }
                catch (Exception e)
                {
                    e.ToString();

                }
            }
        public static void SendUserMEssageToText( string usermsg)
        {
            var line = Environment.NewLine + Environment.NewLine;
            
            try
            {
                string filepath = "C://SmartHotel/Client/Log";
                //Path.Combine(Server.MapPath("~/Content/img/EmployeeImages"),
                //Path.GetFileName("log.txt"))  //Text File Path

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);

                }
                filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + ".txt";   //Text File Name
                if (!File.Exists(filepath))
                {


                    File.Create(filepath).Dispose();

                }
                using (StreamWriter sw = File.AppendText(filepath))
                {
                   
                        sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                        sw.WriteLine("-------------------------------------------------------------------------------------");
                        sw.WriteLine("User Message:" + " " + usermsg);
                        sw.WriteLine("--------------------------------*End*------------------------------------------");
                        sw.WriteLine(line);
                        sw.Flush();
                        sw.Close();

                    }

                }
                
            catch (Exception e)
            {
                e.ToString();

            }
        }
    }
}