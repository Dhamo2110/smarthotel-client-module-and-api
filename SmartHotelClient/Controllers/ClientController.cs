﻿using SmartHotelDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.IO;
using System.Globalization;
using System.Data;
using ClosedXML.Excel;
using System.Net.Mail;
using System.Net;
using appKeys = System.Configuration.ConfigurationManager;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Web.Http.Cors;
using System.Security.Cryptography;
using System.Text;

namespace SmartHotelClient.Controllers
{
    [EnableCors(origins: "http://34.212.35.235:88/", headers: "*", methods: "*")]
    public class ClientController : Controller
    {
        // GET: Client


        #region DeshBoard
        [CheckSessionOut]
        public ActionResult Index()
        {
            if (!"OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))
            {
                return HttpNotFound();
            }
                return View();
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult getPresentEmployee()
        {
            try
            {
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getAllTiemCards(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        EmployeeName = t1.employee.Name,
                        JobTitle = t1.employee.jobtitle.JobTitle1,
                        color= getJobTitleColor( t1.employee.jobtitle.JobTitle1),
                        curruntStatus = t1.ClockOutTime != null ? "ClockOut" : t1.LunchOutTime != null ? "LunchOut" : t1.LunchInTime != null ? "LunchIn" : t1.ClockInTime != null ? "ClockIn" : "Unknown",
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getJobTitleColor(string title)
        {
            string color;

            if (title.Equals("FRONTDESK")) {
                color = "label label-info";
            }
            else if (title.Equals("HEAD HOUSEKEEPER")) {

                color = "label label-success";
            }
            else if (title.Equals("MAINTENANCE PERSON")) {
                color = "label label-warning";
            }
            else if (title.Equals("LAUNDRY PERSON")) {
                color = "label label-danger";
            }
            else if (title.Equals("HOUSEKEEPER")) {
                color = "label label-primary";
            }
            else if (title.Equals("MANAGER")) {
                color = "label label-default";
            } else {
                color = "label label-default";
            }
            return color;
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult updateDashboardCounts()
        {
            try
            {
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                return Json(new
                {
                allAssignedRoomCount = repObj.getAllCreatedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).Count(),
                allCompletedRoomCount=repObj.getAllRoomListByStatus(Convert.ToInt32(Session["BusinessID"]), repObj.getRoomListStatusIdByStatus("COMPLETED"),requestDate).Count(),
                allEmployeeCount = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).Count(),
                allAlertCount = repObj.getAllAlerts(Convert.ToInt32(Session["BusinessID"])).Count(),
                allInspectionCount = repObj.getAllInspection(Convert.ToInt32(Session["BusinessID"])).Count(),
                allInspectionCount30 = repObj.getAllInspection(Convert.ToInt32(Session["BusinessID"])).Where(b=>b.CreatedAt>=requestDate.AddDays(-30)).Count(),
                allMaintenanceCount = repObj.getAllMaintenanceRooms(Convert.ToInt32(Session["BusinessID"])).Where(b=>b.maintenancestatu.Status.Equals("OPEN")).Count(),
                    allMaintenanceCount30 = repObj.getAllMaintenanceRooms(Convert.ToInt32(Session["BusinessID"])).Where(b => b.CreatedAt >= requestDate.AddDays(-30)).Count(),
                    allLostFoundCount = repObj.getAllLostFoundRequest(Convert.ToInt32(Session["BusinessID"])).Where(b=>b.lostfoundstatu.Status.Equals("OPEN")).Count(),
                    allLostFoundCount30 = repObj.getAllLostFoundRequest(Convert.ToInt32(Session["BusinessID"])).Where(b => b.CreatedAt >= requestDate.AddDays(-30)).Count()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ec)
            {
                return null;
            }
        }
        #endregion

        #region Common

        [HttpPost]
        [CheckSessionOut]
        public JsonResult updateAllCounts()
        {
            try
            {
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                return Json(new
                {
                    TotalClockInUser = repObj.getTotalClockedINCount(Convert.ToInt32(Session["BusinessID"]), requestDate),
                    TotalTimeOffRequests = repObj.getTotalTimeOffRequests(Convert.ToInt32(Session["BusinessID"])),
                    TotalUnreadMessage= repObj.getTotalUnreadMessageCount(Convert.ToInt32(Session["BusinessID"]), Convert.ToInt32(Session["EmployeeId"])),
                    TotalGUnreadMessage= repObj.getGuestMessageTotalUnreadCount(Convert.ToInt32(Session["BusinessID"]))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ec)
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult SetUTC(string dateString)
        {
            Session["reduestedDateTime"] = dateString;
            Session["reduestedDate"] = dateString.Split(' ')[0].Trim();

            return Json(null);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            try
            {
                string username = collection["Username"].ToString().Trim();
                string password = collection["Password"].ToString().Trim();
                if(!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    SmartHotelRepository repObj = new SmartHotelRepository();
                    employee emp = repObj.login(username,Encryptdata(password));
                    if(emp!=null)
                    {
                        if ("OWNER;MANAGER;FRONTDESK;".Contains(emp.jobtitle.JobTitle1))
                        {
                            Session["BusinessID"] = emp.BusinessID;
                            Session["EmployeeId"] = emp.EmployeeID;
                            Session["EmployeeName"] = emp.Name;
                            Session["EmployeeSince"] = ((DateTime)emp.CreatedAt).ToString("MMM") + ". " + ((DateTime)emp.CreatedAt).ToString("yyyy");
                            Session["JobTitle"] = emp.jobtitle.JobTitle1;
                            Session["ImagePath"] = emp.ImagePath!=null?emp.ImagePath: "/Content/Alphabets/"+emp.Name.Substring(0,1).ToUpper()+".jpg";
                            Session["Username"] = emp.Username;
                            Session["Password"] =Decryptdata( emp.Password);
                            Session["Email"] = emp.Email;
                            Session["Phone"] = emp.Phone;
                            Session["BusinessName"] = emp.business.BusinessName;
                            Session["BusinessNumber"] = emp.business.BusinessNumber;
                            Session["City"] = emp.business.City;
                            string modules = "";
                            foreach (var item in (List<string>)repObj.getBusinessModuleName(emp.BusinessID))
                            {
                                modules += item + ";";
                            }
                            Session["SubscribedModules"] = modules;
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ViewBag.Message = "You are not eligible to view client module";
                            ViewBag.Username = username;
                            return View();
                        }
                    }
                    else
                    {
                        if(repObj.getEmployeeIdFromUserName(username)>0)
                        {
                            ViewBag.Message = "Enter Correct Password";
                            ViewBag.Username = username;
                            return View();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            @ViewBag.Message = "Enter Correct Username and Password";
            return View();
        }

        [HttpPost]
        public ActionResult LoginSuperAdmin(FormCollection collection)
        {
            try
            {
                string username = collection["ClientUsername"].ToString().Trim();
                string password = collection["ClientPassword"].ToString().Trim();
                string businessId = collection["ClientbusinessId"].ToString().Trim();
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(businessId))
                {
                    SmartHotelRepository repObj = new SmartHotelRepository();
                    
                    employee emp = repObj.getOwnerOfBusiness(Convert.ToInt32(businessId));
                    if (emp != null)
                    {
                        if ("OWNER;MANAGER;FRONTDESK;".Contains(emp.jobtitle.JobTitle1))
                        {
                            Session["BusinessID"] = emp.BusinessID;
                            Session["EmployeeId"] = emp.EmployeeID;
                            Session["EmployeeName"] = emp.Name;
                            Session["EmployeeSince"] = ((DateTime)emp.CreatedAt).ToString("MMM") + ". " + ((DateTime)emp.CreatedAt).ToString("yyyy");
                            Session["JobTitle"] = emp.jobtitle.JobTitle1;
                            Session["ImagePath"] = emp.ImagePath != null ? emp.ImagePath : "/Content/Alphabets/" + emp.Name.Substring(0, 1).ToUpper() + ".jpg";
                            Session["Username"] = emp.Username;
                            Session["Password"] = Decryptdata(emp.Password);
                            Session["Email"] = emp.Email;
                            Session["Phone"] = emp.Phone;
                            string modules = "";
                            foreach (var item in (List<string>)repObj.getBusinessModuleName(emp.BusinessID))
                            {
                                modules += item + ";";
                            }
                            Session["SubscribedModules"] = modules;
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ViewBag.Message = "You are not eligible to view client module";
                            ViewBag.Username = username;
                            return View();
                        }
                    }
                    else
                    {
                        if (repObj.getEmployeeIdFromUserName(username) > 0)
                        {
                            ViewBag.Message = "Enter Correct Password";
                            ViewBag.Username = username;
                            return View();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            @ViewBag.Message = "Enter Correct Username and Password";
            return View();
        }


        public ActionResult Logout()
        {
            try
            {

                Session["BusinessID"] = null;
                Session["EmployeeId"] = null;
                Session["EmployeeName"] = null;
                Session["EmployeeSince"] = null;
                Session["JobTitle"] = null;
                Session["ImagePath"] = null;
                Session["SubscribedModules"] = null;
                Session["reduestedDate"] = null;
                return RedirectToAction("Login");

            }
            catch (Exception ex)
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult editProfile(string id, string name, string phone, string email, string username, string password)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                int empid = Convert.ToInt32(id);
                employee editemp = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).Where(e => e.EmployeeID == empid).FirstOrDefault<employee>();
                if (!string.IsNullOrEmpty(name))
                    editemp.Name = name.Trim();
                if (!string.IsNullOrEmpty(phone))
                    editemp.Phone = phone.Trim();
                if (!string.IsNullOrEmpty(email))
                    editemp.Email = email.Trim();
                if (!string.IsNullOrEmpty(username))
                    editemp.Username = username.Trim();
                if (!string.IsNullOrEmpty(password))
                {
                    if (!editemp.Password.Equals(password.Trim()))
                    {
                        editemp.Password = Encryptdata(password.Trim());
                    }
                }
                //if (!string.IsNullOrEmpty(collection["editIsActive"].Trim()))
                //    editemp.isActive =collection["editIsActive"].Trim();
                if(repObj.updateEmployee(editemp))
                {
                    Session["Username"] = editemp.Username;
                    Session["Password"] = Decryptdata(editemp.Password);
                    Session["Email"] = editemp.Email;
                    Session["Phone"] = editemp.Phone;
                    Session["EmployeeName"] = editemp.Name;
                    return Json(1);
                }
                return Json(2);
            }
            catch (Exception ex)
            {
                return Json(2);
            }
        }

        [HttpGet]
        public ActionResult resetPassword()
        {
            return View();
        }

        public string FormateMail(string msg)
        {
            try
            {
                return "<center><img  src = \""+ appKeys.AppSettings["AdminServerURL"]+"/Content/img/smarthotellogo.png\" > </center>" +
"<br /><br />" +
"Hi,<br /><br />" +
msg +
"<br /><br />" +
"Thanks,<br />" +
"Phone : 3233745055 <br />" +
"Email : Support@BSmartHotel.com <br />" +
"Website : www.BSmartHotel.com " +
" <br/> <br/> <table> <tr>" +
 "<td rowspan =\"2\">" +
"Follow us :" +
"</td >" +
"<td ><a href=\""+ appKeys.AppSettings["FacebookLink"] + "\"><img  src = \"" + appKeys.AppSettings["AdminServerURL"] + "/Content/img/facebook.png\" placeholder=\"facebook\"></a></td>" +
   "<td ><a href=\"" + appKeys.AppSettings["TwitterLink"] + "\"><img  src = \"" + appKeys.AppSettings["AdminServerURL"] + "/Content/img/twitter.png\" ></a></td>" +
      "<td ><a href=\"" + appKeys.AppSettings["InstaLink"] + "\"><img  src = \"" + appKeys.AppSettings["AdminServerURL"] + "/Content/img/insta.png\" ></a></td>" +
         "<td ><a href=\"" + appKeys.AppSettings["YoutubeLink"] + "\"><img  src = \"" + appKeys.AppSettings["AdminServerURL"] + "/Content/img/youtube.png\" ></a></td> ";
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpGet]
        public ActionResult resetPasswordFromLink(string id)
        {
            try
            {
                if(!string.IsNullOrEmpty(id))
                {
                    SmartHotelRepository repObj = new SmartHotelRepository();
                    employee emp = repObj.getEmployeeFromResetLink(id);
                    if(emp!=null)
                    {
                        ViewBag.link = id;
                        ViewBag.empid = emp.EmployeeID;
                        return View();
                    }
                    else
                    {
                        return HttpNotFound();
                    }
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "resetPasswordFromLink");
            }
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult resetPasswordFromLink(FormCollection collection)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                int empid =Convert.ToInt32(collection["empid"].ToString().Trim());
                string emplink = collection["emplink"].ToString().Trim();
                string password = collection["addPassword"].ToString().Trim();
                if (empid > 0 && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(emplink))
                {
                    employee editemp = repObj.getEmployeeFromEmpId(empid);
                    if (editemp.resetLink.Equals(emplink))
                    {
                        editemp.Password =Encryptdata( password.Trim());
                        editemp.resetLink = null;
                        if (repObj.updateEmployee(editemp))
                        {
                            return RedirectToAction("Login");
                        }
                        else
                        {
                            return RedirectToAction("resetPasswordFromLink&id="+emplink);
                        }
                    }
                    else
                    {
                        return RedirectToAction("resetPasswordFromLink&id=" + emplink);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "resetPasswordFromLink");
            }
            return View();
        }


        [HttpPost]
        public ActionResult resetPassword(FormCollection collection)
        {
            try
            {
                string username = collection["enteredusername"].ToString().Trim();
                if (!string.IsNullOrEmpty(username))
                {
                    SmartHotelRepository repObj = new SmartHotelRepository();
                    int empid = repObj.getEmployeeIdFromUserName(username);
                    if (empid >0)
                    {
                        if (repObj.isEmployeeActive(empid))
                        {
                            employee emp = repObj.getEmployeeFromEmpId(empid);

                            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                            byte[] key = Guid.NewGuid().ToByteArray();
                            string token = Convert.ToBase64String(time.Concat(key).ToArray()).Replace('+', 'a').Replace('&', 'a').Replace('=', 'a');
                            string link = FormateMail("Please click on below to change password. Link: " + appKeys.AppSettings["ClientServerURL"] + "/Client/resetPasswordFromLink/?id=" + token);

                            //string link = "Registration link: http://localhost:50243/Admin/RegisterBusiness/?id=" + token;
                            emp.resetLink = token;
                            if(repObj.updateEmployee(emp))
                            {
                                string body = link;
                                MailMessage mail = new MailMessage();

                                var senderEmail = new MailAddress(appKeys.AppSettings["MailFromAddress"], "Gamil");
                                mail.To.Add(new MailAddress(emp.Email));
                                mail.From = new MailAddress(appKeys.AppSettings["MailFromAddress"]);

                                var password = appKeys.AppSettings["MailPassword"];
                                var smtp = new SmtpClient
                                {
                                    Host = "smtp.gmail.com",
                                    Port = 587,
                                    EnableSsl = true,
                                    DeliveryMethod = SmtpDeliveryMethod.Network,
                                    UseDefaultCredentials = false,
                                    Credentials = new NetworkCredential(senderEmail.Address, password)
                                };
                                mail.Subject = "Reset Password Link";
                                mail.Body = body;
                                mail.IsBodyHtml = true;
                                {
                                    smtp.Send(mail);
                                }
                                ViewBag.Message = "Please check your register email for reset password link";
                                return View();
                            }
                            else
                            {
                                ViewBag.Message = "Something went wrong please try again";
                                return View();                   
                            }
                            
                        }
                        else
                        {
                            ViewBag.Message = "You have been deactivated please contact your hotel manager";
                            //ViewBag.Username = username;
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Enter Correct Username";
                            return View();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "resetPassword");
            }
            ViewBag.Message = "Enter Correct Username";
            return View();
        }



        [HttpPost]
        [AllowAnonymous]
        [AllowCrossSiteJsonAttribute]
        public object ContactUsEmail(string name, string email, string phoneno, string businessName, string message)
        {
            try
            {
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(phoneno))
                {
                    MailMessage mail = new MailMessage();
                    var senderEmail = new MailAddress(appKeys.AppSettings["MailFromAddress"], "Gamil");
                    mail.To.Add(new MailAddress(appKeys.AppSettings["MailToAddress"]));
                    mail.From = new MailAddress(appKeys.AppSettings["MailFromAddress"]);
                    var password = appKeys.AppSettings["MailPassword"];
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)
                    };
                    mail.Subject = "Contact US";
                    mail.Body = FormateMail("Name : " + name + "<br/>" + "Email : " + email + "<br/>" + "Phone No : " + phoneno + "<br/>" + "Business Name : " + businessName + "<br/>" + "Message : " + message + "<br/>");
                    mail.IsBodyHtml = true;
                    {
                        smtp.Send(mail);

                    }
                    return Json(1);
                }
                else
                {
                    // abc += "# g #";
                    return Json(0);
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "ContactUsEmail name + " + name + " email " + email + " phoneno" + phoneno);
                return Json(0);
            }
        }

        [CheckSessionOut]
        public ActionResult FAQs()
        {
            return View();
        }

        [CheckSessionOut]
        public ActionResult Help()
        {
            return View();
        }

        [CheckSessionOut]
        public ActionResult Support()
        {
            return View();
        }
        #endregion

        #region TimeCards details
        [CheckSessionOut]
        public ActionResult TimeCard()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("TIME CLOCK") && ("OWNER;MANAGER;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            return View();
        }
        [HttpGet]
        [CheckSessionOut]
        public JsonResult getTimeCards()
        {
            try
            {
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getAllTiemCards(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        TimeCardID = t1.TimeCardID,
                        BusinessID = t1.BusinessID,
                        EmployeeID = t1.EmployeeID,
                        EmployeeName = repObj.getEmployeeNameFromEmpId(t1.EmployeeID).ToString(),
                        WorkDate = t1.WorkDate.ToString("MM/dd/yyyy"),
                        ClockInTime = ConvertTOAMPM(t1.ClockInTime),
                        LunchInTime = ConvertTOAMPM(t1.LunchInTime),
                        LunchOutTime = ConvertTOAMPM(t1.LunchOutTime),
                        ClockOutTime = ConvertTOAMPM(t1.ClockOutTime),
                        TotalLunchTime = string.IsNullOrEmpty(t1.TotalLunchTime)?"":Math.Round(Convert.ToDouble(t1.TotalLunchTime),0)+" Minute",
                        TotalWorkedHour = string.IsNullOrEmpty(t1.TotalWorkedHour) ? "" : Math.Round(Convert.ToDouble(t1.TotalWorkedHour), 1) + " Hours",
                        Notes = t1.Notes
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult editTimeCards(string id,string clockintime,string clockouttime,string lunchintime,string lunchouttime,string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                repObj.editTimeCards(id, clockintime, clockouttime, lunchintime, lunchouttime,  note);
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(2);
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult editTimeCardNote(string id,  string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                repObj.editTimeCardNotes(id, note);
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(2);
            }
        }
        public string ConvertTOAMPM(DateTime? t)
        {
            if (t != null)
            {
                DateTime time = (DateTime)t;
                return time.ToString("MM/dd/yyyy hh: mm tt");
            }
            return "";
        }
        public string ConvertTOAMPM(TimeSpan? t)
        {
            if (t != null)
            {
                DateTime time = DateTime.Today.Add((TimeSpan)t);
                return time.ToString("hh:mm tt");
            }
            return "";
        }
        [HttpGet]
        [CheckSessionOut]
        public JsonResult getTimeCardFromID(int id)
            {
            SmartHotelRepository repObj = new SmartHotelRepository();
            object ct = repObj.getTimeCardFromID(id);
            var ret = Json(ct, JsonRequestBehavior.AllowGet);
            return ret;
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteTimeCard(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                repObj.deleteTimeCard(id);
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(2);
            }
        }
        public ActionResult ExpertToExcelTimeCard(FormCollection collection)
        {
            try
            {
                string[] format1 = { "yyyy-MM-dd" };
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (!string.IsNullOrEmpty(collection["startDateExcel"]))
                    startDate = DateTime.ParseExact(collection["startDateExcel"], format1, new CultureInfo("en-US"), DateTimeStyles.None);
                if (!string.IsNullOrEmpty(collection["endDateExcel"]))
                    endDate = DateTime.ParseExact(collection["endDateExcel"], format1, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                DataTable dt = new DataTable("TimeCard");
                dt.Columns.AddRange(new DataColumn[9] {
new DataColumn("EmployeeName"),
new DataColumn("WorkDate"),
new DataColumn("ClockInTime"),
new DataColumn("LunchInTime"),
new DataColumn("LunchOutTime"),
new DataColumn("ClockOutTime"),
new DataColumn("TotalLunchTime"),
new DataColumn("TotalWorkedHour"),
new DataColumn("Notes")
            });
                foreach (var t1 in repObj.getAllTiemCardsFromStart(Convert.ToInt32(Session["BusinessID"]), startDate,endDate))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
t1.employee.Name.ToString(),
t1.WorkDate.ToString("MM/dd/yyyy"),
ConvertTOAMPM(t1.ClockInTime),
ConvertTOAMPM(t1.LunchInTime),
ConvertTOAMPM(t1.LunchOutTime),
ConvertTOAMPM(t1.ClockOutTime),
string.IsNullOrEmpty(t1.TotalLunchTime) ? "" : Math.Round(Convert.ToDouble(t1.TotalLunchTime), 0) + "Minute",
string.IsNullOrEmpty(t1.TotalWorkedHour) ? "" : Math.Round(Convert.ToDouble(t1.TotalWorkedHour), 1) + "Hours",
t1.Notes
);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "TimeCard.xlsx");
                    }
                }
                return RedirectToAction("TimeCard");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("TimeCard");

            }
        }

        #endregion

        #region Employee details

        [HttpGet]
        [CheckSessionOut]
        public ActionResult Employees()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!("OWNER;MANAGER;".Contains(Session["JobTitle"].ToString())))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();

                ViewBag.JobTitles = repObj.getAllJobTitle();
                List < Models.employee > lstEmployee= new List<Models.employee>();
                lstEmployee= repObj.getEmployeesWithoutOwner(Convert.ToInt32(Session["BusinessID"])).ToList().Select(e=> new Models.employee
                {
                    EmployeeID = e.EmployeeID,
                    Name = e.Name,
                    Email = e.Email,
                    Phone = e.Phone,
                    JobTitleID = e.JobTitleID,
                    JobTitle = repObj.getJobTitleFromJobTitleId(e.JobTitleID).ToString(),
                    Username = e.Username,
                    Password =Decryptdata(e.Password).ToString().Trim(),
                    IsActive=e.isActive
                }).ToList< Models.employee>();
                return View(lstEmployee);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getEmployees()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        EmployeeID = t1.EmployeeID,
                        Name = t1.Name,
                        Email = t1.Email,
                        Jobtitle = repObj.getJobTitleFromJobTitleId(t1.JobTitleID).ToString(),
                        Phone = t1.Phone.ToString(),
                        JobTitleID = t1.JobTitleID,
                        Username = t1.Username,
                        BusinessID = t1.BusinessID,
                        ImagePath = t1.ImagePath,
                        Password =Decryptdata(t1.Password),
                        IsActive=t1.isActive
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult EditEmployee(FormCollection collection)
            {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                int empid=Convert.ToInt32(collection["editEmployeeID"]);
                employee editemp = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).Where(e => e.EmployeeID == empid).FirstOrDefault<employee>();
                if (!string.IsNullOrEmpty(collection["editName"].Trim()))
                    editemp.Name = collection["editName"].Trim();
                if (!string.IsNullOrEmpty(collection["editEmail"].Trim()))
                    editemp.Email = collection["editEmail"].Trim();
                if (!string.IsNullOrEmpty(collection["editPhone"].Trim()))
                    editemp.Phone = collection["editPhone"].Trim();
                if (!string.IsNullOrEmpty(collection["editJobTitle"].Trim()))
                    editemp.JobTitleID =repObj.getJobTitleIdFromJobTitle(collection["editJobTitle"].Trim());
                if (!string.IsNullOrEmpty(collection["editUsername"].Trim()))
                    editemp.Username = collection["editUsername"].Trim();
                if (!string.IsNullOrEmpty(collection["editPassword"].Trim()))
                {
                    if(!editemp.Password.Equals(collection["editPassword"].Trim()))
                    {
                        editemp.Password = Encryptdata(collection["editPassword"].Trim());
                    }
                }
                    
                //if (!string.IsNullOrEmpty(collection["editIsActive"].Trim()))
                //    editemp.isActive =collection["editIsActive"].Trim();
                repObj.updateEmployee(editemp);
                return RedirectToAction("Employees");
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "EditEmployee");
                return RedirectToAction("Employees");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult AddEmployee(FormCollection collection)
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                employee addemp = new employee();
                addemp.BusinessID = Convert.ToInt32(Session["BusinessID"]);
                addemp.CreatedAt = DateTime.Now;
                if (!string.IsNullOrEmpty(collection["addName"].Trim()))
                    addemp.Name = collection["addName"].Trim();
                if (!string.IsNullOrEmpty(collection["addEmail"].Trim()))
                    addemp.Email = collection["addEmail"].Trim();
                if (!string.IsNullOrEmpty(collection["addPhone"].Trim()))
                    addemp.Phone = collection["addPhone"].Trim();
                if (!string.IsNullOrEmpty(collection["addJobTitle"].Trim()))
                    addemp.JobTitleID = repObj.getJobTitleIdFromJobTitle(collection["addJobTitle"].Trim());
                if (!string.IsNullOrEmpty(collection["addUsername"].Trim()))
                    addemp.Username = collection["addUsername"].Trim();
                if (!string.IsNullOrEmpty(collection["addPassword"].Trim()))
                {

                    addemp.Password =Encryptdata(collection["addPassword"].Trim());
                }
                addemp.isActive = "Active";
                
                repObj.addEmployee(addemp);
                return RedirectToAction("Employees");
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AddEmployee");
                return RedirectToAction("Employees");
            }
            
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteEmployee(FormCollection collection)
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                int empid = Convert.ToInt32(collection["deleteEmployeeID"]);
                repObj.deleteEmployee(empid);
                return RedirectToAction("Employees");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Employees");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult ActivateEmployee(FormCollection collection)
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                int empid = Convert.ToInt32(collection["activateEmployeeID"]);
                employee editemp = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).Where(e => e.EmployeeID == empid).FirstOrDefault<employee>();
                editemp.isActive = "Active";
                editemp.EmployeeToken = null;
                repObj.updateEmployee(editemp);
                return RedirectToAction("Employees");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Employees");
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeactivateEmployee(FormCollection collection)
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                int empid = Convert.ToInt32(collection["deactivateEmployeeID"]);
                employee editemp = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).Where(e => e.EmployeeID == empid).FirstOrDefault<employee>();
                editemp.isActive = "Deactive";
                editemp.EmployeeToken = null;
                repObj.updateEmployee(editemp);
                return RedirectToAction("Employees");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Employees");
            }
        }
        [HttpPost]
        public JsonResult checkUsernameValidate(string username)
        {

            SmartHotelRepository repObj = new SmartHotelRepository();
            int employeeObj = repObj.getEmployeeIdFromUserName(username);

            if (employeeObj <=0)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }
        [HttpPost]
        public JsonResult checkUsernameValidateForEdit(string username,string id)
        {

            SmartHotelRepository repObj = new SmartHotelRepository();
            int employeeObj = repObj.getEmployeeIdFromUserName(username);

            if (employeeObj <= 0 || employeeObj==Convert.ToInt32(id))
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        [HttpPost]
        public JsonResult checkOldPassword(string oldpassword,string id)
        {

            SmartHotelRepository repObj = new SmartHotelRepository();
            bool status=repObj.checkOldPassword(Convert.ToInt32(id),Encryptdata(oldpassword));
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        public ActionResult ExpertToExcelEmployees()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                DataTable dt = new DataTable("Employees");
                dt.Columns.AddRange(new DataColumn[6] {
new DataColumn("Name"),
new DataColumn("Email"),
new DataColumn("Phone"),
new DataColumn("JobTitle"),
new DataColumn("Username"),
new DataColumn("IsActive")
            });
                foreach (var e in repObj.getEmployeesWithoutOwner(Convert.ToInt32(Session["BusinessID"])))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
e.Name,
e.Email,
e.Phone,
e.jobtitle.JobTitle1,
e.Username,
e.isActive
);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Employees.xlsx");
                    }
                }
                return RedirectToAction("Employees");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("Employees");

            }
        }

        #endregion

        #region TimeOffRequest
        [CheckSessionOut]
        public ActionResult TimeOffRequest()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("TIMEOFF") && ("OWNER;MANAGER;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            return View();
        }
        [CheckSessionOut]
        [HttpGet]
        public JsonResult getTimeOffRequests()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    RedirectToAction("Login");
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getTimeOffRequests(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        TimeOffRequestID = t1.TimeOffRequestID,
                        BusinessID = t1.BusinessID,
                        EmployeeID = t1.EmployeeID,
                        EmployeeName = repObj.getEmployeeNameFromEmpId(t1.EmployeeID).ToString(),
                        StartDate = t1.StartDate.ToString("MM/dd/yyyy"),
                        EndDate = t1.EndDate.ToString("MM/dd/yyyy"),
                        Reason = t1.Reason,
                        CreatedAt = t1.CreatedAt.ToString("MM/dd/yyyy"),
                        Status = t1.Status,
                        Comment = t1.Comment,
                        SignPath = t1.SignPath,
                        TotlaTimeOffRequest = repObj.getTotalTimeOffRequests(Convert.ToInt32(Session["BusinessID"]))
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult approveRejectTimeOffRequest(string id, string status,string comment)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                repObj.approveRejectTimeOffRequest(id, status,comment,Convert.ToInt32(Session["EmployeeId"]));
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(2);
            }
        }
        public ActionResult ExpertToExcelTimeOffRequest()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                DataTable dt = new DataTable("TimeOffRequest");
                dt.Columns.AddRange(new DataColumn[9] {
new DataColumn("EmployeeName"),
new DataColumn("StartDate"),
new DataColumn("EndDate"),
new DataColumn("Reason"),
new DataColumn("CreatedAt"),
new DataColumn("Status"),
new DataColumn("Comment"),
new DataColumn("SignPath"),
new DataColumn("TotlaTimeOffRequest")

            });
                foreach (var t1 in repObj.getTimeOffRequests(Convert.ToInt32(Session["BusinessID"])))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
repObj.getEmployeeNameFromEmpId(t1.EmployeeID).ToString(),
t1.StartDate.ToString("MM/dd/yyyy"),
t1.EndDate.ToString("MM/dd/yyyy"),
t1.Reason,
t1.CreatedAt.ToString("MM/dd/yyyy"),
t1.Status,
t1.Comment,
t1.SignPath,
repObj.getTotalTimeOffRequests(Convert.ToInt32(Session["BusinessID"]))
);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "TimeOffRequest.xlsx");
                    }
                }
                return RedirectToAction("TimeOffRequest");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("TimeOffRequest");

            }
        }

        #endregion

        #region Room List
        [HttpGet]
        [CheckSessionOut]
        public ActionResult CreateRoomList()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                int start=repObj.getRoomStartRange(Convert.ToInt32(Session["BusinessID"]));
                int end = repObj.getRoomEndRange(Convert.ToInt32(Session["BusinessID"]));
                ViewBag.RoomAllStatus = repObj.getAllRoomRoomDuties();
                ViewBag.RoomStartRange = start;
                ViewBag.RoomEndRange = end;
                if(start+25<end)
                    ViewBag.RoomEndDisplayRange = start + 25;
                else
                    ViewBag.RoomEndDisplayRange = end;
                //List<roomlist> lstRoomsList = repObj.getAllRoomLists(Convert.ToInt32(Session["BusinessID"]));
                List<Models.RoomLists> lstRoomsList = new List<Models.RoomLists>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                lstRoomsList = repObj.getAllRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(e => new Models.RoomLists
                {
                    RoomListID = e.RoomListID,
                    RoomID = e.RoomID,
                    RoomNumber = repObj.getRoomNumberFromRoomId(e.RoomID),
                    RoomStatusID = e.RoomStatusID,
                    RoomDutiesID = e.RoomDutiesID,
                    PriorityID = e.PriorityID,
                    EmployeeID = e.EmployeeID,
                    Notes = e.Notes,
                    StartTime = e.StartTime,
                    EndTime = e.EndTime,
                    TotalTime = e.TotalTime,
                    IsAssigned = e.IsAssigned,
                    HousekeeperID = e.HousekeeperID,
                    CreatedAt = e.CreatedAt,
                    BusinessID = e.BusinessID,
                    HeadHousekeeperID = e.HeadHousekeeperID,
                }).ToList<Models.RoomLists>();
                return View(lstRoomsList);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "CreateRoomList");
                return View();
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getAllRooms()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {

                    data = repObj.getAllUnassignedRooms(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        RoomID = t1.RoomID,
                        RoomNumber = t1.RoomNumber
                        
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //[HttpPost]
        //[CheckSessionOut]
        //public JsonResult getAllRoomsByStatus(int status)
        //{
        //    try
        //    {
        //        Session["BusinessID"] = 1;
        //        SmartHotelRepository repObj = new SmartHotelRepository();
        //        var ret = Json(new
        //        {
        //            data = repObj.getAllRoomsByStatus(Convert.ToInt32(Session["BusinessID"]),status).ToList().Select(t1 => new
        //            {
        //                RoomID = t1.RoomID,
        //                RoomNumber = t1.RoomNumber

        //            }).ToArray()
        //        }, JsonRequestBehavior.AllowGet);
        //        return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        [HttpPost]
        [CheckSessionOut]
        public ActionResult AddRoomList(FormCollection collection)
        {
            try
            {
                //int empid = 1;// Convert.ToInt32(collection["addEmployeeID"]);
                int roomstatusId = Convert.ToInt32(collection["selectedStatus"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime createdAt = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                
                List<roomlist> lstRoomsList = new List<roomlist>();
                SmartHotelRepository repObj = new SmartHotelRepository();
                foreach (var key in collection.Keys)
                {
                    if(key.ToString().Contains("roomlist"))
                    {
                        try
                        {
                            roomlist rl = new roomlist();
                            rl.RoomID = Convert.ToInt32(collection[key.ToString()]);
                            rl.RoomStatusID = 1;
                            rl.RoomDutiesID = roomstatusId;
                            rl.PriorityID = 2;
                            rl.EmployeeID = Convert.ToInt32(Session["EmployeeId"]);
                            rl.CreatedAt = createdAt;
                            rl.BusinessID = Convert.ToInt32(Session["BusinessID"]);
                            lstRoomsList.Add(rl);
                        }
                        catch (Exception ex)
                        {
                            ExceptionLogining.SendErrorToText(ex, "addroomlistinside");
                        }
                    }
                }
                repObj.addRoomLists(lstRoomsList);
                return RedirectToAction("CreateRoomList");
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AddRoomList");
                return RedirectToAction("CreateRoomList");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult EditRoomList(FormCollection collection)
        {
            try
            {
                //int empid = 1;// Convert.ToInt32(collection["addEmployeeID"]);
                int roomDutyId = Convert.ToInt32(collection["editselectedStatus"]);
                List<int> lstpresentRoomList = new List<int>();
                List<int> lstdeletedRoomList = new List<int>();
                List<int> lstdisplayedRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().Contains("roomlist"))
                    {
                            lstdisplayedRoomList.Add(Convert.ToInt32(collection[key.ToString()]));

                    }
                }
                lstpresentRoomList = repObj.getAllRoomIdByDuty(Convert.ToInt32(Session["BusinessID"]), roomDutyId, requestDate);
                foreach (var item in lstpresentRoomList)
                {
                    if(!lstdisplayedRoomList.Contains(item))
                    {
                        lstdeletedRoomList.Add(item);
                    }
                }
                if(lstdeletedRoomList.Count>0)
                {
                    repObj.deleteRoomLists(lstdeletedRoomList, requestDate);
                }
                return RedirectToAction("CreateRoomList");
            }
            catch (Exception ex)
            {
                return RedirectToAction("CreateRoomList");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteRoomList(FormCollection collection)
        {
            try
            {
                int roomDutyId = Convert.ToInt32(collection["deleteselectedStatus"]);
                List<int> lstpresentRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                
                lstpresentRoomList = repObj.getAllRoomIdByDuty(Convert.ToInt32(Session["BusinessID"]), roomDutyId, requestDate);
                
                if (lstpresentRoomList.Count > 0)
                {
                    repObj.deleteRoomLists(lstpresentRoomList, requestDate);
                }
                return RedirectToAction("CreateRoomList");
            }
            catch (Exception ex)
            {
                return RedirectToAction("CreateRoomList");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteAllRoomList(FormCollection collection)
        {
            try
            {
                List<int> lstpresentRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                lstpresentRoomList = repObj.getAllRoomList(Convert.ToInt32(Session["BusinessID"]), requestDate);

                if (lstpresentRoomList.Count > 0)
                {
                    repObj.deleteRoomLists(lstpresentRoomList, requestDate);
                }
                return RedirectToAction("CreateRoomList");
            }
            catch (Exception ex)
            {
                return RedirectToAction("CreateRoomList");
            }
        }

        #endregion

        #region AssignToHousekeeper
        [CheckSessionOut]
        public ActionResult AssignToHousekeeper()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                int start = repObj.getRoomStartRangeFormRoomList(Convert.ToInt32(Session["BusinessID"]), requestDate);
                int end = repObj.getRoomEndRangeFormRoomList(Convert.ToInt32(Session["BusinessID"]), requestDate);
                ViewBag.AllHousekeeper = repObj.getAllHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                ViewBag.RoomStartRange = start;
                ViewBag.RoomEndRange = end;
                if (start + 25 < end)
                    ViewBag.RoomEndDisplayRange = start + 25;
                else
                    ViewBag.RoomEndDisplayRange = end;
                //List<roomlist> lstRoomsList = repObj.getAllRoomLists(Convert.ToInt32(Session["BusinessID"]));
                List<Models.RoomLists> lstRoomsList = new List<Models.RoomLists>();
                
                lstRoomsList = repObj.getAllCreatedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(e => new Models.RoomLists
                {
                    RoomListID = e.RoomListID,
                    RoomID = e.RoomID,
                    RoomNumber = repObj.getRoomNumberFromRoomId(e.RoomID),
                    RoomStatusID = e.RoomStatusID,
                    RoomDutiesID = e.RoomDutiesID,
                    PriorityID = e.PriorityID,
                    EmployeeID = e.EmployeeID,
                    Notes = e.Notes,
                    StartTime = e.StartTime,
                    EndTime = e.EndTime,
                    TotalTime = e.TotalTime,
                    IsAssigned = e.IsAssigned,
                    HousekeeperID = e.HousekeeperID,
                    CreatedAt = e.CreatedAt,
                    BusinessID = e.BusinessID,
                    HeadHousekeeperID = e.HeadHousekeeperID,
                }).ToList<Models.RoomLists>();
                return View(lstRoomsList);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AssignToHousekeeper");
                return View();
            }
            return View();
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getAllHKUnassignedRooms()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {

                    data = repObj.getAllHKUnassignedRooms(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        RoomID = t1.RoomID,
                        RoomNumber = t1.room.RoomNumber

                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public ActionResult AssignHKRoomList(FormCollection collection)
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmployeeId"]);
                int selectedEmployeeId = Convert.ToInt32(collection["selectedEmployee"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime reduestedDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

                List<roomlist> lstRoomsList = new List<roomlist>();
                SmartHotelRepository repObj = new SmartHotelRepository();
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().Contains("roomlist"))
                    {
                        try
                        {
                            repObj.updateHKToRoomList(Convert.ToInt32(Session["BusinessID"]), reduestedDate, Convert.ToInt32(collection[key.ToString()]), selectedEmployeeId);
                            
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                return RedirectToAction("AssignToHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHousekeeper");
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public ActionResult EditHKRoomList(FormCollection collection)
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmployeeId"]);
                int selectedEmployee = Convert.ToInt32(collection["editselectedEmployee"]);
                List<int> lstpresentRoomList = new List<int>();
                List<int> lstdeletedRoomList = new List<int>();
                List<int> lstdisplayedRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().Contains("roomlist"))
                    {
                        lstdisplayedRoomList.Add(Convert.ToInt32(collection[key.ToString()]));

                    }
                }
                lstpresentRoomList = repObj.getAllRoomIdByHK(Convert.ToInt32(Session["BusinessID"]), selectedEmployee, requestDate);
                foreach (var item in lstpresentRoomList)
                {
                    if (!lstdisplayedRoomList.Contains(item))
                    {
                        lstdeletedRoomList.Add(item);
                    }
                }
                if (lstdeletedRoomList.Count > 0)
                {
                    repObj.deleteHKToRoomList(lstdeletedRoomList, requestDate);
                }
                return RedirectToAction("AssignToHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHousekeeper");
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteHKRoomList(FormCollection collection)
        {
            try
            {
                //int empid = 1;// Convert.ToInt32(collection["addEmployeeID"]);
                int selectedEmployee = Convert.ToInt32(collection["deleteselectedEmployee"]);
                List<int> lstpresentRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                lstpresentRoomList = repObj.getAllRoomIdByHK(Convert.ToInt32(Session["BusinessID"]), selectedEmployee, requestDate);

                if (lstpresentRoomList.Count > 0)
                {
                    repObj.deleteHKToRoomList(lstpresentRoomList, requestDate);
                }
                return RedirectToAction("AssignToHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHousekeeper");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteAllHKRoomList(FormCollection collection)
        {
            try
            {
                List<int> lstpresentRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                lstpresentRoomList = repObj.getAllCreatedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).Select(b=>b.RoomID).ToList();

                if (lstpresentRoomList.Count > 0)
                {
                    repObj.deleteHKToRoomList(lstpresentRoomList, requestDate);
                }
                return RedirectToAction("AssignToHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHousekeeper");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult AssignHKAutomatically()
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmployeeId"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestedDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

                List<roomlist> lstRoomsList = new List<roomlist>();
                SmartHotelRepository repObj = new SmartHotelRepository();
                lstRoomsList=repObj.getAllHKUnassignedRooms(Convert.ToInt32(Session["BusinessID"]), requestedDate);
                if(lstRoomsList!=null && lstRoomsList.Count>0)
                {
                    List<employee> lstHKEmp= repObj.getAllHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                    if(lstHKEmp!=null && lstHKEmp.Count>0)
                    {
                        int roomIndex = 0;
                        int totalRooms = lstRoomsList.Count;
                        int totolHk = lstHKEmp.Count;
                        foreach (var HK in lstHKEmp)
                        {
                            for(int i=0;i<(totalRooms/totolHk)+ (totalRooms % totolHk); i++)
                            {
                                if (roomIndex >= totalRooms)
                                    break;
                                repObj.updateHKToRoomListByRoomListId(lstRoomsList.ElementAt(roomIndex).RoomListID,HK.EmployeeID);
                                roomIndex++;
                            }
                        }
                    }
                }
                
                return RedirectToAction("AssignToHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHousekeeper");
            }
        }
        #endregion

        #region AssignToHeadHousekeeper
        [CheckSessionOut]
        public ActionResult AssignToHeadHousekeeper()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                int start = repObj.getRoomStartRangeFormRoomListForHeadHK(Convert.ToInt32(Session["BusinessID"]), requestDate);
                int end = repObj.getRoomEndRangeFormRoomListForHeadHK(Convert.ToInt32(Session["BusinessID"]), requestDate);
                ViewBag.AllHousekeeper = repObj.getAllHeadHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                ViewBag.RoomStartRange = start;
                ViewBag.RoomEndRange = end;
                if (start + 25 < end)
                    ViewBag.RoomEndDisplayRange = start + 25;
                else
                    ViewBag.RoomEndDisplayRange = end;
                //List<roomlist> lstRoomsList = repObj.getAllRoomLists(Convert.ToInt32(Session["BusinessID"]));
                List<Models.RoomLists> lstRoomsList = new List<Models.RoomLists>();
                
                lstRoomsList = repObj.getAllHeadHKAssignedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(e => new Models.RoomLists
                {
                    RoomListID = e.RoomListID,
                    RoomID = e.RoomID,
                    RoomNumber = repObj.getRoomNumberFromRoomId(e.RoomID),
                    RoomStatusID = e.RoomStatusID,
                    RoomDutiesID = e.RoomDutiesID,
                    PriorityID = e.PriorityID,
                    EmployeeID = e.EmployeeID,
                    Notes = e.Notes,
                    StartTime = e.StartTime,
                    EndTime = e.EndTime,
                    TotalTime = e.TotalTime,
                    IsAssigned = e.IsAssigned,
                    HousekeeperID = e.HousekeeperID,
                    CreatedAt = e.CreatedAt,
                    BusinessID = e.BusinessID,
                    HeadHousekeeperID = e.HeadHousekeeperID,
                }).ToList<Models.RoomLists>();
                return View(lstRoomsList);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AssignToHeadHousekeeper");
                return View();
            }
            return View();
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getAllHeadHKUnassignedRooms()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {

                    data = repObj.getAllHeadHKUnassignedRooms(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        RoomID = t1.RoomListID,
                        RoomNumber = t1.room.RoomNumber

                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public ActionResult AssignHeadHKRoomList(FormCollection collection)
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmployeeId"]);
                int selectedEmployeeId = Convert.ToInt32(collection["selectedEmployee"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime reduestedDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

                List<roomlist> lstRoomsList = new List<roomlist>();
                SmartHotelRepository repObj = new SmartHotelRepository();
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().Contains("roomlist"))
                    {
                        try
                        {
                            repObj.updateHeadHKToRoomList(Convert.ToInt32(collection[key.ToString()]), selectedEmployeeId);

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                return RedirectToAction("AssignToHeadHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHeadHousekeeper");
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public ActionResult EditHeadHKRoomList(FormCollection collection)
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmployeeId"]);
                int selectedEmployee = Convert.ToInt32(collection["editselectedEmployee"]);
                List<int> lstpresentRoomList = new List<int>();
                List<int> lstdeletedRoomList = new List<int>();
                List<int> lstdisplayedRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().Contains("roomlist"))
                    {
                        lstdisplayedRoomList.Add(Convert.ToInt32(collection[key.ToString()]));

                    }
                }
                lstpresentRoomList = repObj.getAllRoomListIdByHK(Convert.ToInt32(Session["BusinessID"]), selectedEmployee, requestDate);
                foreach (var item in lstpresentRoomList)
                {
                    if (!lstdisplayedRoomList.Contains(item))
                    {
                        lstdeletedRoomList.Add(item);
                    }
                }
                if (lstdeletedRoomList.Count > 0)
                {
                    repObj.deleteHeadHKToRoomList(lstdeletedRoomList, requestDate);
                }
                return RedirectToAction("AssignToHeadHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHeadHousekeeper");
            }
        }
        
        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteHeadHKRoomList(FormCollection collection)
        {
            try
            {
                //int empid = 1;// Convert.ToInt32(collection["addEmployeeID"]);
                int selectedEmployee = Convert.ToInt32(collection["deleteselectedEmployee"]);
                List<int> lstpresentRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                lstpresentRoomList = repObj.getAllRoomIdByHeadHK(Convert.ToInt32(Session["BusinessID"]), selectedEmployee, requestDate);

                if (lstpresentRoomList.Count > 0)
                {
                    repObj.deleteHeadHKToRoomList(lstpresentRoomList, requestDate);
                }
                return RedirectToAction("AssignToHeadHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHeadHousekeeper");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult AssignHeadHKAutomatically()
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmployeeId"]);
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestedDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

                List<roomlist> lstRoomsList = new List<roomlist>();
                SmartHotelRepository repObj = new SmartHotelRepository();
                lstRoomsList = repObj.getAllHeadHKUnassignedRooms(Convert.ToInt32(Session["BusinessID"]), requestedDate);
                if (lstRoomsList != null && lstRoomsList.Count > 0)
                {
                    List<employee> lstHKEmp = repObj.getAllHeadHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                    if (lstHKEmp != null && lstHKEmp.Count > 0)
                    {
                        int roomIndex = 0;
                        int totalRooms = lstRoomsList.Count;
                        int totolHk = lstHKEmp.Count;
                        foreach (var HK in lstHKEmp)
                        {
                            for (int i = 0; i < (totalRooms / totolHk) + (totalRooms % totolHk); i++)
                            {
                                if (roomIndex >= totalRooms)
                                    break;
                                repObj.updateHeadHKToRoomListByRoomListId(lstRoomsList.ElementAt(roomIndex).RoomListID, HK.EmployeeID);
                                roomIndex++;
                            }
                        }
                    }
                }

                return RedirectToAction("AssignToHeadHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHeadHousekeeper");
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public ActionResult DeleteAllHeadHKRoomList(FormCollection collection)
        {
            try
            {
                List<int> lstpresentRoomList = new List<int>();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                lstpresentRoomList = repObj.getAllHeadHKAssignedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).Select(b=>b.RoomListID).ToList();

                if (lstpresentRoomList.Count > 0)
                {
                    repObj.deleteHeadHKToRoomList(lstpresentRoomList, requestDate);
                }
                return RedirectToAction("AssignToHeadHousekeeper");
            }
            catch (Exception ex)
            {
                return RedirectToAction("AssignToHeadHousekeeper");
            }
        }

        #endregion

        #region ViewAllRooms
        [CheckSessionOut]
        public ActionResult ViewAllRooms()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                
                ViewBag.allDuties = repObj.getAllRoomRoomDuties();
                ViewBag.allStatus = repObj.getAllRoomStatus();
                ViewBag.allPriorities = repObj.getAllPriority();
                ViewBag.AllHousekeeper = repObj.getAllHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                ViewBag.AllHeadHousekeeper = repObj.getAllHeadHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));

            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "ViewAllRooms");
                return View();
            }
            return View();
        }
        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllHKRooms()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllHeadHKAssignedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        RoomListID = t1.RoomListID,
                        RoomNumber = t1.room.RoomNumber.ToString(),
                        Duty = t1.roomduty.Duty,
                        DutyId=t1.RoomDutiesID,
                        Status= t1.roomstatu!=null?t1.roomstatu.Status:"",
                        StatusId=t1.RoomStatusID,
                        Priority= t1.priority!=null?t1.priority.Priority1:"",
                        PriorityId=t1.PriorityID,
                        AssignedTo = t1.employee2!=null?t1.employee2.Name:"",
                        AssignToId=t1.HousekeeperID,
                        VerifiedBy = t1.employee1!=null?t1.employee1.Name:"",
                        VerifiedById=t1.HeadHousekeeperID,
                        StartTime = ConvertTOAMPM(t1.StartTime),
                        EndTime = ConvertTOAMPM(t1.EndTime),
                        TotalTime= t1.TotalTime!=null?Math.Round((double)t1.TotalTime,0)+ " Minute" : "",
                        Note=t1.Notes
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult editRoomListAjax(string id, string HKId, string duty, string status, string priority,string HeadHk, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st= repObj.editRoomList(Convert.ToInt32(id), Convert.ToInt32(HKId), Convert.ToInt32(duty), Convert.ToInt32(status), Convert.ToInt32(priority), Convert.ToInt32(HeadHk), note);
                if (st)
                    return Json(1,JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2,JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteRoomListAjax(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteRoomListsByRoomListId(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExpertToExcelViewAllRooms()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                DataTable dt = new DataTable("GuestList");
                dt.Columns.AddRange(new DataColumn[10] {
new DataColumn("RoomNumber"),
new DataColumn("Duty"),
new DataColumn("Status"),
new DataColumn("Priority"),
new DataColumn("AssignedTo"),
new DataColumn("VerifiedBy"),
new DataColumn("StartTime"),
new DataColumn("EndTime"),
new DataColumn("TotalTime"),
new DataColumn("Note")

            });
                foreach (var t1 in repObj.getAllHeadHKAssignedRoomLists(Convert.ToInt32(Session["BusinessID"]),requestDate))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
t1.room.RoomNumber.ToString(),
t1.roomduty.Duty,
t1.roomstatu!=null ? t1.roomstatu.Status : "",
t1.priority!=null ? t1.priority.Priority1 : "",
t1.employee2!=null ? t1.employee2.Name : "",
t1.employee1!=null ? t1.employee1.Name : "",
ConvertTOAMPM(t1.StartTime),
ConvertTOAMPM(t1.EndTime),
t1.TotalTime!=null ? Math.Round((double)t1.TotalTime, 0) + "Minute" : "",
t1.Notes

);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ViewAllRooms.xlsx");
                    }
                }
                return RedirectToAction("ViewAllRooms");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("ViewAllRooms");

            }
        }

        #endregion

        #region ViewByHousekeeper
        [CheckSessionOut]
        public ActionResult ViewByHousekeeper()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();
            string[] formats = { "MM/dd/yyyy" };
            DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

            ViewBag.allDuties = repObj.getAllRoomRoomDuties();
            ViewBag.allStatus = repObj.getAllRoomStatus();
            ViewBag.allPriorities = repObj.getAllPriority();
            ViewBag.AllHousekeeper = repObj.getAllHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
            ViewBag.AllHeadHousekeeper = repObj.getAllHeadHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
            List<roomlist> lstRoomList = repObj.getAllHeadHKAssignedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate);
            return View(lstRoomList);
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getAllRoomsListforHK()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllHeadHKAssignedRoomLists(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        RoomListID = t1.RoomListID,
                        RoomNumber = t1.room.RoomNumber.ToString(),
                        Duty = t1.roomduty.Duty,
                        DutyId = t1.RoomDutiesID,
                        Status = t1.roomstatu.Status,
                        StatusId = t1.RoomStatusID,
                        Priority = t1.priority.Priority1,
                        PriorityId = t1.PriorityID,
                        AssignedTo = t1.employee2.Name,
                        AssignToId = t1.HousekeeperID,
                        VerifiedBy = t1.employee1.Name,
                        VerifiedById = t1.HeadHousekeeperID,
                        StartTime = ConvertTOAMPM(t1.StartTime),
                        EndTime = ConvertTOAMPM(t1.EndTime),
                        TotalTime = t1.TotalTime,
                        Note = t1.Notes
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }
        #endregion

        #region MaintenanceRequest
        [CheckSessionOut]
        public ActionResult MaintenanceRequest()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();

            ViewBag.allMaintenanceType = repObj.getAllMaintenanceType();
            ViewBag.allMaintenanceStatus = repObj.getAllMaintenanceStatus();
            ViewBag.allPriorities = repObj.getAllPriority();
            ViewBag.AllHeadHK = repObj.getAllHeadHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
            ViewBag.AllMaintenance = repObj.getAllMaintenanceOfBusiness(Convert.ToInt32(Session["BusinessID"]));
            ViewBag.AllRooms = repObj.getAllRooms(Convert.ToInt32(Session["BusinessID"]));
            return View();
        }

        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllMaintenanceRooms()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllMaintenanceRooms(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        MaintenanceID = t1.MaintenanceID,
                        RoomNumber = t1.room.RoomNumber.ToString(),
                        RoomId = t1.RoomID,
                        Type = t1.maintenancetype.Type,
                        TypeId = t1.MaintenanceDutyID,
                        Status = t1.maintenancestatu.Status,
                        MaintenanceStatusID = t1.MaintenanceStatusID,
                        Priority = t1.priority != null? t1.priority.Priority1:"",
                        PriorityId = t1.PriorityID,
                        AddedBy = t1.employee != null ? t1.employee.Name : "",
                        AddedById = t1.AddedBy,
                        AssignedTo = t1.employee1!=null?t1.employee1.Name:"",
                        AssignToId = t1.AssignedTo,
                        VerifiedBy = t1.employee2 != null ? t1.employee2.Name:"",
                        VerifiedById = t1.VerifiedBy,
                        StartTime = ConvertTOAMPM(t1.StartTime),
                        EndTime = ConvertTOAMPM(t1.EndTime),
                        TotalTime = t1.TotalTime != null ? Math.Round((double)t1.TotalTime, 0) + " Minute" : "",
                        Note = t1.Notes,
                        Picture1 = t1.Img1Path,
                        Picture2 = t1.Img2Path,
                        Picture3 = t1.Img3Path,
                        Picture4 = t1.Img4Path
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult editMaintenanceAjax(string id, string MpId, string type, string status, string priority, string HeadHk, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.editMaintenanceRequest(Convert.ToInt32(id), Convert.ToInt32(MpId), Convert.ToInt32(type), Convert.ToInt32(status), Convert.ToInt32(priority), Convert.ToInt32(HeadHk), note);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteMaintenanceRequestAjax(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteMaintenanceRequest(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult AddMaintenanceRequestAjax(string id, string MpId, string type, string status, string priority, string HeadHk, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                maintenance addMaintananceObj = new maintenance();
                addMaintananceObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addMaintananceObj.CreatedAt = requestDate; 
                addMaintananceObj.AddedBy = Convert.ToInt32(Session["EmployeeId"]);
                if (!string.IsNullOrEmpty(id))
                    addMaintananceObj.RoomID = Convert.ToInt32(id);
                if (!string.IsNullOrEmpty(MpId))
                    addMaintananceObj.AssignedTo = Convert.ToInt32(MpId);
                if (!string.IsNullOrEmpty(type))
                    addMaintananceObj.MaintenanceDutyID = Convert.ToInt32(type);
                if (!string.IsNullOrEmpty(status))
                    addMaintananceObj.MaintenanceStatusID = Convert.ToInt32(status);
                if (!string.IsNullOrEmpty(priority))
                    addMaintananceObj.PriorityID = Convert.ToInt32(priority);
                if (!string.IsNullOrEmpty(HeadHk))
                    addMaintananceObj.VerifiedBy = Convert.ToInt32(HeadHk);
                if (!string.IsNullOrEmpty(note))
                    addMaintananceObj.Notes = note;
               
                bool st=repObj.addMaintenanceRequest(addMaintananceObj);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "Add Maintenance");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExpertToExcelMaintenanceRequest()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                
                DataTable dt = new DataTable("MaintenanceRequest");
                dt.Columns.AddRange(new DataColumn[11] {
new DataColumn("RoomNumber"),
new DataColumn("Type"),
new DataColumn("Status"),
new DataColumn("Priority"),
new DataColumn("AddedBy"),
new DataColumn("AssignedTo"),
new DataColumn("VerifiedBy"),
new DataColumn("StartTime"),
new DataColumn("EndTime"),
new DataColumn("TotalTime"),
new DataColumn("Note")


            });
                foreach (var t1 in repObj.getAllMaintenanceRooms(Convert.ToInt32(Session["BusinessID"])))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
t1.room.RoomNumber.ToString(),
t1.maintenancetype.Type,
t1.maintenancestatu.Status,
t1.priority != null ? t1.priority.Priority1 : "",
t1.employee != null ? t1.employee.Name : "",
t1.employee1 != null ? t1.employee1.Name : "",
t1.employee2 != null ? t1.employee2.Name : "",
ConvertTOAMPM(t1.StartTime),
ConvertTOAMPM(t1.EndTime),
t1.TotalTime != null ? Math.Round((double)t1.TotalTime, 0) + "Minute" : "",
t1.Notes

);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "MaintenanceRequest.xlsx");
                    }
                }
                return RedirectToAction("MaintenanceRequest");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("MaintenanceRequest");

            }
        }


        #endregion

        #region LostFoundRequest
        [CheckSessionOut]
        public ActionResult LostFoundRequest()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();
            
            ViewBag.allLostFoundStatus = repObj.allLostFoundStatus();
            ViewBag.allPriorities = repObj.getAllPriority();
            ViewBag.getAllEmployeeOfBusiness = repObj.getAllEmployeeOfBusiness(Convert.ToInt32(Session["BusinessID"]));
            ViewBag.AllRooms = repObj.getAllRooms(Convert.ToInt32(Session["BusinessID"]));
            return View();
        }

        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllLostFoundRequestAjax()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllLostFoundRequest(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        LostFoundID = t1.LostFoundID,
                        RoomNumber = t1.room != null ? t1.room.RoomNumber.ToString() : "",
                        RoomId = t1.RoomID,
                        StayDate = t1.StayDate != null?((DateTime)t1.StayDate).ToString("MM/dd/yyyy"):"",
                        Status = t1.lostfoundstatu!=null?t1.lostfoundstatu.Status:"",
                        LostFoundStatusID = t1.LostFoundStatusID,
                        Priority = t1.priority!=null?t1.priority.Priority1:"",
                        PriorityId = t1.PriorityID,
                        Picture1 = t1.Picture1,
                        Picture2 = t1.Picture2,
                        Picture3 = t1.Picture3,
                        AddedBy = t1.employee!=null?t1.employee.Name:"",
                        AddedById = t1.AddedBy,
                        Note = t1.Notes
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }
        [CheckSessionOut]
        public JsonResult editLostFoundRequestAjax(string id, string status, string priority, string collectedby, string note)
        {
            try
            {
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.editLostFoundRequest(Convert.ToInt32(id), Convert.ToInt32(status), Convert.ToInt32(priority), Convert.ToInt32(collectedby), note, requestDate);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteLostFoundRequestAjax(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteLostFoundRequest(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult AddLostFoundRequestAjax(string id,string staydate,string status, string priority, string collectedby, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                DateTime stayDate = new DateTime();
                if (!string.IsNullOrEmpty(staydate))
                    stayDate = DateTime.ParseExact(staydate, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                lostfound addMaintananceObj = new lostfound();
                addMaintananceObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);
                
                addMaintananceObj.AddedBy = Convert.ToInt32(Session["EmployeeId"]);
                if (!string.IsNullOrEmpty(id))
                    addMaintananceObj.RoomID = Convert.ToInt32(id);
                if (staydate!=null)
                    addMaintananceObj.StayDate = stayDate;
                if (!string.IsNullOrEmpty(status))
                    addMaintananceObj.LostFoundStatusID = Convert.ToInt32(status);
                if (!string.IsNullOrEmpty(collectedby))
                    addMaintananceObj.AddedBy = Convert.ToInt32(collectedby);
                if (!string.IsNullOrEmpty(priority))
                    addMaintananceObj.PriorityID = Convert.ToInt32(priority);
                if (!string.IsNullOrEmpty(note))
                    addMaintananceObj.Notes = note;

                bool st = repObj.addLostFoundRequest(addMaintananceObj);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AddLostFoundRequestAjax");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExpertToExcelLostFoundRequest()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                DataTable dt = new DataTable("LostFoundRequest");
                dt.Columns.AddRange(new DataColumn[6] {
new DataColumn("RoomNumber"),
new DataColumn("StayDate"),
new DataColumn("Status"),
new DataColumn("Priority"),
new DataColumn("AddedBy"),
new DataColumn("Note")



            });
                foreach (var t1 in repObj.getAllLostFoundRequest(Convert.ToInt32(Session["BusinessID"])))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
t1.room!=null ? t1.room.RoomNumber.ToString() : "",
t1.StayDate!=null ? ((DateTime)t1.StayDate).ToString("MM/dd/yyyy") : "",
t1.lostfoundstatu!=null ? t1.lostfoundstatu.Status : "",
t1.priority!=null ? t1.priority.Priority1 : "",
t1.employee!=null ? t1.employee.Name : "",
t1.Notes


);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "LostFoundRequest.xlsx");
                    }
                }
                return RedirectToAction("LostFoundRequest");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("LostFoundRequest");

            }
        }

        #endregion

        #region Alerts
        [CheckSessionOut]
        public ActionResult Alerts()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();

            ViewBag.allAlertStatus = repObj.allAlertStatus();
            ViewBag.allAlertType = repObj.allAlertType();
            ViewBag.allAssignedTo = repObj.getAllHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"])).Concat(
            repObj.getAllMaintenanceOfBusiness(Convert.ToInt32(Session["BusinessID"])));
            ViewBag.getAllHeadHousekeeprOfBusiness = repObj.getAllHeadHousekeeprOfBusiness(Convert.ToInt32(Session["BusinessID"]));
            ViewBag.AllRooms = repObj.getAllRooms(Convert.ToInt32(Session["BusinessID"]));
            return View();
        }

        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllAlertsAjax()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDateTime"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllAlerts(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        AlertsID = t1.AlertsID,
                        RoomNumber = t1.room!=null?t1.room.RoomNumber.ToString():"",
                        CreatedAt = t1.CreatedAt.ToString("MM/dd/yyyy hh:mm tt"),
                        RoomId = t1.RoomId,
                        Status = t1.alertstatu!=null?t1.alertstatu.Status:"",
                        AlertStatusID = t1.AlertStatusID,
                        Type = t1.alerttype!=null?t1.alerttype.type:"",
                        AlertTypeID = t1.AlertTypeID,
                        Picture1 = t1.Attachment1,
                        Picture2 = t1.Attachment2,
                        Picture3 = t1.Attachment3,
                        CreatedBy = t1.employee1!=null?t1.employee1.Name:"",
                        CreatedById = t1.CreatedBy,
                        AssignedTo = t1.employee!=null?t1.employee.Name:"",
                        AssignedToId = t1.AssignedTo,
                        VerifiedBy = t1.employee2!=null?t1.employee2.Name:"",
                        VerifiedById = t1.VerifiedBy,
                        Note = t1.Notes
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }
        [CheckSessionOut]
        public JsonResult editAlertsAjax(string id, string type, string assignedto, string status, string verifyedby, string note)
        {
            try
            {
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.editAlerts(Convert.ToInt32(id), Convert.ToInt32(type), Convert.ToInt32(assignedto), Convert.ToInt32(status), Convert.ToInt32(verifyedby), note);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteAlertsAjax(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteAlerts(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult AddAlertsAjax(string id, string type, string assignedto, string status, string verifyedby, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDateTime"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                
                alert addAlertObj = new alert();
                addAlertObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addAlertObj.CreatedBy = Convert.ToInt32(Session["EmployeeId"]);
                addAlertObj.CreatedAt = requestDate;
                if (!string.IsNullOrEmpty(id))
                    addAlertObj.RoomId = Convert.ToInt32(id);
                if (!string.IsNullOrEmpty(type))
                    addAlertObj.AlertTypeID = Convert.ToInt32(type);
                if (!string.IsNullOrEmpty(status))
                    addAlertObj.AlertStatusID = Convert.ToInt32(status);
                if (!string.IsNullOrEmpty(assignedto))
                    addAlertObj.AssignedTo = Convert.ToInt32(assignedto);
                if (!string.IsNullOrEmpty(verifyedby))
                    addAlertObj.VerifiedBy = Convert.ToInt32(verifyedby);
                if (!string.IsNullOrEmpty(note))
                    addAlertObj.Notes = note;

                bool st = repObj.addAlerts(addAlertObj);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AddAlertsAjax");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExpertToExcelAlerts()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                DataTable dt = new DataTable("Alerts");
                dt.Columns.AddRange(new DataColumn[8] {
new DataColumn("RoomNumber"),
new DataColumn("CreatedAt"),
new DataColumn("Status"),
new DataColumn("Type"),
new DataColumn("CreatedBy"),
new DataColumn("AssignedTo"),
new DataColumn("VerifiedBy"),
new DataColumn("Note"),




            });
                foreach (var t1 in repObj.getAllAlerts(Convert.ToInt32(Session["BusinessID"])))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
t1.room!=null ? t1.room.RoomNumber.ToString() : "",
t1.CreatedAt.ToString("MM/dd/yyyyhh:mmtt"),
t1.alertstatu!=null ? t1.alertstatu.Status : "",
t1.alerttype!=null ? t1.alerttype.type : "",
t1.employee!=null ? t1.employee.Name : "",
t1.employee1!=null ? t1.employee1.Name : "",
t1.employee2!=null ? t1.employee2.Name : "",
t1.Notes


);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Alerts.xlsx");
                    }
                }
                return RedirectToAction("Alerts");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("Alerts");

            }
        }

        #endregion

        #region Laundry
        [CheckSessionOut]
        public ActionResult Laundry()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();

                ViewBag.getAllLaundryStatus = repObj.getAllLaundryStatus();
                ViewBag.AllLaundryPerson = repObj.getAllLaundryPersonOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllLaundry()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllLaundry(Convert.ToInt32(Session["BusinessID"]), requestDate).ToList().Select(t1 => new
                    {
                        LaundryId = t1.LaundryId,
                        Description = t1.Description,
                        Status = t1.laundrystatu != null? t1.laundrystatu.Status:"",
                        StatusId = t1.LaundryStatusID,
                        AssignedTo = t1.employee1!=null?t1.employee1.Name:"",
                        AssignToId = t1.AssignedTo,
                        AddedBy = t1.employee!=null?t1.employee.Name:"",
                        AddedById = t1.AddedBy,
                        StartTime = t1.StartTime != null ? ConvertTOAMPM(((DateTime)t1.StartTime).TimeOfDay) : null,
                        EndTime = t1.EndTime!=null?ConvertTOAMPM(((DateTime)t1.EndTime).TimeOfDay):null,
                        TotalTime = t1.TotalTime != null ? Math.Round((double)t1.TotalTime, 0) + " Minute" : ""
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
            //    return Json(new
            //    {
            //         data = {new < int, string, string, int, string, int?, string, int, string, string, float?>[0]};
            //}, JsonRequestBehavior.AllowGet);
            return null;
            }
        }
        [CheckSessionOut]
        public JsonResult editLaundry(string id, string description, string assignedto, string status)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.editLaundry(Convert.ToInt32(id), description, Convert.ToInt32(assignedto), Convert.ToInt32(status));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteLaundry(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteLaundry(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult AddLaundry(string description, string assignedto)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDateTime"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

                laundry addAlertObj = new laundry();
                addAlertObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addAlertObj.AddedBy = Convert.ToInt32(Session["EmployeeId"]);
                addAlertObj.CreatedAt = requestDate;
                addAlertObj.LaundryStatusID = 1;
                if (!string.IsNullOrEmpty(description))
                    addAlertObj.Description = description;
                if (!string.IsNullOrEmpty(assignedto))
                    addAlertObj.AssignedTo = Convert.ToInt32(assignedto);

                bool st = repObj.addLaundry(addAlertObj);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AddLaundry");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region schedule
        [CheckSessionOut]
        public ActionResult schedule()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("SCHEDULING") && ("OWNER;MANAGER;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();
            
            ViewBag.allAssignedTo = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"]));
            List<schedule> lstSchedule = repObj.getAllSchedules(Convert.ToInt32(Session["BusinessID"])).ToList();
            return View(lstSchedule);
        }

        public string employeeAtForColor(int empid)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                var lstEmp = repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])).ToArray();
                int retval = 1;
                for (int i = 0; i < lstEmp.Count(); i++)
                {
                    if(lstEmp[i].EmployeeID==empid)
                    {
                        retval = (i + 1) % 5;
                        break;
                    }
                }
                switch (retval)
                {
                    case 1:
                        return "rgb(0, 166, 90)";
                    case 2:
                        return "rgb(243, 156, 18)";
                    case 3:
                        return "rgb(0, 192, 239)";
                    case 4:
                        return "rgb(60, 141, 188)";
                    case 0:
                        return "rgb(221, 75, 57)";
                    default:
                        return "rgb(0, 166, 90)";
                }
            }
            catch (Exception ex)
            {
                return "rgb(0, 166, 90)";
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getScheduleEvents()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                var rows = repObj.getAllSchedules(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                {
                    id = t1.ScheduleID,
                    title =t1.employee.Username+" : "+ t1.employee.Name,
                    Desc = t1.Notes,
                    start = t1.ScheduleDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    end = t1.ScheduleDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    StartTime =ConvertTOAMPM( t1.StartTime),
                    EndTime = ConvertTOAMPM( t1.EndTime),
                    TotalScheduledHours = t1.TotalScheduledHours,
                    Notes=t1.Notes,
                    CreatedAt=t1.CreatedAt,
                    BusinessID=t1.BusinessID,
                    color = employeeAtForColor(t1.EmployeeID),
                    //someKey = e.SomeImportantKeyID,
                    allDay = false
                }).ToArray();
                var ret = Json(rows, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }

        [HttpPost]
        public JsonResult addSchedule(string id, DateTime date,string color)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                //string s = "Mon Jan 13 2014 00:00:00 GMT+0000 (GMT Standard Time)";
                //string date1 = Convert.ToDateTime(date).ToString("MM/dd/yyyy hh:mm:ss"); 
                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDateTime"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                requestDate = requestDate.AddDays(1);
                schedule addAlertObj = new schedule();
                addAlertObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addAlertObj.CreatedBy = Session["EmployeeId"].ToString();
                addAlertObj.CreatedAt = requestDate;
                
                addAlertObj.EmployeeID = repObj.getEmployeeIdFromUserName(id.Split(':')[0].Trim());
                addAlertObj.ScheduleDate = date.AddDays(1);
                addAlertObj.StartTime = TimeSpan.Parse("09:00:00");
                addAlertObj.EndTime = TimeSpan.Parse("18:00:00");
                addAlertObj.TotalScheduledHours = "9";
                if (!repObj.isSchedulePresent(addAlertObj.EmployeeID, addAlertObj.ScheduleDate))
                {
                    bool st = repObj.addSchedule(addAlertObj);
                    if (st)
                        return Json(1, JsonRequestBehavior.AllowGet);
                    else
                        return Json(0, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "addSchedule");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteSchedule(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteSchedule(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult sendScheduleMail()
        {
            try
            {
                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDateTime"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                SmartHotelRepository repObj = new SmartHotelRepository();

                var senderEmail = new MailAddress(appKeys.AppSettings["MailFromAddress"], "Gamil");
                SmartHotelEntities dbModel = new SmartHotelEntities();

                var password = appKeys.AppSettings["MailPassword"];
                var sub = "Schedule";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                foreach (var item in repObj.getEmployees(Convert.ToInt32(Session["BusinessID"])))
                {
                    List<schedule> scheduleList = repObj.getNextSevenDaysSchedule(Convert.ToInt32(Session["BusinessID"]), requestDate, item.EmployeeID);
                    if (scheduleList!=null && scheduleList.Count>0)
                    {
                        MailMessage mail = new MailMessage();
                        mail.To.Add(item.Email.Trim().ToString());
                        mail.From = new MailAddress(appKeys.AppSettings["MailFromAddress"]);
                        var body = "Please find below schedule for next week<br/><br/>";
                        body += "<table border=\"1\"><tr><th>Date</th><th>Start Time</th><th>End Time</th><th>Total Hours</th></tr>";
                  foreach (var item1 in scheduleList)
                        {
                            body += "<tr><td>" +item1.ScheduleDate.ToString("MM/dd/yyyy") + "</td><td>" +ConvertTOAMPM(item1.StartTime) + "</td><td>" + ConvertTOAMPM(item1.EndTime) + "</td><td>" + item1.TotalScheduledHours + "</td></tr>";
                        }
                        body +="</table>";
                        mail.IsBodyHtml = true;
                        mail.Subject = sub;
                        mail.Body =FormateMail(body);

                        {
                            smtp.Send(mail);
                        }
                    }
                }
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [CheckSessionOut]
        public JsonResult editSchedule(string id, string starttime, string endtime, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                repObj.editSchedule(id, starttime, endtime, note);
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(2);
            }
        }

        #endregion

        #region Message
        [CheckSessionOut]
        public ActionResult Message()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("MESSAGING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                ViewBag.allEmployee = repObj.getEmployeesForMessage(Convert.ToInt32(Session["BusinessID"]), Convert.ToInt32(Session["EmployeeId"]));
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult getAllmessage()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                var ret = Json(new
                {
                    data = repObj.getAllmessage(Convert.ToInt32(Session["BusinessID"]), Convert.ToInt32(Session["EmployeeId"])).ToList().Select(t1 => new
                    {
                        LaundryId = t1.AllmessageID,
                        CreatedAt = string.Format("{0:MMMM dd hh:mm tt}", t1.CreatedAt),
                        MessageSentBy = t1.employee1.Name,
                        MessageSentByID = t1.MessageSentByID,
                        MessageSentByImage=t1.employee1.ImagePath != null ? t1.employee1.ImagePath : "/Content/Alphabets/" + t1.employee1.Name.Substring(0, 1).ToUpper() + ".jpg",
                        MessageRecievedBy = t1.employee.Name,
                        MessageRecievedByID = t1.MessageRecievedByID,
                        messageText = t1.messageText,
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                //    return Json(new
                //    {
                //         data = {new < int, string, string, int, string, int?, string, int, string, string, float?>[0]};
                //}, JsonRequestBehavior.AllowGet);
                return null;
            }
        }
        [HttpPost]
        
        [CheckSessionOut]
        public JsonResult addMessage(string sendId, string msgText,string msgTime)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                //string s = "Mon Jan 13 2014 00:00:00 GMT+0000 (GMT Standard Time)";
                //string date1 = Convert.ToDateTime(date).ToString("MM/dd/yyyy hh:mm:ss"); 
                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(msgTime, formats, new CultureInfo("en-US"), DateTimeStyles.None);

                allmessage addMsgObj = new allmessage();
                addMsgObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addMsgObj.MessageSentByID = Convert.ToInt32(Session["EmployeeId"].ToString());
                addMsgObj.CreatedAt = requestDate;
                addMsgObj.messageText = msgText;
                addMsgObj.MessageRecievedByID = Convert.ToInt32(sendId);
                addMsgObj.isRead = 0;
                bool st = repObj.addMessage(addMsgObj);
                    if (st)
                        return Json(1, JsonRequestBehavior.AllowGet);
                    else
                        return Json(0, JsonRequestBehavior.AllowGet);
                
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "addMessage");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getUnreadCount()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                var ret = Json(new
                {
                    data = repObj.getEmployeeMessageCounter(Convert.ToInt32(Session["BusinessID"]), Convert.ToInt32(Session["EmployeeId"])).ToArray()
            }, JsonRequestBehavior.AllowGet);
                return ret;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult readAllMessage(int id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                bool status = repObj.readAllMessage(Convert.ToInt32(Session["BusinessID"]), Convert.ToInt32(Session["EmployeeId"]), id);
                if (status)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);

                return Json(0, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        
        #region Reports
        [CheckSessionOut]
        public ActionResult ReportHousekeeping()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                ViewBag.EmployeePerformance = repObj.HKPerformance(Convert.ToInt32(Session["BusinessID"]));
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [CheckSessionOut]
        public ActionResult ReportMaintenance()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                ViewBag.EmployeePerformance = repObj.MPPerformance(Convert.ToInt32(Session["BusinessID"]));
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        public ActionResult ExpertToExcel(string reportType)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                if (reportType.Equals("HKReport"))
                {
                    DataTable dt = new DataTable("HouseKeeper Report");
                    dt.Columns.AddRange(new DataColumn[7] {
 new DataColumn("Employee Name"),
 new DataColumn("AvgRoomPerDay"),
 new DataColumn("AvgTimePerRoom"),
 new DataColumn("AvgRoomPerDay Weekly"),
 new DataColumn("AvgTimePerRoom Weekly"),
 new DataColumn("AvgRoomPerDay Monthly"),
 new DataColumn("AvgTimePerRoom Monthly")
            });
                    foreach (var HKReportObj in repObj.HKPerformance(Convert.ToInt32(Session["BusinessID"])))
                    {
                        // var ve   hicles = vehicle.find(driver.Id);         
                        dt.Rows.Add(
                            HKReportObj.Name,
                            HKReportObj.avgRoomPerDay,
                            HKReportObj.avgTimePerRoom,
                            HKReportObj.avgRoomPerDayWeekly,
                            HKReportObj.avgTimePerRoomWeekly,
                            HKReportObj.avgRoomPerDayMonthly,
                            HKReportObj.avgTimePerRoomMonthly);
                    }
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "HouseKeeperReport.xlsx");
                        }
                    }
                    return RedirectToAction("ReportHousekeeping");
                }
                else
                {
                    DataTable dt = new DataTable("Maintenance Report");
                    dt.Columns.AddRange(new DataColumn[4] {
 new DataColumn("Employee Id"),
 new DataColumn("Employee Name"),
 new DataColumn("AvgRoomPerDay"),
 new DataColumn("AvgTimePerRoom")
            });
                    foreach (var HKReportObj in repObj.MPPerformance(Convert.ToInt32(Session["BusinessID"])))
                    {
                        // var ve   hicles = vehicle.find(driver.Id);         
                        dt.Rows.Add(
                            HKReportObj.employeeId,
                            HKReportObj.Name,
                            HKReportObj.avgRoomPerDay,
                            HKReportObj.avgTimePerRoom);
                    }
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "MaintenanceReport.xlsx");
                        }
                    }
                    return RedirectToAction("ReportMaintenance");
                }



            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";
                if (reportType.Equals("HKReport"))
                {
                    return RedirectToAction("ReportHousekeeping");
                }
                else
                {
                    return RedirectToAction("ReportMaintenance");
                }
            }
        }

        #endregion

        #region Inspection
        [CheckSessionOut]
        public ActionResult InspectionList()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("HOUSEKEEPING") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();
            
            return View();
        }
        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllInspectionAjax()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getAllInspection(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        InspectionID = t1.InspectionID,
                        RoomNumber = t1.room != null ? t1.room.RoomNumber.ToString() : "",
                        CreatedAt = t1.CreatedAt.ToString("MM/dd/yyyy hh:mm tt"),
                        RoomId = t1.RoomID,
                        Duty = t1.roomduty != null ? t1.roomduty.Duty : "",
                        RoomDutiesID = t1.RoomDutiesID,
                        Name = t1.Name != null ? t1.Name : "",
                        Picture1 = t1.ImgPath,
                        Result = getInspectionRating(t1.Result),
                        InspectedTo = t1.employee1 != null ? t1.employee1.Name : "",
                        InspectedToID = t1.InspectedToID,
                        comments = t1.comments
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                return null;
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteInspectionAjax(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteInspection(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }

        string getInspectionRating(float rate)
        {
            try
            {
                string result = "";
                if(rate>4)
                    result= "EXCELLENT ["+rate+"]";
                else if (rate > 3)
                    result = "GOOD [" + rate + "]";
                else if (rate > 2)
                    result = "FAIR [" + rate + "]";
                else if (rate > 1)
                    result = "POOR [" + rate + "]";
                else if (rate >= 0)
                    result = "NOT EXPECTABLE [" + rate + "]";
                return result;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public ActionResult ExpertToExcelInspectionList()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                DataTable dt = new DataTable("InspectionList");
                dt.Columns.AddRange(new DataColumn[8] {
new DataColumn("RoomNumber"),
new DataColumn("CreatedAt"),
new DataColumn("Duty"),
new DataColumn("Name"),
new DataColumn("Picture1"),
new DataColumn("Result"),
new DataColumn("InspectedTo"),
new DataColumn("comments")


            });
                foreach (var t1 in repObj.getAllInspection(Convert.ToInt32(Session["BusinessID"])))
                {
                    // var ve   hicles = vehicle.find(driver.Id);         
                    dt.Rows.Add(
t1.room!=null ? t1.room.RoomNumber.ToString() : "",
t1.CreatedAt.ToString("MM/dd/yyyy hh:mm tt"),
t1.roomduty!=null ? t1.roomduty.Duty : "",
t1.Name!=null ? t1.Name : "",
t1.ImgPath,
getInspectionRating(t1.Result),
t1.employee1!=null ? t1.employee1.Name : "",
t1.comments


);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InspectionList.xlsx");
                    }
                }
                return RedirectToAction("InspectionList");




            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";

                return RedirectToAction("InspectionList");

            }
        }

        #endregion

        #region Guest Messeging
        [CheckSessionOut]
        public ActionResult GuestList()
        {
            if (Session["BusinessID"] == null || Session["BusinessID"] == null)
            {
                return RedirectToAction("Login");
            }
            if (!(Session["SubscribedModules"].ToString().Contains("GUEST APP") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
            {
                return HttpNotFound();
            }
            SmartHotelRepository repObj = new SmartHotelRepository();
            ViewBag.AllRooms = repObj.getAllRooms(Convert.ToInt32(Session["BusinessID"]));
            return View();
        }

        [HttpPost]
        public JsonResult AddGuestAjaxA(string id,string name, string phone, string email, string startDate, string endDate, string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();

                string[] formats = { "MM/dd/yyyy" };
                DateTime requestDate = DateTime.ParseExact(Session["reduestedDate"].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                DateTime stayDateStart = new DateTime();
                DateTime stayDateEnd = new DateTime();
                string[] format1 = { "MM-dd-yyyy HH:mm:ss" };
                if (!string.IsNullOrEmpty(startDate))
                    stayDateStart = DateTime.ParseExact(startDate, format1, new CultureInfo("en-US"), DateTimeStyles.None);
                if (!string.IsNullOrEmpty(endDate))
                    stayDateEnd = DateTime.ParseExact(endDate, format1, new CultureInfo("en-US"), DateTimeStyles.None);
                guest addGuestObj = new guest();
                guestbusiness addGuestBusinessObj = new guestbusiness();
                addGuestBusinessObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addGuestBusinessObj.AddedBy = Convert.ToInt32(Session["EmployeeId"]);
                addGuestBusinessObj.isActive = "Active";
                addGuestBusinessObj.isDeleted = 0;
                if (!string.IsNullOrEmpty(id))
                    addGuestBusinessObj.RoomID = Convert.ToInt32(id);

                addGuestObj = repObj.getGuestByNumber(phone);
                bool st = false;
                if (addGuestObj != null)
                {
                    if (name != null)
                        addGuestObj.Name = name;
                    if (!string.IsNullOrEmpty(phone))
                        addGuestObj.phone = phone;
                    if (!string.IsNullOrEmpty(email))
                        addGuestObj.Email = email;
                    st = repObj.editGuest(addGuestObj);
                }
                else
                {
                    addGuestObj = new guest();
                    if (name != null)
                        addGuestObj.Name = name;
                    if (!string.IsNullOrEmpty(phone))
                        addGuestObj.phone = phone;
                    if (!string.IsNullOrEmpty(email))
                        addGuestObj.Email = email;
                    st = repObj.addGuest(addGuestObj);
                }
                if (!string.IsNullOrEmpty(startDate))
                    addGuestBusinessObj.StayDateStart = stayDateStart;
                if (!string.IsNullOrEmpty(endDate))
                    addGuestBusinessObj.StayDateEnd = stayDateEnd;
                if (!string.IsNullOrEmpty(note))
                    addGuestBusinessObj.Note = note;
                string random = "";
                while (true)
                {
                    Random r = new Random();
                    int randNum = r.Next(1000000);
                    random = randNum.ToString("D6");
                    if (repObj.checkGuestTokenPresent(random))
                        break;
                }
                addGuestBusinessObj.Token = random;
                if (st)
                {
                    addGuestBusinessObj.GuestID = addGuestObj.GuestID;
                    bool st1 = repObj.addGuestBusiness(addGuestBusinessObj);
                    if (st1)
                    {
                        // Find your Account Sid and Auth Token at twilio.com/console
                        try
                        {
                            const string accountSid = "AC8b83eb1cc0576a24eea2e4cb2d84915c";
                            const string authToken = "cdf0c8afc65f3e55e3ceec6b7d2c0997";
                            TwilioClient.Init(accountSid, authToken);

                            var to = new PhoneNumber("+1" + addGuestObj.phone);
                            var message = MessageResource.Create(
                                to,
                                from: new PhoneNumber("+13236761692"),
                                body: "To download BSmartHotel guest application goto " + appKeys.AppSettings["GuestMobielAppLink"] + "Link. Your Access Code is : " + addGuestBusinessObj.Token);

                        }
                        catch (Exception ex)
                        {
                            ExceptionLogining.SendErrorToText(ex, "AddGuest Send MEssage");
                            return Json(2, JsonRequestBehavior.AllowGet);
                        }
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "Add Guest");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllGuestAjax()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getAllGuest(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        GuestID = t1.GuestID,
                        GuestBusinessID=t1.GuestBusinessID,
                        RoomNumber = t1.room != null ? t1.room.RoomNumber.ToString() : "",
                        phone = t1.guest.phone,
                        Email=t1.guest.Email,
                        RoomId = t1.RoomID,
                        Name = t1.guest.Name != null ? t1.guest.Name : "",
                        StayDateStart = t1.StayDateStart!=null?((DateTime)t1.StayDateStart).ToString("MM/dd/yyyy"):"",
                        StayDateEnd = t1.StayDateEnd != null ? ((DateTime)t1.StayDateEnd).ToString("MM/dd/yyyy") : "",
                        UnreadCount =repObj.getGuestMessageUnreadCount(t1.GuestBusinessID),
                        AddedBy = t1.employee != null ? t1.employee.Name : "",
                        AddedByID = t1.AddedBy,
                        Note = t1.Note,
                        isDeleted=t1.isDeleted,
                        isActive=t1.isActive
                    }).OrderByDescending(s=>s.UnreadCount).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getAllGuestAjax");
                return null;
            }
        }

        [CheckSessionOut]
        public JsonResult editGuestAjax(string id, string bid, string roomid, string name, string phone, string email, string startdate, string enddate,string note)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                string[] formats = { "yyyy-MM-dd" };
                DateTime startDate = new DateTime();
                if (!string.IsNullOrEmpty(startdate))
                    startDate = DateTime.ParseExact(startdate, formats, new CultureInfo("en-US"), DateTimeStyles.None);

                DateTime endDate = new DateTime();
                if (!string.IsNullOrEmpty(enddate))
                    endDate = DateTime.ParseExact(enddate, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                guest addGuestObj = repObj.getGuestById(Convert.ToInt32(id));
                guestbusiness addGuestBusinessObj= repObj.getGuestBusinessById(Convert.ToInt32(bid));
                //addGuestObj.AddedBy = Convert.ToInt32(Session["EmployeeId"]);
                addGuestBusinessObj.RoomID = Convert.ToInt32(roomid);
                if (name != null)
                    addGuestObj.Name = name;
                if (!string.IsNullOrEmpty(phone))
                    addGuestObj.phone = phone;
                if (!string.IsNullOrEmpty(email))
                    addGuestObj.Email = email;
                if (!string.IsNullOrEmpty(startdate))
                    addGuestBusinessObj.StayDateStart = startDate;
                if (!string.IsNullOrEmpty(enddate))
                    addGuestBusinessObj.StayDateEnd = endDate;
                if (!string.IsNullOrEmpty(note))
                    addGuestBusinessObj.Note = note;
                bool st = repObj.updateGuest(addGuestObj);
                bool st1= repObj.updateGuest(addGuestObj);
                if (st && st1)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteGuest(string id, string bid)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteBusinessGuest(Convert.ToInt32(bid));
                if (st)
                {
                    repObj.readAllGuestMessage(Convert.ToInt32(bid));
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }

        [CheckSessionOut]
        public ActionResult GuestMessages(int id)
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("GUEST APP") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();
                repObj.readAllGuestMessage(id);
                return View(repObj.getGuestBusinessById(id));
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult ActivateDeactivateGuest(string id, string action)
        {

            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(action))
                {
                    bool status = repObj.ActivateDeactivateGuest(Convert.ToInt32(id), action);

                    if (status)
                    {
                        return Json(1);
                    }
                    else
                    {
                        return Json(0);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "ActivateDeactivateGuest");
            }
            return Json(0);
        }

        [CheckSessionOut]
        public JsonResult addGuestMessage(string sendId, string msgText, string msgTime)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                //string s = "Mon Jan 13 2014 00:00:00 GMT+0000 (GMT Standard Time)";
                //string date1 = Convert.ToDateTime(date).ToString("MM/dd/yyyy hh:mm:ss"); 
                string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                DateTime requestDate = DateTime.ParseExact(msgTime, formats, new CultureInfo("en-US"), DateTimeStyles.None);

                guestmessage addMsgObj = new guestmessage();
                addMsgObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);

                addMsgObj.MessageSentByID = Convert.ToInt32(sendId);
                addMsgObj.CreatedAt = requestDate;
                addMsgObj.messageText = msgText;
                addMsgObj.isRead = 0;
                addMsgObj.messageTtype = "to";
                bool st = repObj.addGuestMessage(addMsgObj);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);

                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "addGuestMessage");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getAllGuestmessage(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getAllGuestmessage(Convert.ToInt32(id)).ToList().Select(t1 => new
                    {
                        guestmessageID = t1.guestmessageID,
                        CreatedAt = string.Format("{0:MMMM dd hh:mm tt}", t1.CreatedAt),
                        MessageSentBy = t1.guestbusiness.guest.Name,
                        MessageSentByID = t1.MessageSentByID,
                        MessageSentByImage = "/Content/Alphabets/" + t1.guestbusiness.guest.Name.Substring(0, 1).ToUpper() + ".jpg",
                        messageText = t1.messageText,
                        messageTtype=t1.messageTtype,
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getTimeOffRequests");
                //    return Json(new
                //    {
                //         data = {new < int, string, string, int, string, int?, string, int, string, string, float?>[0]};
                //}, JsonRequestBehavior.AllowGet);
                return null;
            }
        }

        [HttpPost]
        [CheckSessionOut]
        public JsonResult getExistingGuestDetails(string phone)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                guest obj = repObj.getGuestByNumber(phone);
                if (obj != null)
                {
                    var ret = Json(new
                    {

                        data = new
                        {
                            phone = obj.phone,
                            Name = obj.Name,
                            Email = obj.Email
                        }
                    }, JsonRequestBehavior.AllowGet);
                    return ret;
                }
                return null;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getExistingGuestDetails");
                //    return Json(new
                //    {
                //         data = {new < int, string, string, int, string, int?, string, int, string, string, float?>[0]};
                //}, JsonRequestBehavior.AllowGet);
                return null;
            }
        }

        public ActionResult ExpertToExcelGuestList(string reportType)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                
                    DataTable dt = new DataTable("GuestList");
                    dt.Columns.AddRange(new DataColumn[10] {
new DataColumn("RoomNumber"),
new DataColumn("phone"),
new DataColumn("Email"),
new DataColumn("Name"),
new DataColumn("StayDateStart"),
new DataColumn("StayDateEnd"),
new DataColumn("AddedBy"),
new DataColumn("Note"),
new DataColumn("isDeleted"),
new DataColumn("isActive"),

            });
                    foreach (var t1 in repObj.getAllGuest(Convert.ToInt32(Session["BusinessID"])))
                    {
                        // var ve   hicles = vehicle.find(driver.Id);         
                        dt.Rows.Add(
 t1.room != null ? t1.room.RoomNumber.ToString() : "",
 t1.guest.phone,
t1.guest.Email,
 t1.guest.Name != null ? t1.guest.Name : "",
 t1.StayDateStart != null ? ((DateTime)t1.StayDateStart).ToString("MM/dd/yyyy") : "",
 t1.StayDateEnd != null ? ((DateTime)t1.StayDateEnd).ToString("MM/dd/yyyy") : "",
 t1.employee != null ? t1.employee.Name : "",
 t1.Note,
t1.isDeleted,
t1.isActive
);
                    }
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "GuestList.xlsx");
                        }
                    }
                    return RedirectToAction("GuestList");
                



            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Something goes wrong Please try again";
                
                    return RedirectToAction("GuestList");
                
            }
        }

        #endregion

        #region Guest Amenties
        [CheckSessionOut]
        public ActionResult GuestAmenties()
        {
            try
            {
                if (Session["BusinessID"] == null || Session["BusinessID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (!(Session["SubscribedModules"].ToString().Contains("GUEST APP") && ("OWNER;MANAGER;FRONTDESK;".Contains(Session["JobTitle"].ToString()))))
                {
                    return HttpNotFound();
                }
                SmartHotelRepository repObj = new SmartHotelRepository();

                ViewBag.getAllLaundryStatus = repObj.getAllLaundryStatus();
                ViewBag.AllLaundryPerson = repObj.getAllLaundryPersonOfBusiness(Convert.ToInt32(Session["BusinessID"]));
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [HttpGet]
        [CheckSessionOut]
        public JsonResult getAllAmenties()
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                var ret = Json(new
                {
                    data = repObj.getAllAmenties(Convert.ToInt32(Session["BusinessID"])).ToList().Select(t1 => new
                    {
                        AmentiesID = t1.AmentiesID,
                        Description = t1.AmentiesText
                    }).ToArray()
                }, JsonRequestBehavior.AllowGet);
                return ret;// Json(repObj.getAllTiemCards().Select(a=>a.TimeCardID).ToArray() , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "getAllAmenties");
                //    return Json(new
                //    {
                //         data = {new < int, string, string, int, string, int?, string, int, string, string, float?>[0]};
                //}, JsonRequestBehavior.AllowGet);
                return null;
            }
        }
        [CheckSessionOut]
        public JsonResult editAmenties(string id, string description)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.editAmenties(Convert.ToInt32(id), description);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult deleteAmenties(string id)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                bool st = repObj.deleteAmenties(Convert.ToInt32(id));
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionOut]
        public JsonResult AddAmenties(string description)
        {
            try
            {
                SmartHotelRepository repObj = new SmartHotelRepository();
                
                amenty addAlertObj = new amenty();
                addAlertObj.BusinessID = Convert.ToInt32(Session["BusinessID"]);
                
                if (!string.IsNullOrEmpty(description))
                    addAlertObj.AmentiesText = description;

                bool st = repObj.addAmenties(addAlertObj);
                if (st)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "AddLaundry");
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Help
        [HttpPost]
        public JsonResult sendHelpMail(string datet,string comment)
        {
            try
            {
                //string username = collection["enteredusername"].ToString().Trim();
                if (!string.IsNullOrEmpty(datet) && !string.IsNullOrEmpty(comment))
                {
                    
                                string body = FormateMail(" Business Name : "+ Session["BusinessName"] + "<br/> BusinessNumber : " + Session["BusinessNumber"]+" <br/> Employee Name : " + Session["EmployeeName"] + "<br/> Schedule date time : "+datet+" <br/> Comment : "+comment);
                                MailMessage mail = new MailMessage();

                                var senderEmail = new MailAddress(appKeys.AppSettings["MailFromAddress"], "Gamil");
                                mail.To.Add("sales@bsmarthotel.com");
                    mail.From = new MailAddress(appKeys.AppSettings["MailFromAddress"]);

                                var password = appKeys.AppSettings["MailPassword"];
                                var smtp = new SmtpClient
                                {
                                    Host = "smtp.gmail.com",
                                    Port = 587,
                                    EnableSsl = true,
                                    DeliveryMethod = SmtpDeliveryMethod.Network,
                                    UseDefaultCredentials = false,
                                    Credentials = new NetworkCredential(senderEmail.Address, password)
                                };
                                mail.Subject = "Schedule a meeting";
                                mail.Body = body;
                                mail.IsBodyHtml = true;
                                {
                                    smtp.Send(mail);
                                }
                                ViewBag.Message = "Please check your register email for reset password link";

                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "sendHelpMail");
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Feedback
        [HttpPost]
        public JsonResult sendFeedback(string datet, string comment)
        {
            try
            {
                //string username = collection["enteredusername"].ToString().Trim();
                if (!string.IsNullOrEmpty(datet) && !string.IsNullOrEmpty(comment))
                {
                    string[] formats = { "MM/dd/yyyy HH:mm:ss" };
                    DateTime requestDate = DateTime.ParseExact(datet, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                    SmartHotelRepository repobj = new SmartHotelRepository();
                    if(repobj.addFeedback(Convert.ToInt32(Session["BusinessID"]),requestDate,comment))
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogining.SendErrorToText(ex, "sendFeedback");
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Invoice
        [HttpGet]
        public ActionResult invoice()
        {
            try
            {
                SmartHotelRepository repobj = new SmartHotelRepository();
                business businessObj = repobj.getBusiness(Convert.ToInt32(Session["BusinessID"]));
                payment p1 = repobj.getLastPayment(Convert.ToInt32(Session["BusinessID"]));
                if(p1==null)
                {
                    ViewData["Message"] = "There is no invoice";
                    return View("ErrorPage");
                }
                if (!string.IsNullOrEmpty(businessObj.PlanType)&& businessObj.PlanType.Equals("Full"))
                {
                    ViewBag.SubTotal = 6* businessObj.TotalRooms;
                }
                else if (!string.IsNullOrEmpty(businessObj.PaymentPlan) && businessObj.PaymentPlan.Equals("Yearly"))
                {
                    ViewBag.SubTotal = p1.Amount / 12;
                }
                else
                {
                    ViewBag.SubTotal = p1.Amount;
                }
                ViewBag.SelectedModule = repobj.getBusinessModules(Convert.ToInt32(Session["BusinessID"]));
                ViewBag.PaymentObj = p1;
                return View(businessObj);
            }
            catch (Exception ex)
            {
                ViewData["Message"] = "There is no invoice";
                return View("ErrorPage");
            }
        }

        #endregion

        public static string Encryptdata(string password)
        {
            return Cipher.Encrypt(password, appKeys.AppSettings["CipherKey"]);

        }
        public static string Decryptdata(string encryptpwd)
        {
            return Cipher.Decrypt(encryptpwd, appKeys.AppSettings["CipherKey"]);
        }


    }
    public static class Cipher
    {
        /// <summary>
        /// Encrypt a string.
        /// </summary>
        /// <param name="plainText">String to be encrypted</param>
        /// <param name="password">Password</param>
        public static string Encrypt(string plainText, string password)
        {
            if (plainText == null)
            {
                return null;
            }

            if (password == null)
            {
                password = String.Empty;
            }

            // Get the bytes of the string
            var bytesToBeEncrypted = Encoding.UTF8.GetBytes(plainText);
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesEncrypted = Cipher.Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(bytesEncrypted);
        }

        /// <summary>
        /// Decrypt a string.
        /// </summary>
        /// <param name="encryptedText">String to be decrypted</param>
        /// <param name="password">Password used during encryption</param>
        /// <exception cref="FormatException"></exception>
        public static string Decrypt(string encryptedText, string password)
        {
            if (encryptedText == null)
            {
                return null;
            }

            if (password == null)
            {
                password = String.Empty;
            }

            // Get the bytes of the string
            var bytesToBeDecrypted = Convert.FromBase64String(encryptedText);
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesDecrypted = Cipher.Decrypt(bytesToBeDecrypted, passwordBytes);

            return Encoding.UTF8.GetString(bytesDecrypted);
        }

        private static byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }

                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        private static byte[] Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }

                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }
    }
}