﻿
using SmartHotelDataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHotelDataAccessLayer
{
    public class SmartHotelRepository
    {
        SmartHotelEntities DbCo = new SmartHotelEntities();

        #region Client Methods

        public int getProrityIdFromPriority(string pr)
        {
            try
            {
                return DbCo.priorities.Where(p => p.Priority1.Equals(pr)).Select(p => p.PriorityID).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public business getBusiness(int businessId)
        {
            try
            {
                return DbCo.businesses.Where(b => b.BusinessID == businessId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<businessmodule> getBusinessModules(int businessId)
        {
            try
            {
                return DbCo.businessmodules.Where(m => m.BusinessID == businessId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region payment
        public payment getLastPayment(int businessId)
        {
            try
            {
                return DbCo.payments.Where(p => p.BusinessID == businessId).OrderByDescending(p => p.PaymentDate).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Employye details
        public employee getOwnerOfBusiness(int businessId)
        {
            try
            {
                int ownerid = getJobTitleIdFromJobTitle("Owner");
                if(ownerid>0)
                {
                    return DbCo.employees.Where(e => e.BusinessID == businessId && e.JobTitleID == ownerid).FirstOrDefault<employee>();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public object getEmployeeDetails(int id)
        {
            try
            {
                object empObj = DbCo.employees.Where(e => e.EmployeeID == id).ToList().Select(a => new { a.EmployeeToken, a.EmployeeID, a.Name, a.Email, a.Phone, a.JobTitleID, JobTitle = getJobTitleFromJobTitleId(a.JobTitleID), a.BusinessID, a.CreatedAt, Module = getEmpBusinessModuleName(a.BusinessID,a.jobtitle.JobTitle1), Latitude = getLatitudeFromBusiness(a.BusinessID), Longitude = getLongitudeFromBusiness(a.BusinessID), ImagePath=a.ImagePath }).FirstOrDefault();
                return empObj;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }
        
        public employee login(string username,string password)
        {
            try
            {
                return DbCo.employees.Where(e => e.Username.ToLower().Equals(username.ToLower()) && e.Password.Equals(password) && e.isActive.Equals("Active") && e.business.IsVerified==1 && e.business.Status.Equals("Active")).FirstOrDefault<employee>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<employee> getEmployees(int businessID)
        {
            try
            {
                return DbCo.employees.Where(b => b.BusinessID == businessID).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<employee> getEmployeesWithoutOwner(int businessID)
        {
            try
            {
                int jobtitleid = getJobTitleIdFromJobTitle("OWNER");
                return DbCo.employees.Where(b => b.BusinessID == businessID && b.JobTitleID!=jobtitleid).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool updateEmployee(employee empObj)
        {
            try
            {
                DbCo.Entry(empObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool deleteEmployee(int empid)
        {
            try
            {
                employee empObj = DbCo.employees.Where(e => e.EmployeeID == empid).FirstOrDefault<employee>();
                DbCo.employees.Remove(empObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool addEmployee(employee empObj)
        {
            try
            {
                DbCo.employees.Add(empObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addEmployee");
                return false;
            }
        }

        public int getEmployeeIdFromUserName(string username)
        {
            try
            {
                int empid = -1;
                if (!string.IsNullOrEmpty(username))
                {
                    empid = DbCo.employees.Where(e => e.Username == username).Select(n => n.EmployeeID).FirstOrDefault<int>();
                }
                return empid;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getEmployeeIdFromUserName" + username );
                return -1;
            }
        }

        public string getEmployeeToken(int empId)
        {
            try
            {
                return DbCo.employees.Where(e => e.EmployeeID == empId).Select(e => e.EmployeeToken).FirstOrDefault<string>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool checkOldPassword(int empId,string password)
        {
            try
            {
                if (DbCo.employees.Where(e => e.EmployeeID == empId && e.Password.Equals(password)).FirstOrDefault<employee>() != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public String getEmployeeNameFromEmpId(int id)
        {
            return DbCo.employees.Where(e => e.EmployeeID == id).Select(s => s.Name).FirstOrDefault<string>();
        }

        public bool isEmployeeActive(int empid)
        {
            try
            {
                return DbCo.employees.Where(e => e.EmployeeID == empid).Select(s => s.isActive).FirstOrDefault<string>().Equals("Active");
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public employee getEmployeeFromEmpId(int empid)
        {
            try
            {
                return DbCo.employees.Where(e => e.EmployeeID == empid).FirstOrDefault<employee>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public employee getEmployeeFromResetLink(string link)
        {
            try
            {
                return DbCo.employees.Where(e => e.resetLink == link).FirstOrDefault<employee>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Business Details
        public string getLatitudeFromBusiness(int id)
        {
            try
            {
                string latitude = DbCo.businesses.Where(e => e.BusinessID == id).Select(a => a.latitude).FirstOrDefault<string>();
                return latitude;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string getLongitudeFromBusiness(int id)
        {
            try
            {
                string longitude = DbCo.businesses.Where(e => e.BusinessID == id).Select(a => a.longitude).FirstOrDefault<string>();
                return longitude;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public object getBusinessModuleName(int businessId)
        {
            try
            {
                List<string> module = new List<string>();

                var moduleList = (from moduleObj in DbCo.modules
                                  join busmodObj in DbCo.businessmodules
                                  on moduleObj.ModuleID equals busmodObj.ModuleID
                                  where busmodObj.BusinessID == businessId
                                  select new { moduleObj.ModuleName }).ToList();

                foreach (var modu in moduleList)
                {
                    module.Add(modu.ModuleName);
                }

                return module;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public object getEmpBusinessModuleName(int businessId,string jobTitle)
        {
            try
            {
                List<string> module = new List<string>();
                List<string> jobtitlesallow = new List<string> { "HEAD HOUSEKEEPER", "HOUSEKEEPER", "LAUNDRY PERSON", "MAINTENANCE PERSON", "INSPECTOR" };
                List<string> jobtitlesallow1 = new List<string> { "MANAGER", "FRONTDESK"};
                List<string> allowModuleList = new List<string> { "TIME CLOCK", "SCHEDULING","MESSAGING","TIMEOFF" };
                var moduleList = (from moduleObj in DbCo.modules
                                  join busmodObj in DbCo.businessmodules
                                  on moduleObj.ModuleID equals busmodObj.ModuleID
                                  where busmodObj.BusinessID == businessId
                                  select new { moduleObj.ModuleName }).ToList();

                foreach (var modu in moduleList)
                {
                    if(jobtitlesallow.Contains(jobTitle))
                    {
                        module.Add(modu.ModuleName);
                    }
                    else if(jobtitlesallow1.Contains(jobTitle) && allowModuleList.Contains(modu.ModuleName))
                    {
                        module.Add(modu.ModuleName);
                    }

                }
                
                return module;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Job Details
        public string getJobTitleFromJobTitleId(int jobTitleId)
        {
            try
            {
                return DbCo.jobtitles.Where(j => j.JobTtitleID == jobTitleId).Select(j => j.JobTitle1).FirstOrDefault<string>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int getJobTitleIdFromJobTitle(string JobTitle)
        {
            try
            {
                return DbCo.jobtitles.Where(j => j.JobTitle1 == JobTitle).Select(j => j.JobTtitleID).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public List<jobtitle> getAllJobTitle()
        {
            try
            {
                return DbCo.jobtitles.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region TimeCard Details
        public object getTimeCardFromID(int id)
        {
            timecard t1 = DbCo.timecards.Where(t => t.TimeCardID == id).FirstOrDefault<timecard>();
            object retobj = new
            {
                TimeCardID = t1.TimeCardID,
                BusinessID = t1.BusinessID,
                EmployeeID = t1.EmployeeID,
                WorkDate = t1.WorkDate.ToString(),
                ClockInTime = t1.ClockInTime.ToString(),
                LunchInTime = t1.LunchInTime.ToString(),
                LunchOutTime = t1.LunchOutTime.ToString(),
                ClockOutTime = t1.ClockOutTime.ToString(),
                TotalLunchTime = t1.TotalLunchTime,
                TotalWorkedHour = t1.TotalWorkedHour,
                Notes = t1.Notes
            };
            return retobj;
        }

        public List<timecard> getAllTiemCards(int businessID,DateTime requestDate)
        {
            return DbCo.timecards.Where(b => b.BusinessID == businessID).ToList().Where(l => l.WorkDate.Year == requestDate.Year && l.WorkDate.Month == requestDate.Month && l.WorkDate.Date == requestDate.Date).ToList();
        }
        public int getTotalClockedINCount(int businessID,DateTime requestDate)
        {
            try
            {
                int cou = DbCo.timecards.Where(b => b.BusinessID == businessID && b.ClockInTime != null).ToList().Where(l => l.WorkDate.Year == requestDate.Year && l.WorkDate.Month == requestDate.Month && l.WorkDate.Date == requestDate.Date).ToList().Count;
                return cou;
            }
            catch
            {
                return 0;
            }
            

        }
        
        public bool editTimeCards(string id, string clockintime, string clockouttime, string lunchintime, string lunchouttime, string note)
        {

            try
            {
                int timeCardID = Convert.ToInt32(id);
                timecard tObj = DbCo.timecards.Where(b => b.TimeCardID == timeCardID).FirstOrDefault<timecard>();

                tObj.ClockInTime = stringToTimeSpanHM(clockintime);
                tObj.ClockOutTime = stringToTimeSpanHM(clockouttime);
                tObj.LunchInTime = stringToTimeSpanHM(lunchintime);
                tObj.LunchOutTime = stringToTimeSpanHM(lunchouttime);
                tObj.TotalLunchTime = (tObj.LunchInTime != null && tObj.LunchOutTime != null) ? (((TimeSpan)tObj.LunchOutTime).TotalMinutes - ((TimeSpan)tObj.LunchInTime).TotalMinutes).ToString() : null;
                tObj.TotalWorkedHour = (tObj.ClockInTime != null && tObj.ClockOutTime != null) ? Math.Round((((TimeSpan)tObj.ClockOutTime - (TimeSpan)tObj.ClockInTime).TotalHours)- Convert.ToDouble(tObj.TotalLunchTime)/60.0, 1).ToString() : null;
                tObj.Notes = string.IsNullOrEmpty(note) ? null : note;
                DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public TimeSpan? stringToTimeSpan(string time)
        {
            try
            {
                return DateTime.ParseExact(time, "hh:mm:ss tt", CultureInfo.InvariantCulture).TimeOfDay;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "stringToTimeSpan"+time);
                return null;
            }

        }
        public TimeSpan? stringToTimeSpanHM(string time)
        {
            try
            {
                return DateTime.ParseExact(time, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "stringToTimeSpan" + time);
                return null;
            }

        }
        public string ConvertTOAMPM(TimeSpan? t)
        {
            try
            {
                if (t != null)
                {
                    DateTime time = DateTime.Today.Add((TimeSpan)t);
                    return time.ToString("hh:mm:ss tt");
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "ConvertTOAMPM TimeSpan" + t);
            }
            return "";
        }
        public string ConvertTOAMPM(DateTime? t)
        {
            try
            {
                if (t != null)
                {
                    DateTime time = (DateTime)t;
                    return time.ToString("MM/dd/yyyy hh:mm:ss tt");
                }
                return "";
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "ConvertTOAMPM datetime" + t);
                return "";
            }
        }
        public bool editTimeCardNotes(string id, string note)
        {

            try
            {
                int timeCardID = Convert.ToInt32(id);
                timecard tObj = DbCo.timecards.Where(b => b.TimeCardID == timeCardID).FirstOrDefault<timecard>();


                tObj.Notes = string.IsNullOrEmpty(note) ? null : note;
                DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool deleteTimeCard(string id)
        {

            try
            {
                int timeCardID = Convert.ToInt32(id);
                timecard tObj = DbCo.timecards.Where(b => b.TimeCardID == timeCardID).FirstOrDefault<timecard>();

                DbCo.timecards.Remove(tObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<timecard> getAllTiemCardsFromStart(int businessID, DateTime startDate,DateTime endDate)
        {
            return DbCo.timecards.Where(b => b.BusinessID == businessID).ToList().Where(l => l.WorkDate>=startDate && l.WorkDate<=endDate).OrderBy(b=>b.WorkDate).ToList();
        }
        #endregion

        #region TimeOff Details
        int GetStatusOrder(string status)
        {
            switch (status)
            {
                case "Pending": return 1;
                case "Approve": return 2;
                case "Reject": return 3;
                default: return 4;
            }
        }
        public List<timeoffrequest> getTimeOffRequests(int businessID)
        {
            try
            {
                return DbCo.timeoffrequests.Where(b => b.BusinessID == businessID).ToList().OrderBy(r => GetStatusOrder(r.Status)).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int getTotalTimeOffRequests(int businessID)
        {
            try
            {
                int cou = DbCo.timeoffrequests.Where(b => b.BusinessID == businessID && b.Status.Equals("Pending")).ToList().Count;
                return cou;
            }
            catch
            {
                return 0;
            }

        }
        public bool approveRejectTimeOffRequest(string id, string status, string comment,int reviewerID)
        {

            try
            {
                int timeOffRequestId = Convert.ToInt32(id);
                timeoffrequest tObj = DbCo.timeoffrequests.Where(b => b.TimeOffRequestID == timeOffRequestId).FirstOrDefault<timeoffrequest>();


                tObj.Comment = string.IsNullOrEmpty(comment) ? null : comment;
                tObj.Status = status;
                tObj.ReviewerID = reviewerID;
                DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        #endregion

        #region Room Details
        public int getRoomNumberFromRoomId(int id)
        {
            try
            {
                return DbCo.rooms.Where(r => r.RoomID == id).Select(r => r.RoomNumber).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public int getRoomStartRange(int id)
        {
            try
            {
                return DbCo.rooms.Where(r => r.BusinessID == id).OrderBy(r => r.RoomNumber).Select(r => r.RoomNumber).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public int getRoomEndRange(int id)
        {
            try
            {
                return DbCo.rooms.Where(r => r.BusinessID == id).OrderByDescending(r=>r.RoomNumber).Select(r => r.RoomNumber).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public List<roomstatu> getAllRoomStatus()
        {
            return DbCo.roomstatus.ToList<roomstatu>();
        }
        public List<roomduty> getAllRoomRoomDuties()
        {
            return DbCo.roomduties.ToList<roomduty>();
        }
        public List<roomlist> getAllRoomLists(int businessID,DateTime requestDate)
        {
            return DbCo.roomlists.Where(l=>l.BusinessID==businessID).ToList().Where(l=>l.CreatedAt.Year==requestDate.Year && l.CreatedAt.Month==requestDate.Month && l.CreatedAt.Date==requestDate.Date).ToList<roomlist>();
        }
        public List<room> getAllRooms(int businessID)
        {
            return DbCo.rooms.Where(b => b.BusinessID == businessID).ToList<room>();
        }
        public List<room> getAllUnassignedRooms(int businessID,DateTime requestDate)
        {
            List<int> presentRooms = DbCo.roomlists.Where(r => r.BusinessID == businessID).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomID).ToList<int>();
            return DbCo.rooms.Where(b => b.BusinessID == businessID && !presentRooms.Contains(b.RoomID) ).ToList<room>();
        }

        public List<int> getAllRoomIdByDuty(int businessID,int status,DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.RoomDutiesID==status).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomID).ToList<int>();
        }

        public List<int> getAllRoomList(int businessID, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomID).ToList<int>();
        }

        public List<int> getAllRoomListByStatus(int businessID, int status, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.RoomStatusID == status).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomID).ToList<int>();
        }

        public bool addRoomLists(List<roomlist> lstRoomList)
        {
            try
            {
                DbCo.roomlists.AddRange(lstRoomList);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addRoomLists");
                return false;
            }
        }

        public bool deleteRoomLists(List<int> lstRoomList,DateTime requestDate)
        {
            try
            {
                var removeLst = DbCo.roomlists.Where(r => lstRoomList.Contains(r.RoomID)).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date);
                DbCo.roomlists.RemoveRange(removeLst);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Assign RooList Houseskeeper
        public int getRoomStartRangeFormRoomList(int businessID, DateTime requestDate)
        {

            try
            {
                return (from roomListObj in (DbCo.roomlists.Where(r => r.BusinessID == businessID).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<roomlist>())
                        join roomObj in DbCo.rooms
                        on roomListObj.RoomID equals roomObj.RoomID
                        
                        orderby roomObj.RoomNumber
                        select roomObj.RoomNumber).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int getRoomEndRangeFormRoomList(int businessID, DateTime requestDate)
        {

            try
            {
                return (from roomListObj in (DbCo.roomlists.Where(r => r.BusinessID == businessID).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<roomlist>())
                        join roomObj in DbCo.rooms
                        on roomListObj.RoomID equals roomObj.RoomID

                        orderby roomObj.RoomNumber descending
                        select roomObj.RoomNumber).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        
        public List<employee> getAllHousekeeprOfBusiness(int businessID)
        {

            try
            {
                int housekeeperId = getJobTitleIdFromJobTitle("HOUSEKEEPER");
                List<employee> lstEmp = new List<SmartHotelDataAccessLayer.employee>();
                lstEmp = DbCo.employees.Where(e => e.BusinessID == businessID && e.JobTitleID == housekeeperId).ToList<employee>();
                return lstEmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<roomlist> getAllCreatedRoomLists(int businessID, DateTime requestDate)
        {
            return DbCo.roomlists.Where(l => l.BusinessID == businessID && l.HousekeeperID!=null).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).OrderBy(r=>r.room.RoomID).ToList<roomlist>();
        }

        public List<roomlist> getAllHKUnassignedRooms(int businessID, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HousekeeperID==null).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<roomlist>();
            //return DbCo.rooms.Where(b => b.BusinessID == businessID && presentRooms.Contains(b.RoomID)).ToList<room>();
        }
        public bool updateHKToRoomList(int businessID, DateTime requestDate,int roomId,int HKId)
        {
            try
            {
                roomlist rmObj = DbCo.roomlists.Where(r => r.BusinessID == businessID && r.RoomID == roomId).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).FirstOrDefault<roomlist>();
                if(rmObj!=null)
                {
                    rmObj.HousekeeperID = HKId;
                    rmObj.RoomStatusID = getRoomListStatusIdByStatus("ASSIGNED");
                    DbCo.Entry(rmObj).State = System.Data.Entity.EntityState.Modified;
                    DbCo.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool updateHKToRoomListByRoomListId(int roomListId,int HKId)
        {
            try
            {
                roomlist rmObj = DbCo.roomlists.Where(r => r.RoomListID == roomListId).FirstOrDefault<roomlist>();
                if (rmObj != null)
                {
                    rmObj.HousekeeperID = HKId;
                    DbCo.Entry(rmObj).State = System.Data.Entity.EntityState.Modified;
                    DbCo.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<int> getAllRoomIdByHK(int businessID, int selectedEmployee, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HousekeeperID == selectedEmployee).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomID).ToList<int>();
        }
        public bool deleteHKToRoomList(List<int> lstdeletedRoomList, DateTime requestDate)
        {
            try
            {
                List<roomlist> rmObj = DbCo.roomlists.Where(r => lstdeletedRoomList.Contains(r.RoomID)).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<roomlist>();
                if (rmObj.Count >0)
                {
                    foreach (var item in rmObj)
                    {
                        item.HousekeeperID = null;
                        DbCo.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        DbCo.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region AssignToHeadHK
        public int getRoomStartRangeFormRoomListForHeadHK(int businessID, DateTime requestDate)
        {

            try
            {
                return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HousekeeperID != null).ToList().Where(l => l.CreatedAt.Year == requestDate.Year &&
                       l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).OrderBy(r => r.room.RoomNumber).Select(r => r.room.RoomNumber).
                       FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int getRoomEndRangeFormRoomListForHeadHK(int businessID, DateTime requestDate)
        {

            try
            {
                return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HousekeeperID!=null).ToList().Where(l => l.CreatedAt.Year == requestDate.Year &&
                       l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).OrderByDescending(r => r.room.RoomNumber).Select(r => r.room.RoomNumber).
                       FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<employee> getAllHeadHousekeeprOfBusiness(int businessID)
        {

            try
            {
                int housekeeperId = getJobTitleIdFromJobTitle("HEAD HOUSEKEEPER");
                List<employee> lstEmp = new List<SmartHotelDataAccessLayer.employee>();
                lstEmp = DbCo.employees.Where(e => e.BusinessID == businessID && e.JobTitleID == housekeeperId).ToList<employee>();
                return lstEmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<roomlist> getAllHeadHKAssignedRoomLists(int businessID, DateTime requestDate)
        {
            return DbCo.roomlists.Where(l => l.BusinessID == businessID && l.HousekeeperID != null).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).OrderBy(r => r.room.RoomID).ToList<roomlist>();
        }

        public List<roomlist> getAllHeadHKUnassignedRooms(int businessID, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HousekeeperID!=null && r.HeadHousekeeperID == null).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<roomlist>();
            //return DbCo.rooms.Where(b => b.BusinessID == businessID && presentRooms.Contains(b.RoomID)).ToList<room>();
        }
        public bool updateHeadHKToRoomList(int roomListId, int HKId)
        {
            try
            {
                roomlist rmObj = DbCo.roomlists.Where(r=>r.RoomListID == roomListId).ToList().FirstOrDefault<roomlist>();
                if (rmObj != null)
                {
                    rmObj.HeadHousekeeperID = HKId;
                    DbCo.Entry(rmObj).State = System.Data.Entity.EntityState.Modified;
                    DbCo.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<int> getAllRoomListIdByHK(int businessID, int selectedEmployee, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HeadHousekeeperID == selectedEmployee).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomListID).ToList<int>();
        }

        public bool deleteHeadHKToRoomList(List<int> lstdeletedRoomList, DateTime requestDate)
        {
            try
            {
                List<roomlist> rmObj = DbCo.roomlists.Where(r => lstdeletedRoomList.Contains(r.RoomListID)).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<roomlist>();
                if (rmObj.Count > 0)
                {
                    foreach (var item in rmObj)
                    {
                        item.HeadHousekeeperID = null;
                        DbCo.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        DbCo.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<int> getAllRoomIdByHeadHK(int businessID, int selectedEmployee, DateTime requestDate)
        {
            return DbCo.roomlists.Where(r => r.BusinessID == businessID && r.HeadHousekeeperID == selectedEmployee).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).Select(r => r.RoomListID).ToList<int>();
        }

        public bool updateHeadHKToRoomListByRoomListId(int roomListId, int HKId)
        {
            try
            {
                roomlist rmObj = DbCo.roomlists.Where(r => r.RoomListID == roomListId).FirstOrDefault<roomlist>();
                if (rmObj != null)
                {
                    rmObj.HeadHousekeeperID = HKId;
                    DbCo.Entry(rmObj).State = System.Data.Entity.EntityState.Modified;
                    DbCo.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        #endregion

        #region VewAllRooms
        public List<priority> getAllPriority()
        {
            try
            {
                return DbCo.priorities.ToList<priority>();
            }
            catch (Exception ex)
            {
               return null;
            }
        }

        public bool editRoomList(int id, int HKId, int duty, int status, int priority, int HeadHk, string note)
        {
            try
            {
                roomlist rlObj = DbCo.roomlists.Where(r => r.RoomListID == id).FirstOrDefault<roomlist>();
                rlObj.HousekeeperID = HKId;
                rlObj.HeadHousekeeperID = HeadHk;
                rlObj.RoomDutiesID = duty;
                rlObj.RoomStatusID = status;
                rlObj.PriorityID = priority;
                rlObj.Notes = note;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool deleteRoomListsByRoomListId(int id)
        {
            try
            {
                roomlist rlObj = DbCo.roomlists.Where(r => r.RoomListID == id).FirstOrDefault<roomlist>();
                DbCo.roomlists.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        #endregion

        #region maintenance
        public List<maintenancetype> getAllMaintenanceType()
        {
            return DbCo.maintenancetypes.ToList<maintenancetype>();
        }

        public List<maintenancestatu> getAllMaintenanceStatus()
        {
            return DbCo.maintenancestatus.ToList<maintenancestatu>();
        }

        public List<employee> getAllMaintenanceOfBusiness(int businessId)
        {
            int housekeeperId = getJobTitleIdFromJobTitle("MAINTENANCE PERSON");
            List<employee> lstEmp = new List<SmartHotelDataAccessLayer.employee>();
            lstEmp = DbCo.employees.Where(e => e.BusinessID == businessId && e.JobTitleID == housekeeperId).ToList<employee>();
            return lstEmp;
        }

        
        public List<maintenance> getAllMaintenanceRooms(int businessID)
        {
            try
            {
                return DbCo.maintenances.Where(l => l.BusinessID == businessID).ToList<maintenance>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool editMaintenanceRequest(int id, int MPId, int type, int status, int priority, int HeadHk, string note)
        {
            try
            {
                maintenance rlObj = DbCo.maintenances.Where(r => r.MaintenanceID == id).FirstOrDefault<maintenance>();
                rlObj.AssignedTo = MPId;
                rlObj.MaintenanceDutyID = type;
                rlObj.MaintenanceStatusID = status;
                rlObj.PriorityID = priority;
                rlObj.Notes = note;
                rlObj.VerifiedBy = HeadHk;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool deleteMaintenanceRequest(int id)
        {
            try
            {
                maintenance rlObj = DbCo.maintenances.Where(r => r.MaintenanceID == id).FirstOrDefault<maintenance>();
                DbCo.maintenances.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addMaintenanceRequest(maintenance mainObj)
        {
            try
            {
                DbCo.maintenances.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
        #endregion

        #region LostFoundRequest
        public int getLostAndFoundStatusByStatus(string status)
        {
            try
            {
                return DbCo.lostfoundstatus.Where(s => s.Status.Equals(status)).Select(s => s.LostFoundStatusID).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public List<lostfound> getAllLostFoundRequest(int businessID)
        {
            return DbCo.lostfounds.Where(l => l.BusinessID == businessID).OrderBy(r => r.CreatedAt).ToList<lostfound>();
        }
        public List<lostfoundstatu> allLostFoundStatus()
        {
            return DbCo.lostfoundstatus.ToList<lostfoundstatu>();
        }
        public List<employee> getAllEmployeeOfBusiness(int businessId)
        {
            List<employee> lstEmp = new List<SmartHotelDataAccessLayer.employee>();
            lstEmp = DbCo.employees.Where(e => e.BusinessID == businessId).ToList<employee>();
            return lstEmp;
        }
        public bool editLostFoundRequest(int id, int status, int priority, int collectedby, string note,DateTime updatetime)
        {
            try
            {
                lostfound rlObj = DbCo.lostfounds.Where(r => r.LostFoundID == id).FirstOrDefault<lostfound>();
                rlObj.LostFoundStatusID = status;
                rlObj.PriorityID = priority;
                rlObj.AddedBy = collectedby;
                rlObj.Notes = note;
                rlObj.UpdatedAt = updatetime;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool deleteLostFoundRequest(int id)
        {
            try
            {
                lostfound rlObj = DbCo.lostfounds.Where(r => r.LostFoundID == id).FirstOrDefault<lostfound>();
                DbCo.lostfounds.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addLostFoundRequest(lostfound mainObj)
        {
            try
            {
                DbCo.lostfounds.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
        #endregion

        #region Alerts

        public List<alert> getAllAlerts(int businessID)
        {
            try
            {
                List<alert> obj = DbCo.alerts.Where(l => l.BusinessID == businessID).OrderBy(r => r.CreatedAt).ToList<alert>();
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int getAlertTypeIdFromAlertType(string type)
        {
            return DbCo.alerttypes.Where(l => l.type.Equals(type)).Select(r => r.AlertTypeID).FirstOrDefault<int>();
        }
        public int getAlertStatusIdFromAlertStatus(string status)
        {
            return DbCo.alertstatus.Where(l => l.Status.Equals(status)).Select(r => r.AlertStatusID).FirstOrDefault<int>();
        }
        public List<alertstatu> allAlertStatus()
        {
            return DbCo.alertstatus.ToList<alertstatu>();
        }
        public List<alerttype> allAlertType()
        {
            return DbCo.alerttypes.ToList<alerttype>();
        }
       
        public bool editAlerts(int id, int type, int assignedto, int status, int verifyedby, string note)
        {
            try
            {
                alert rlObj = DbCo.alerts.Where(r => r.AlertsID == id).FirstOrDefault<alert>();
                rlObj.AlertTypeID = type;
                rlObj.AssignedTo = assignedto;
                rlObj.AlertStatusID = status;
                rlObj.Notes = note;
                rlObj.VerifiedBy = verifyedby;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool deleteAlerts(int id)
        {
            try
            {
                alert rlObj = DbCo.alerts.Where(r => r.AlertsID == id).FirstOrDefault<alert>();
                DbCo.alerts.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addAlerts(alert mainObj)
        {
            try
            {
                DbCo.alerts.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Laundry
       

        public List<laundrystatu> getAllLaundryStatus()
        {
            return DbCo.laundrystatus.ToList<laundrystatu>();
        }

        public List<employee> getAllLaundryPersonOfBusiness(int businessId)
        {
            int housekeeperId = getJobTitleIdFromJobTitle("LAUNDRY PERSON");
            List<employee> lstEmp = new List<SmartHotelDataAccessLayer.employee>();
            lstEmp = DbCo.employees.Where(e => e.BusinessID == businessId && e.JobTitleID == housekeeperId).ToList<employee>();
            return lstEmp;
        }

        public List<laundry> getAllLaundry(int businessID, DateTime requestDate)
        {
            return DbCo.laundries.Where(l => l.BusinessID == businessID).ToList().Where(l => l.CreatedAt.Year == requestDate.Year && l.CreatedAt.Month == requestDate.Month && l.CreatedAt.Date == requestDate.Date).ToList<laundry>();
        }
        public bool editLaundry(int id, string description, int assignedto, int status)
        {
            try
            {
                laundry rlObj = DbCo.laundries.Where(r => r.LaundryId == id).FirstOrDefault<laundry>();
                rlObj.AssignedTo = assignedto;
                rlObj.LaundryStatusID = status;
                rlObj.Description = description;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool deleteLaundry(int id)
        {
            try
            {
                laundry rlObj = DbCo.laundries.Where(r => r.LaundryId == id).FirstOrDefault<laundry>();
                DbCo.laundries.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addLaundry(laundry mainObj)
        {
            try
            {
                DbCo.laundries.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
        #endregion

        #region Schedule
        public List<schedule> getAllSchedules(int businessID)
        {
            try
            {
                return DbCo.schedules.Where(l => l.BusinessID == businessID).ToList<schedule>();
            }
            catch (Exception ex)
            {
                return new List<schedule>();
            }
        }
        public bool addSchedule(schedule schedObj)
        {
            try
            {
                DbCo.schedules.Add(schedObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool isSchedulePresent(int empid,DateTime datetime)
        {
            try
            {
                schedule emp = DbCo.schedules.Where(s => s.EmployeeID == empid).ToList().Where(e => e.ScheduleDate.Year == datetime.Year
                      && e.ScheduleDate.Month == datetime.Month && e.ScheduleDate.Date == datetime.Date).FirstOrDefault<schedule>();
                if (emp != null)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool deleteSchedule(int id)
        {
            try
            {
                schedule rlObj = DbCo.schedules.Where(r => r.ScheduleID == id).FirstOrDefault<schedule>();
                DbCo.schedules.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool editSchedule(string id, string starttime, string endtime, string note)
        {

            try
            {
                int scheduleID = Convert.ToInt32(id);
                schedule tObj = DbCo.schedules.Where(b => b.ScheduleID == scheduleID).FirstOrDefault<schedule>();

                tObj.StartTime = stringToTimeSpanHM(starttime);
                tObj.EndTime = stringToTimeSpanHM(endtime);
                tObj.TotalScheduledHours = (tObj.StartTime != null && tObj.EndTime != null) ? Math.Round((((TimeSpan)tObj.EndTime - (TimeSpan)tObj.StartTime).TotalHours), 1).ToString() : null;
                tObj.Notes = string.IsNullOrEmpty(note) ? null : note;
                DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<schedule> getNextSevenDaysSchedule(int businessId,DateTime requestDate,int empId)
        {
            try
            {
                 return DbCo.schedules.Where(b => b.EmployeeID == empId).ToList().Where(a => a.ScheduleDate < requestDate.AddDays(7) && a.ScheduleDate > requestDate).OrderBy(s=>s.ScheduleDate).ToList();
                
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "sendScheduleMail");
                return null;
            }
        }

        #endregion

        #region Message
        public List<employee> getEmployeesForMessage(int businessID,int employeeID)
        {
            try
            {
                return DbCo.employees.Where(b => b.BusinessID == businessID && b.EmployeeID!= employeeID ).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<allmessage> getAllmessage(int businessId,int empid)
        {
            try
            {
                List<allmessage> lstMEssage = DbCo.allmessages.Where(s => s.BusinessID == businessId && (s.MessageSentByID == empid || s.MessageRecievedByID == empid)).
                    OrderBy(s => s.CreatedAt).OrderBy(l => l.CreatedAt).ToList<allmessage>();
                
                return lstMEssage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool addMessage(allmessage msgObj)
        {
            try
            {
                DbCo.allmessages.Add(msgObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        

        public List<object> getEmployeeMessageCounter(int businessID,int employeeID)
        {
            try
            {
                List<object> objLst = new List<object>();
                List<employee> empLst = getEmployeesForMessage(businessID, employeeID);
                if(empLst.Count>0)
                {
                    foreach (var emp in empLst)
                    {
                        objLst.Add(new { employeeId = emp.EmployeeID,
                            Name=emp.Name,
                            unreadCount = DbCo.allmessages.Where(a => a.MessageRecievedByID == employeeID && a.MessageSentByID==emp.EmployeeID && a.isRead==0).Count(),
                            ImagePath = emp.ImagePath
                        });
                    }
                    return objLst;
                }
                return null;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool readAllMessage(int businessID, int employeeID,int id)
        {
            try
            {
                List<allmessage> objLst = new List<allmessage>();
                objLst = DbCo.allmessages.Where(a => a.MessageRecievedByID == employeeID && a.MessageSentByID == id && a.isRead==0).ToList();
                //objLst.ForEach(a => a.isRead = 1);
                foreach (var item in objLst)
                {
                    item.isRead = 1;
                    DbCo.Entry(item).State = EntityState.Modified;
                }
                DbCo.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int getTotalUnreadMessageCount(int businessId,int employeeId)
        {
            try
            {
                return DbCo.allmessages.Where(a => a.BusinessID==businessId && a.MessageRecievedByID == employeeId && a.isRead == 0).Count();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        #region Report
        public List<ReportHK> HKPerformance(int businessId)
        {
            try
            {
                List<ReportHK> lst = new List<ReportHK>();
                List<employee> lstHouseKeepr = getAllHousekeeprOfBusiness(businessId);
                foreach (var emp in lstHouseKeepr)
                {
                    lst.Add(new ReportHK
                    {
                        employeeId=emp.EmployeeID,
                        Name=emp.Name,
                        avgTimePerRoom= getAverageMinutesPerRoomHK(emp.EmployeeID,"ALL"),
                        avgRoomPerDay= getAverageRoomPerDayHK(emp.EmployeeID,"ALL"),
                        avgTimePerRoomWeekly = getAverageMinutesPerRoomHK(emp.EmployeeID, "Weekly"),
                        avgRoomPerDayWeekly = getAverageRoomPerDayHK(emp.EmployeeID, "Weekly"),
                        avgTimePerRoomMonthly = getAverageMinutesPerRoomHK(emp.EmployeeID, "Monthly"),
                        avgRoomPerDayMonthly = getAverageRoomPerDayHK(emp.EmployeeID, "Monthly"),
                    });
                }
                if (lst.Count() > 0)
                    return lst;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int getAverageMinutesPerRoomHK(int employeeId,string type)
        {
            try
            {
                float? avg = 0;
                int cleanStatusId = getRoomListStatusIdByStatus("VERIFIED");
                switch (type)
                {
                    case "ALL":
                        avg = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).Select(b => b.TotalTime).Average();
                        break;
                    case "Weekly":
                        avg = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).ToList().Where(a=> a.CreatedAt > DateTime.Now.AddDays(-7)).Select(b => b.TotalTime).Average();
                        break;
                    case "Monthly":
                        avg = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).ToList().Where(a => a.CreatedAt > DateTime.Now.AddMonths(-1)).Select(b => b.TotalTime).Average();
                        break;
                    default:
                        break;
                }
                
                if (avg != null)
                    return (int)avg;
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int getAverageRoomPerDayHK(int employeeId,string type)
        {
            try
            {
                int totalRoom = 0,totalDays=1;
                int cleanStatusId = getRoomListStatusIdByStatus("VERIFIED");
                

                switch (type)
                {
                    case "ALL":
                        totalRoom = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).Count();
                        totalDays = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).GroupBy(b => b.CreatedAt).Count();
                        break;
                    case "Weekly":
                        totalRoom = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).ToList().Where(a => a.CreatedAt > DateTime.Now.AddDays(-7)).Count();
                        totalDays = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).ToList().Where(a => a.CreatedAt > DateTime.Now.AddDays(-7)).GroupBy(b => b.CreatedAt).Count();
                        break;
                    case "Monthly":
                        totalRoom = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).ToList().Where(a => a.CreatedAt > DateTime.Now.AddMonths(-1)).Count();
                        totalDays = DbCo.roomlist_backup.Where(b => b.HousekeeperID == employeeId && b.RoomStatusID == cleanStatusId).ToList().Where(a => a.CreatedAt > DateTime.Now.AddMonths(-1)).GroupBy(b => b.CreatedAt).Count();
                        break;
                }

                return (int)(totalRoom/totalDays);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<ReportHK> MPPerformance(int businessId)
        {
            try
            {
                List<ReportHK> lst = new List<ReportHK>();
                List<employee> lstHouseKeepr = getAllMaintenanceOfBusiness(businessId);
                foreach (var emp in lstHouseKeepr)
                {
                    lst.Add(new ReportHK
                    {
                        employeeId = emp.EmployeeID,
                        Name = emp.Name,
                        avgTimePerRoom = getAverageMinutesPerRoomMP(emp.EmployeeID),
                        avgRoomPerDay = getAverageRoomPerDayMP(emp.EmployeeID)
                    });
                }
                if (lst.Count() > 0)
                    return lst;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int getAverageMinutesPerRoomMP(int employeeId)
        {
            try
            {
                float? avg = 0;
                int cleanStatusId = getMainenanceStatusIdByStatus("VERIFIED");
                avg = DbCo.maintenances.Where(b => b.AssignedTo == employeeId && b.MaintenanceStatusID == cleanStatusId).Select(b => b.TotalTime).Average();
                if (avg != null)
                    return (int)avg;
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int getAverageRoomPerDayMP(int employeeId)
        {
            try
            {
                int totalRoom = 0, totalDays = 1;
                int cleanStatusId = getMainenanceStatusIdByStatus("VERIFIED");
                totalRoom = DbCo.maintenances.Where(b => b.AssignedTo == employeeId && b.MaintenanceStatusID== cleanStatusId).Count();
                //totalDays = DbCo.maintenances.Where(b => b.AssignedTo == employeeId && b.MaintenanceStatusID == cleanStatusId).GroupBy(b => EntityFunctions.TruncateTime(b.CreatedAt)).Count();
                return totalRoom;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        #region Inspection
        public List<inspection> getAllInspection(int businessID)
        {
            try
            {
                List<inspection> obj = DbCo.inspections.Where(l => l.BusinessID == businessID).OrderBy(r => r.CreatedAt).ToList<inspection>();
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool deleteInspection(int id)
        {
            try
            {
                inspection rlObj = DbCo.inspections.Where(r => r.InspectionID == id).FirstOrDefault<inspection>();
                DbCo.inspections.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Guest
        public bool addGuest(guest mainObj)
        {
            try
            {
                DbCo.guests.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addGuest");
                return false;
            }
        }
        public bool addGuestBusiness(guestbusiness mainObj)
        {
            try
            {
                DbCo.guestbusinesses.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addGuestBusiness");
                return false;
            }
        }
        public bool checkGuestTokenPresent(string token)
        {
            try
            {
                if(DbCo.guestbusinesses.Where(g=>g.Token.Equals(token)).FirstOrDefault()!=null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public List<guestbusiness> getAllGuest(int businessID)
        {
            try
            {
                List<guestbusiness> obj = DbCo.guestbusinesses.Where(l => l.BusinessID == businessID && l.business.Status.Equals("Active") && 
                l.isDeleted!=1).OrderBy(r => r.StayDateStart).ToList<guestbusiness>();
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public guest getGuestById(int id)
        {
            try
            {
                return DbCo.guests.Where(l => l.GuestID == id).FirstOrDefault();
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool updateGuest(guest g)
        {

            try
            {
                DbCo.Entry(g).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool updateGuestBusiness(guestbusiness g)
        {

            try
            {
                DbCo.Entry(g).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public guestbusiness getGuestBusinessById(int id)
        {
            try
            {
                return DbCo.guestbusinesses.Where(l => l.GuestBusinessID == id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool deleteBusinessGuest(int id)
        {
            try
            {
                guestbusiness rlObj = getGuestBusinessById(id);
                rlObj.isDeleted = 1;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool ActivateDeactivateGuest(int id, string action)
        {
            try
            {
                guestbusiness rlObj = getGuestBusinessById(id);
                if (action.Trim().Equals("activate"))
                {
                    rlObj.isActive = "Active";
                }
                else if (action.Trim().Equals("deactivate"))
                {
                    rlObj.isActive = "Inactive";
                }
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int getGuestMessageUnreadCount(int id)
        {
            try
            {
                int cou= DbCo.guestmessages.Where(m => m.MessageSentByID == id && m.isRead == 0 && m.messageTtype.Equals("from")).Count();
                return cou;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getGuestMessageUnreadCount");
                return 0;
            }
        }

        public int getGuestMessageTotalUnreadCount(int id)
        {
            try
            {
               
                int cou = DbCo.guestmessages.Where(m => m.BusinessID == id && m.isRead == 0 && m.messageTtype.Equals("from")).Count();
                return cou;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getGuestMessageUnreadCount");
                return 0;
            }
        }


        public bool addGuestMessage(guestmessage mainObj)
        {
            try
            {
                DbCo.guestmessages.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addGuestMessage");
                return false;
            }
        }

        public List<guestmessage> getAllGuestmessage(int id)
        {
            try
            {
                List<guestmessage> lstMEssage = DbCo.guestmessages.Where(s => s.MessageSentByID == id ).
                    OrderBy(s => s.CreatedAt).ToList<guestmessage>();

                return lstMEssage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool readAllGuestMessage(int id)
        {
            try
            {
                List<guestmessage> objLst = new List<guestmessage>();
                objLst = DbCo.guestmessages.Where(a => a.MessageSentByID == id && a.messageTtype.Equals("from") && a.isRead == 0).ToList();
                //objLst.ForEach(a => a.isRead = 1);
                foreach (var item in objLst)
                {
                    item.isRead = 1;
                    DbCo.Entry(item).State = EntityState.Modified;
                }
                DbCo.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool editGuest(guest obj)
        {
            try
            {
                DbCo.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public guest getGuestByNumber(string phoneNo)
        {
            try
            {
                guest obj = DbCo.guests.Where(g => g.phone.Equals(phoneNo)).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<amenty> getAllAmenties(int businessID)
        {
            return DbCo.amenties.Where(l => l.BusinessID == businessID).ToList<amenty>();
        }
        public bool editAmenties(int id, string amentiesText)
        {
            try
            {
                amenty rlObj = DbCo.amenties.Where(r => r.AmentiesID == id).FirstOrDefault<amenty>();
                
                rlObj.AmentiesText = amentiesText;
                DbCo.Entry(rlObj).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool deleteAmenties(int id)
        {
            try
            {
                amenty rlObj = DbCo.amenties.Where(r => r.AmentiesID == id).FirstOrDefault<amenty>();
                DbCo.amenties.Remove(rlObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool addAmenties(amenty mainObj)
        {
            try
            {
                DbCo.amenties.Add(mainObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
        #endregion

        #region Support
        public bool addFeedback(int bid, DateTime dt,string comm)
        {
            try
            {
                feedback fb = new feedback();
                fb.BusinessID = bid;
                fb.Comment = comm;
                fb.FeedbackDate = dt;
                DbCo.feedbacks.Add(fb);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addFeedback");
                return false;
            }
        }
        #endregion

        #endregion

        #region For API

        public bool loginCheck(int empid, string password)
        {
            string abc = "0";
            try
            {
                  abc = " 1 ";
                employee emp = DbCo.employees.Where(e => e.EmployeeID==empid && e.Password.Equals(password) && e.isActive.Equals("Active") && e.business.IsVerified == 1 && e.business.Status.Equals("Active")).FirstOrDefault<employee>();
                //abc += " 2 ";
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                 abc += " 3 ";
                byte[] key = Guid.NewGuid().ToByteArray();
                abc += " 4 ";
                string token = Convert.ToBase64String(time.Concat(key).ToArray());
                abc += " 5 ";
                emp.EmployeeToken = token;
                abc += " 6 ";
                DbCo.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                abc += " 7 ";
                DbCo.SaveChanges();
                abc += " 8 ";
                if (emp != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "loginCheck API"+empid+" pass : "+password+" abc : "+abc);
                //return ex.InnerException+ "## "+ ex.Message + "## " + ex.Source + "## " + ex.StackTrace + "## " + ex.TargetSite + "## " + ex.HResult + "## " + ex.HelpLink + "## " + abc;
                return false;
            }
            return false;
        }

        public bool userCheck(int empid, string password)
        {
            //string abc = "0";
            try
            {
                //  abc = " 1 ";
                employee emp = DbCo.employees.Where(e => e.EmployeeID == empid && e.Password.Equals(password)).FirstOrDefault<employee>();
                //abc += " 2 ";
                
                if (emp != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "userCheck API");
                //return ex.InnerException+ "## "+ ex.Message + "## " + ex.Source + "## " + ex.StackTrace + "## " + ex.TargetSite + "## " + ex.HResult + "## " + ex.HelpLink + "## " + abc;
                return false;
            }
            return false;
        }

        public bool Lougout(int empid)
        {
            try
            {
                employee emp = DbCo.employees.Where(b => b.EmployeeID==empid).FirstOrDefault<employee>();
                emp.EmployeeToken = null;
                DbCo.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool changePassword(int id,string password)
        {
            try
            {
                employee emp = DbCo.employees.Where(b => b.EmployeeID==id).FirstOrDefault<employee>();
                emp.Password=password;
                DbCo.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool updateEmployeeProfile(int employeeId,string Name, string Email, string Phone, string ImagePath)
        {
            try
            {
                employee emp = DbCo.employees.Where(e => e.EmployeeID == employeeId).FirstOrDefault<employee>();
                if(emp!=null)
                {
                    if (!string.IsNullOrEmpty(Name))
                        emp.Name = Name;
                    if (!string.IsNullOrEmpty(Email))
                        emp.Email = Email;
                    if (!string.IsNullOrEmpty(Phone))
                        emp.Phone = Phone;
                    if (!string.IsNullOrEmpty(ImagePath))
                        emp.ImagePath = ImagePath;
                    DbCo.Entry(emp).State = EntityState.Modified;
                    DbCo.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool TimeCardRequest(int employeeId, int businessId, DateTime requestDate, string requestType, string requestTime)
        {
            string abc = "";
            try
            {
                timecard tObj = DbCo.timecards.Where(b => b.EmployeeID==employeeId && b.BusinessID==businessId && b.WorkDate.Year==requestDate.Year && b.WorkDate.Month == requestDate.Month && b.WorkDate.Day == requestDate.Day).FirstOrDefault<timecard>();
                abc += "# 1 #";
                switch (requestType)
                {

                    case "ClockIn":
                        bool newlyAdded = false;
                        abc += "# 2 #";
                        if (tObj == null)
                        {
                            newlyAdded = true;
                            tObj = new timecard();
                        }
                        abc += "# 4 #";
                        tObj.WorkDate = requestDate;
                        tObj.BusinessID = businessId;
                        tObj.EmployeeID = employeeId;
                        abc += "# 5 #";
                        tObj.ClockInTime = stringToTimeSpan(requestTime);
                        abc += "# 6 #";
                        if (newlyAdded)
                            DbCo.timecards.Add(tObj);
                        else
                            DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                        abc += "# 7 #";
                        DbCo.SaveChanges();
                        abc += "# 8 #";
                        if (tObj.TimeCardID > 0)
                            return true;
                        else
                            return false;
                    case "ClockOut":
                        abc += "# 9 #";
                        tObj.ClockOutTime = stringToTimeSpan(requestTime);
                        abc += "# 10 #"+tObj.ClockInTime;
                        tObj.TotalWorkedHour = (tObj.ClockInTime != null && tObj.ClockOutTime != null) ? Math.Round((((TimeSpan)tObj.ClockOutTime - (TimeSpan)tObj.ClockInTime).TotalHours)-Convert.ToDouble(tObj.TotalLunchTime)/60.0, 1).ToString() : null;
                        DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                        abc += "# 11 #";
                        DbCo.SaveChanges();
                        return true;
                    case "LunchIn":
                        tObj.LunchInTime = stringToTimeSpan(requestTime);
                        DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                        DbCo.SaveChanges();
                        return true;
                    case "LunchOut":
                        tObj.LunchOutTime = stringToTimeSpan(requestTime);
                        tObj.TotalLunchTime = (tObj.LunchInTime != null && tObj.LunchOutTime != null) ? (((TimeSpan)tObj.LunchOutTime).TotalMinutes - ((TimeSpan)tObj.LunchInTime).TotalMinutes).ToString().Split('.')[0] : null;
                        DbCo.Entry(tObj).State = System.Data.Entity.EntityState.Modified;
                        DbCo.SaveChanges();
                        return true;
                    default:
                        return false;
                }
            }
            catch (DbEntityValidationException e)
            {
                string msg = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    msg += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        msg += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                ExeptionLoginning.SendErrorToText(e, "TimeCardRequest API " + msg);
                return false;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex,abc+requestDate.Month+" "+ requestDate.Day+" "+requestDate.Year);
                return false;
                //return ex.Message+abc+ "#Data#" + ex.Data + "#HResult#" + ex.HResult + "#InnerException#" + ex.InnerException + "#Message#" + ex.Message + "#Source#" + ex.Source +
                //    "#StackTrace#" + ex.StackTrace + "#TargetSite#" + ex.TargetSite ;
            }
            return false ;
        }

        public object getTimeCardDetails(int employeeId, int businessId, DateTime requestDate)
        {
            string abc = "";
            try
            {
                object tObj = DbCo.timecards.Where(b => b.EmployeeID == employeeId && b.BusinessID == businessId && b.WorkDate.Year == requestDate.Year && b.WorkDate.Month == requestDate.Month && b.WorkDate.Day == requestDate.Day).ToList().Select(b=>new
                { LunchInTime=ConvertTOAMPM(b.LunchInTime),
                    LunchOutTime = ConvertTOAMPM(b.LunchOutTime),
                    ClockInTime = ConvertTOAMPM(b.ClockInTime),
                    ClockOutTime = ConvertTOAMPM(b.ClockOutTime),
                    b.TotalLunchTime,b.TotalWorkedHour }).FirstOrDefault();
                return tObj;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getTimeCardDetails");
                return null;
            }
        }

        public object getLastWeekTimeCardDetails(int employeeId, int businessId, DateTime requestDate)
        {
            string abc = "";
            try
            {
                object tObj = DbCo.timecards.Where(b => b.EmployeeID == employeeId && b.BusinessID == businessId).ToList().Where(b=>b.WorkDate>requestDate.AddDays(-7)).Select(b => new
                {
                    b.BusinessID,
                    b.EmployeeID,
                    WorkDate =b.WorkDate.ToString("MM/dd/yyyy"),
                    LunchInTime=ConvertTOAMPM(b.LunchInTime),
                    LunchOutTime=ConvertTOAMPM(b.LunchOutTime),
                    ClockInTime=ConvertTOAMPM(b.ClockInTime),
                    ClockOutTime=ConvertTOAMPM(b.ClockOutTime),
                    b.TotalLunchTime,
                    b.TotalWorkedHour }).ToList();
                return tObj;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getLastWeekTimeCardDetails");
                return null;
            }
        }

        public bool addTimeOffRequest(int employeeId, int businessId, DateTime StartDate,DateTime EndDate,DateTime CreatedAt,string reason,string path)
        {
            
            try
            {
                timeoffrequest timeoffObj = new timeoffrequest();
                timeoffObj.StartDate = StartDate;
                timeoffObj.EndDate = EndDate;
                timeoffObj.CreatedAt = CreatedAt;
                timeoffObj.EmployeeID = employeeId;
                timeoffObj.BusinessID = businessId;
                timeoffObj.Reason = reason;
                timeoffObj.Status = "Pending";
                timeoffObj.SignPath = path;
                DbCo.timeoffrequests.Add(timeoffObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                string exmsg = "";
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        exmsg += "## " + message + " ##";
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                ExeptionLoginning.SendErrorToText(dbEx, "addTimeOffRequest"+exmsg + employeeId + "    #   " + businessId + "    #   " + StartDate + "    #   " + EndDate + "    #   " + CreatedAt + "    #   " + reason + "    #   " + path);

                return false;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addTimeOffRequest" + employeeId + "    #   " + businessId + "    #   " + StartDate + "    #   " + EndDate + "    #   " + CreatedAt + "    #   " + reason + "    #   " + path);
                return false;
            }
        }

        public object getTimeOffRequest(int employeeId, int businessId)
        {
            try
            {
                object tObj = DbCo.timeoffrequests.Where(b => b.EmployeeID == employeeId && b.BusinessID == businessId).OrderByDescending(b=>b.CreatedAt).ToList().Select(b => new
                {

                    b.TimeOffRequestID,
                    b.BusinessID,
                    b.EmployeeID,
                    CreatedAt = b.CreatedAt.ToString("MM/dd/yyyy"),
                    StartDate = b.StartDate.ToString("MM/dd/yyyy"),
                    EndDate = b.EndDate.ToString("MM/dd/yyyy"),
                    b.Reason,
                    b.Comment,
                    b.Status
                }).ToList();
                return tObj;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getTimeOffRequest");
                return null;
            }
        }


        public object getTasksDetails(int employeeId, int businessId, DateTime requestDate)
        {
            string abc = "";
            try
            {
                string jobtitle = DbCo.employees.Where(e => e.EmployeeID == employeeId).Select(e => e.jobtitle.JobTitle1).FirstOrDefault<string>();
                //object tObj = null;
                switch (jobtitle)
                {
                    case "HOUSEKEEPER":
                        object tObj = DbCo.roomlists.Where(b => b.HousekeeperID == employeeId && b.BusinessID == businessId).ToList().Where(b =>
                   b.CreatedAt.Year == requestDate.Year && b.CreatedAt.Month == requestDate.Month && b.CreatedAt.Day == requestDate.Day
                    ).Select(b => new
                    {
                        Type = "HOUSEKEEPER",
                        BusinessID = b.BusinessID.ToString(),
                        EmployeeID = b.EmployeeID.ToString(),
                        TaskId = b.RoomListID.ToString(),
                        RoomID = b.RoomID.ToString(),
                        RoomNumber = b.room.RoomNumber.ToString(),
                        StatusID = b.RoomStatusID.ToString(),
                        Status = b.roomstatu.Status,
                        DutyId = b.RoomDutiesID.ToString(),
                        Duty = b.roomduty.Duty,
                        PriorityID = b.PriorityID.ToString(),
                        Priority = b.priority.Priority1,
                        CreatedAt = b.CreatedAt.ToString("MM/dd/yyyy"),
                        StartTime = ConvertTOAMPM(b.StartTime),
                        EndTime = ConvertTOAMPM(b.EndTime),
                        PauseTime = ConvertTOAMPM(b.PauseTime),
                        ResumeTime = ConvertTOAMPM(b.ResumeTime),
                        TotalTime = b.TotalTime.ToString(),
                        Image1 = "",
                        Image2 = "",
                        Image3 = "",
                        Notes= string.IsNullOrEmpty(b.Notes) ? "" : b.Notes
                    }).ToList();
                        return tObj;

                    case "MAINTENANCE PERSON":
                        object tObj1 = DbCo.maintenances.Where(b => b.AssignedTo == employeeId && b.BusinessID == businessId).ToList().Select(b => new
                      Model.Tasks{
                            Type = "MAINTENANCEPERSON",
                            BusinessID = b.BusinessID.ToString(),
                            EmployeeID = b.AssignedTo.ToString(),
                            TaskId = b.MaintenanceID.ToString(),
                            RoomID = b.RoomID.ToString(),
                            RoomNumber = b.room.RoomNumber.ToString(),
                            StatusID = b.MaintenanceStatusID.ToString(),
                            Status = b.maintenancestatu.Status,
                            DutyId = b.MaintenanceDutyID.ToString(),
                            Duty = b.maintenancetype.Type,
                            PriorityID = b.PriorityID.ToString(),
                            Priority = b.priority.Priority1,
                            CreatedAt = b.CreatedAt.ToString("MM/dd/yyyy"),
                            StartTime = ConvertTOAMPM(b.StartTime),
                            EndTime = ConvertTOAMPM(b.EndTime),
                            PauseTime = ConvertTOAMPM(b.PauseTime),
                            ResumeTime = ConvertTOAMPM(b.ResumeTime),
                            TotalTime = b.TotalTime.ToString(),
                            Image1 = string.IsNullOrEmpty(b.Img1Path)?"":b.Img1Path,
                            Image2 = string.IsNullOrEmpty(b.Img2Path) ? "" : b.Img2Path,
                            Image3 = string.IsNullOrEmpty(b.Img3Path) ? "" : b.Img3Path,
                            Notes = string.IsNullOrEmpty(b.Notes) ? "" : b.Notes
                        }).ToList();
                        return tObj1;

                    case "HEAD HOUSEKEEPER" :
                        {
                            List<Model.Tasks> tObj2 = DbCo.roomlists.Where(b => b.HeadHousekeeperID == employeeId && b.BusinessID == businessId).ToList().Where(b =>
                       b.CreatedAt.Year == requestDate.Year && b.CreatedAt.Month == requestDate.Month && b.CreatedAt.Day == requestDate.Day
                        ).Select(b => new Model.Tasks
                        {
                            Type = "HOUSEKEEPER",
                            BusinessID = b.BusinessID.ToString(),
                            EmployeeID = b.EmployeeID.ToString(),
                            TaskId = b.RoomListID.ToString(),
                            RoomID = b.RoomID.ToString(),
                            RoomNumber = b.room.RoomNumber.ToString(),
                            StatusID = b.RoomStatusID.ToString(),
                            Status = b.roomstatu.Status,
                            DutyId = b.RoomDutiesID.ToString(),
                            Duty = b.roomduty.Duty,
                            PriorityID = b.PriorityID.ToString(),
                            Priority = b.priority.Priority1,
                            CreatedAt = b.CreatedAt.ToString("MM/dd/yyyy"),
                            StartTime = ConvertTOAMPM(b.StartTime),
                            EndTime = ConvertTOAMPM(b.EndTime),
                            PauseTime = ConvertTOAMPM(b.PauseTime),
                            ResumeTime = ConvertTOAMPM(b.ResumeTime),
                            TotalTime = b.TotalTime.ToString(),
                            Image1 ="",
                            Image2 ="",
                            Image3 ="",
                            Notes = string.IsNullOrEmpty(b.Notes) ? "" : b.Notes

                        }).ToList<Model.Tasks>();

                            List<Model.Tasks> tObj3 = DbCo.maintenances.Where(b => b.VerifiedBy == employeeId && b.BusinessID == businessId).ToList().Select(b => new
                            Model.Tasks {
                                Type = "MAINTENANCEPERSON",
                                BusinessID = b.BusinessID.ToString(),
                                EmployeeID = b.AssignedTo.ToString(),
                                TaskId = b.MaintenanceID.ToString(),
                                RoomID = b.RoomID.ToString(),
                                RoomNumber = b.room.RoomNumber.ToString(),
                                StatusID = b.MaintenanceStatusID.ToString(),
                                Status = b.maintenancestatu.Status,
                                DutyId = b.MaintenanceDutyID.ToString(),
                                Duty = b.maintenancetype.Type,
                                PriorityID = b.PriorityID.ToString(),
                                Priority = b.priority.Priority1,
                                CreatedAt = b.CreatedAt.ToString("MM/dd/yyyy"),
                                StartTime = ConvertTOAMPM(b.StartTime),
                                EndTime = ConvertTOAMPM(b.EndTime),
                                PauseTime = ConvertTOAMPM(b.PauseTime),
                                ResumeTime = ConvertTOAMPM(b.ResumeTime),
                                TotalTime = b.TotalTime.ToString(),
                                Image1 = string.IsNullOrEmpty(b.Img1Path) ? "" : b.Img1Path,
                                Image2 = string.IsNullOrEmpty(b.Img2Path) ? "" : b.Img2Path,
                                Image3 = string.IsNullOrEmpty(b.Img3Path) ? "" : b.Img3Path,
                                Notes = string.IsNullOrEmpty(b.Notes) ? "" : b.Notes
                            }).ToList<Model.Tasks>();
                            return tObj2.Concat(tObj3);
                        }
                    default:
                        return null;
                }

                
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getTasksDetails");
                return null;
            }
        }

        public int getRoomListStatusIdByStatus(string status)
        {
            try
            {
                return DbCo.roomstatus.Where(s => s.Status.Equals(status)).Select(s=>s.RoomstatusID).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int getMainenanceStatusIdByStatus(string status)
        {
            try
            {
                return DbCo.maintenancestatus.Where(s => s.Status.Equals(status)).Select(s => s.MaintenanceStatusID).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int getMainenanceTypeIdByType(string type)
        {
            try
            {
                return DbCo.maintenancetypes.Where(s => s.Type.Equals(type)).Select(s => s.MaintenanceTypeID).FirstOrDefault<int>();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public bool addMainenanceRequest(int employeeId, int businessId,int roomId,DateTime requestDateTime,string type,string image1,string image2,string image3,string note)
        {
            try
            {
                maintenance addMaintananceObj = new maintenance();
                addMaintananceObj.BusinessID = businessId;
                addMaintananceObj.CreatedAt = requestDateTime;
                addMaintananceObj.AddedBy = employeeId;
                addMaintananceObj.RoomID = roomId;
                addMaintananceObj.MaintenanceStatusID = getMainenanceStatusIdByStatus("OPEN");
                addMaintananceObj.PriorityID = 1;
                if (!string.IsNullOrEmpty(type))
                    addMaintananceObj.MaintenanceDutyID = getMainenanceTypeIdByType(type);
                if (!string.IsNullOrEmpty(image1))
                    addMaintananceObj.Img1Path = image1;
                if (!string.IsNullOrEmpty(image2))
                    addMaintananceObj.Img2Path = image2;
                if (!string.IsNullOrEmpty(image3))
                    addMaintananceObj.Img3Path = image3;
                if (!string.IsNullOrEmpty(note))
                    addMaintananceObj.Notes = note;

                //assign automatic to maintenance person
                int jobid = getJobTitleIdFromJobTitle("MAINTENANCE PERSON");
                if(jobid>0)
                {
                    employee emp= DbCo.employees.Where(e => e.BusinessID == businessId && e.JobTitleID == jobid).FirstOrDefault();
                    if(emp!=null)
                    {
                        addMaintananceObj.AssignedTo = emp.EmployeeID;
                    }
                }


                DbCo.maintenances.Add(addMaintananceObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addMainenanceRequest");
                return false;
            }
        }

        public bool updateTasksDetails(int employeeId, int businessId,int taskid,string type,string status, DateTime requestDate,string note,string img)
        {
            try
            {
                switch (type)
                {
                    case "HOUSEKEEPER":
                        int statusId = 0;
                        roomlist rl = DbCo.roomlists.Where(r => r.RoomListID == taskid).FirstOrDefault<roomlist>();
                        statusId = getRoomListStatusIdByStatus(status);
                        rl.RoomStatusID = statusId;
                        switch (status)
                        {
                            case "IN PROGRESS":
                                
                                if(rl.PauseTime==null)
                                {
                                    rl.StartTime = requestDate.TimeOfDay;
                                }
                                else
                                {
                                    rl.ResumeTime = requestDate.TimeOfDay;
                                }
                                break;
                            case "PAUSED":
                                rl.PauseTime = requestDate.TimeOfDay;
                                if (rl.ResumeTime == null)
                                {
                                    rl.TotalTime = (rl.TotalTime != null ? rl.TotalTime : 0) + ((rl.StartTime != null && rl.PauseTime != null) ? (float)(((TimeSpan)rl.PauseTime).TotalMinutes - ((TimeSpan)rl.StartTime).TotalMinutes) : 0);

                                }
                                else
                                {
                                    rl.TotalTime = (rl.TotalTime != null ? rl.TotalTime : 0) + ((rl.ResumeTime != null && rl.PauseTime != null) ? (float)(((TimeSpan)rl.PauseTime).TotalMinutes - ((TimeSpan)rl.ResumeTime).TotalMinutes) : 0);
                                }
                                rl.ResumeTime = null;
                                break;
                            case "COMPLETED":

                                rl.EndTime = requestDate.TimeOfDay;
                                if(rl.ResumeTime==null && rl.PauseTime!=null)
                                {

                                }
                                else if (rl.ResumeTime == null)
                                {
                                    rl.TotalTime = (rl.TotalTime != null ? rl.TotalTime : 0) + ((rl.StartTime != null && rl.EndTime != null) ? (float)(((TimeSpan)rl.EndTime).TotalMinutes - ((TimeSpan)rl.StartTime).TotalMinutes) : 0);

                                }
                                else
                                {
                                    rl.TotalTime = (rl.TotalTime != null ? rl.TotalTime : 0) + ((rl.ResumeTime != null && rl.EndTime != null) ? (float)(((TimeSpan)rl.EndTime).TotalMinutes - ((TimeSpan)rl.ResumeTime).TotalMinutes) : 0);
                                }
                                break;
                            default:
                                break;
                        }
                        DbCo.Entry(rl).State = System.Data.Entity.EntityState.Modified;
                        DbCo.SaveChanges();
                        return true;
                    case "MAINTENANCEPERSON":
                        //int mStatusId = 0;
                        maintenance mlst = DbCo.maintenances.Where(r => r.MaintenanceID == taskid).FirstOrDefault<maintenance>();
                        statusId = getMainenanceStatusIdByStatus(status);
                        mlst.MaintenanceStatusID = statusId;
                        switch (status)
                        {
                            case "IN PROGRESS":

                                if (mlst.PauseTime == null)
                                {
                                    mlst.StartTime = requestDate;
                                }
                                else
                                {
                                    mlst.ResumeTime = requestDate;
                                }
                                break;
                            case "PAUSED":

                                mlst.PauseTime = requestDate;
                                if (mlst.ResumeTime == null)
                                {
                                    mlst.TotalTime = (mlst.TotalTime != null ? mlst.TotalTime : 0) + ((mlst.StartTime != null && mlst.PauseTime != null) ? (float)(((DateTime)mlst.PauseTime).TimeOfDay.TotalMinutes - ((DateTime)mlst.StartTime).TimeOfDay.TotalMinutes) : 0);

                                }
                                else
                                {
                                    mlst.TotalTime = (mlst.TotalTime != null ? mlst.TotalTime : 0) + ((mlst.ResumeTime != null && mlst.PauseTime != null) ? (float)(((DateTime)mlst.PauseTime).TimeOfDay.TotalMinutes - ((DateTime)mlst.ResumeTime).TimeOfDay.TotalMinutes) : 0);
                                }
                                mlst.ResumeTime = null;
                                break;
                            case "FIXED":
                                if (!string.IsNullOrEmpty(note))
                                    mlst.Notes = note;
                                if (!string.IsNullOrEmpty(img))
                                    mlst.Img4Path = img;
                                mlst.EndTime = requestDate;
                                if (mlst.ResumeTime == null && mlst.PauseTime != null)
                                {

                                }
                                if (mlst.ResumeTime == null)
                                {
                                    mlst.TotalTime = (mlst.TotalTime != null ? mlst.TotalTime : 0) + ((mlst.StartTime != null && mlst.EndTime != null) ? (float)(((DateTime)mlst.EndTime).TimeOfDay.TotalMinutes - ((DateTime)mlst.StartTime).TimeOfDay.TotalMinutes) : 0);

                                }
                                else
                                {
                                    mlst.TotalTime = (mlst.TotalTime != null ? mlst.TotalTime : 0) + ((mlst.ResumeTime != null && mlst.EndTime != null) ? (float)(((DateTime)mlst.EndTime).TimeOfDay.TotalMinutes - ((DateTime)mlst.ResumeTime).TimeOfDay.TotalMinutes) : 0);
                                }
                                break;
                            case "NOT FIXED":
                                if (!string.IsNullOrEmpty(note))
                                    mlst.Notes = note;
                                if (!string.IsNullOrEmpty(img))
                                    mlst.Img4Path = img;
                                mlst.EndTime = requestDate;
                                if (mlst.ResumeTime == null && mlst.PauseTime != null)
                                {

                                }
                                if (mlst.ResumeTime == null)
                                {
                                    mlst.TotalTime = (mlst.TotalTime != null ? mlst.TotalTime : 0) + ((mlst.StartTime != null && mlst.EndTime != null) ? (float)(((DateTime)mlst.EndTime).TimeOfDay.TotalMinutes - ((DateTime)mlst.StartTime).TimeOfDay.TotalMinutes) : 0);

                                }
                                else
                                {
                                    mlst.TotalTime = (mlst.TotalTime != null ? mlst.TotalTime : 0) + ((mlst.ResumeTime != null && mlst.EndTime != null) ? (float)(((DateTime)mlst.EndTime).TimeOfDay.TotalMinutes - ((DateTime)mlst.ResumeTime).TimeOfDay.TotalMinutes) : 0);
                                }
                                break;
                            default:
                                break;
                        }
                        DbCo.Entry(mlst).State = System.Data.Entity.EntityState.Modified;
                        DbCo.SaveChanges();
                        return true;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "updateTasksDetails");
                return false;
            }
            return false;
        }


        public object getLaundryDetails(int employeeId, int businessId, DateTime requestDate)
        {
            string abc = "";
            try
            {
                object tObj = DbCo.laundries.Where(b => b.AssignedTo == employeeId && b.BusinessID == businessId && b.CreatedAt.Year == requestDate.Year && b.CreatedAt.Month == requestDate.Month && b.CreatedAt.Day == requestDate.Day).ToList().Select(b => new
                {
                    b.AddedBy,
                    b.AssignedTo,
                    b.CreatedAt,
                    b.Description,
                    b.LaundryId,
                    b.LaundryStatusID,
                    Status = b.laundrystatu.Status,
                    LunchInTime = ConvertTOAMPM(b.StartTime),
                    LunchOutTime = ConvertTOAMPM(b.EndTime),
                    TotalTime=b.TotalTime != null ? b.TotalTime : 0,
                }).ToList();
                return tObj;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getTimeCardDetails");
                return null;
            }
        }

        public int getLaundryStatusFromStatus(string st)
        {
            return DbCo.laundrystatus.Where(l => l.Status.Equals(st)).Select(s => s.LaundryStatusID).FirstOrDefault<int>();
        }

        public bool updateLaundryDetails(int ldId,string status, DateTime requestTime)
        {
            try
            {
                laundry ld = DbCo.laundries.Where(l => l.LaundryId == ldId).FirstOrDefault<laundry>();
                if(ld!=null)
                {
                    if(status.Equals("IN PROGRESS"))
                    {
                        ld.LaundryStatusID =getLaundryStatusFromStatus("IN PROGRESS");
                        ld.StartTime = requestTime;
                        DbCo.Entry(ld).State = EntityState.Modified;
                        DbCo.SaveChanges();
                        return true;
                    }
                    else if(status.Equals("COMPLETED"))
                    {
                        ld.LaundryStatusID = getLaundryStatusFromStatus("COMPLETED");
                        ld.EndTime = requestTime;
                        ld.TotalTime = (ld.TotalTime != null ? ld.TotalTime : 0) + ((ld.StartTime != null && ld.EndTime != null) ? (float)(((DateTime)ld.EndTime).TimeOfDay.TotalMinutes - ((DateTime)ld.StartTime).TimeOfDay.TotalMinutes) : 0);
                        DbCo.Entry(ld).State = EntityState.Modified;
                        DbCo.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "updateLaundryDetails");
                return false;
            }
        }


        public object getScheduleDetails(int employeeId, int businessId,int  requestMonth,int requestYear)
        {
            try
            {
                object tObj = DbCo.schedules.Where(b => b.BusinessID == businessId && b.EmployeeID == employeeId 
                && b.ScheduleDate.Year == requestYear && b.ScheduleDate.Month == requestMonth).ToList().Select(b => new
                {
                    b.ScheduleID,
                    ScheduleDate=ConvertTOAMPM( b.ScheduleDate),
                    CreatedAt=ConvertTOAMPM( b.CreatedAt),
                    StartTime=ConvertTOAMPM(b.StartTime),
                    EndTime=ConvertTOAMPM(b.EndTime),
                    b.TotalScheduledHours,
                    b.Notes,
                }).ToList();
                return tObj;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "getScheduleDetails");
                return null;
            }
        }

        public object getAllmessageAPI(int businessId, int empid,int msgEmpId)
        {
            try
            {
                object lstMEssage = DbCo.allmessages.Where(s => s.BusinessID == businessId && ((s.MessageSentByID == empid && s.MessageRecievedByID == msgEmpId)|| (s.MessageSentByID == msgEmpId && s.MessageRecievedByID == empid))).
                    OrderBy(s => s.CreatedAt).ToList().Select(b => new
                    {
                        b.AllmessageID,
                        CreatedAt = string.Format("{0:MMMM dd hh:mm tt}", b.CreatedAt),
                        MessageSentBy = b.employee1.Name,
                        MessageSentByID = b.MessageSentByID,
                        MessageSentByImage = b.employee1.ImagePath,
                        MessageRecievedBy = b.employee.Name,
                        MessageRecievedByID = b.MessageRecievedByID,
                        messageText = b.messageText
                    }).ToList();
                List<allmessage> lstUnreadMEssage = DbCo.allmessages.Where(s => s.BusinessID == businessId && s.isRead == 0 && s.MessageSentByID == msgEmpId && s.MessageRecievedByID == empid).ToList<allmessage>();
                foreach (var item in lstUnreadMEssage)
                {
                    item.isRead = 1;
                    DbCo.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }
                DbCo.SaveChanges();
                return lstMEssage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public object getAllHKMPEmployee(int businessId)
        {
            try
            {
                return getAllHousekeeprOfBusiness(businessId).ToList().Select(b => new
                {
                    b.EmployeeID,
                    b.Name
                }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public object getInspectionAssignedRooms(int businessId)
        {
            try
            {
                object tObj = DbCo.rooms.Where(b =>  b.BusinessID == businessId).ToList().Select(r => new
                    {
                        r.RoomID,
                        r.RoomNumber
                    }).ToList();
                return tObj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public object getAllDutiesForAPI()
        {
            try
            {
                object tObj = DbCo.roomduties.ToList().Select(r => new
                {
                    r.RoomDutyID,
                    r.Duty
                }).ToList();
                return tObj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool addInspection(int employeeId,int businessId,int selectedEmployeeId,string inspectedByName, DateTime requestDate,int roomId,int roomDutyId,float result,string comment,string signPath)
        {
            try
            {
                inspection addInspectionObj = new inspection();
                addInspectionObj.BusinessID = businessId;
                addInspectionObj.CreatedAt = requestDate;
                addInspectionObj.InspectedBy = employeeId;
                addInspectionObj.RoomID = roomId;
                addInspectionObj.RoomDutiesID = roomDutyId;
                addInspectionObj.Result = result;
                addInspectionObj.InspectedToID = selectedEmployeeId;
                if (!string.IsNullOrEmpty(comment))
                    addInspectionObj.comments = comment;
                if (!string.IsNullOrEmpty(signPath))
                    addInspectionObj.ImgPath = signPath;
                if (!string.IsNullOrEmpty(inspectedByName))
                    addInspectionObj.Name = inspectedByName;
                DbCo.inspections.Add(addInspectionObj);
                DbCo.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ExeptionLoginning.SendErrorToText(ex, "addInspection");
                return false;
            }
        }

        public guestbusiness GuestLogin(string token)
        {
            try
            {
                return DbCo.guestbusinesses.Where(b => b.Token.Equals(token) && b.business.Status.Equals("Active") && b.isActive.Equals("Active") && b.isDeleted==0).FirstOrDefault();                
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool isGuestActive(int id,int businessId)
        {
            try
            {
                bool status= DbCo.guestbusinesses.Where(b => b.GuestBusinessID==id && b.business.Status.Equals("Active") && b.isActive.Equals("Active") && b.isDeleted == 0).FirstOrDefault()!=null;
                return status;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public object getAllGuestmessageAPI(int businessId, int bguestid)
        {
            try
            {
                object lstMEssage = DbCo.guestmessages.Where(s => s.BusinessID == businessId && s.MessageSentByID == bguestid).
                    OrderBy(s => s.CreatedAt).ToList().Select(b => new
                    {
                        b.guestmessageID,
                        CreatedAt = string.Format("{0:MMMM dd hh:mm tt}", b.CreatedAt),
                        MessageSentByID = b.MessageSentByID,
                        messageText = b.messageText,
                        messageTtype=b.messageTtype,
                    }).ToList();
                //List<allmessage> lstUnreadMEssage = DbCo.allmessages.Where(s => s.BusinessID == businessId && s.isRead == 0 && s.MessageSentByID == msgEmpId && s.MessageRecievedByID == empid).ToList<allmessage>();
                //foreach (var item in lstUnreadMEssage)
                //{
                //    item.isRead = 1;
                //    DbCo.Entry(item).State = System.Data.Entity.EntityState.Modified;
                //}
                //DbCo.SaveChanges();
                return lstMEssage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public object getGuestHistory(int guestID)
        {
            try
            {
                object lstMEssage = DbCo.guestbusinesses.Where(s => s.GuestID == guestID).
                    OrderBy(s => s.StayDateStart).ToList().Select(b => new
                    {
                        b.GuestBusinessID,
                        b.GuestID,
                        StayDateStart = b.StayDateStart != null ? ((DateTime)b.StayDateStart).ToString("MM/dd/yyyy") : "",
                        StayDateEnd = b.StayDateEnd != null ? ((DateTime)b.StayDateEnd).ToString("MM/dd/yyyy") : "",
                        RoomNumber = b.room.RoomNumber,
                        BusinessName = b.business.BusinessName,
                        City = b.business.City,
                        BusinessID = b.BusinessID,
                        BusinessPhone=b.business.BusinessPhone,
                        BusinessEmail=b.business.BusinessEmail
                    }).ToList();
                //List<allmessage> lstUnreadMEssage = DbCo.allmessages.Where(s => s.BusinessID == businessId && s.isRead == 0 && s.MessageSentByID == msgEmpId && s.MessageRecievedByID == empid).ToList<allmessage>();
                //foreach (var item in lstUnreadMEssage)
                //{
                //    item.isRead = 1;
                //    DbCo.Entry(item).State = System.Data.Entity.EntityState.Modified;
                //}
                //DbCo.SaveChanges();
                return lstMEssage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public object getAllAmentiesAPI(int businessID)
        {
            object tObj = DbCo.amenties.Where(l => l.BusinessID == businessID).ToList().Select(r => new
            {
                r.AmentiesID,
                r.AmentiesText,
                r.BusinessID
            }).ToList();
            return tObj;
        }

        public bool updateGuestProfile(int guestId, string Name, string Email, string Phone)
        {
            try
            {
                guest emp = DbCo.guests.Where(e => e.GuestID == guestId).FirstOrDefault<guest>();
                if (emp != null)
                {
                    if (!string.IsNullOrEmpty(Name))
                        emp.Name = Name;
                    if (!string.IsNullOrEmpty(Email))
                        emp.Email = Email;
                    if (!string.IsNullOrEmpty(Phone))
                        emp.phone = Phone;
                    DbCo.Entry(emp).State = EntityState.Modified;
                    DbCo.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}