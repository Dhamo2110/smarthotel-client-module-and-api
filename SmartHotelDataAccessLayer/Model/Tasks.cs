﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHotelDataAccessLayer.Model
{
    public class Tasks
    {
        public String Type { get; set; }
        public String BusinessID { get; set; }
        public String EmployeeID { get; set; }
        public String TaskId { get; set; }
        public String RoomID { get; set; }
        public String RoomNumber { get; set; }
        public String StatusID { get; set; }
        public String Status { get; set; }
        public String DutyId { get; set; }
        public String Duty { get; set; }
        public String PriorityID { get; set; }
        public String Priority { get; set; }
        public String CreatedAt { get; set; }
        public String StartTime { get; set; }
        public String EndTime { get; set; }
        public String PauseTime { get; set; }
        public String ResumeTime { get; set; }
        public String TotalTime { get; set; }
        public String Image1 { get; set; }
        public String Image2 { get; set; }
        public String Image3 { get; set; }
        public String Notes { get; set; }

    }
}
