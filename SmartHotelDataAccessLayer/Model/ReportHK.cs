﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHotelDataAccessLayer.Model
{
    public class ReportHK
    {
        public int employeeId { get; set; }
        public string Name { get; set; }
        public int avgTimePerRoom { get; set; }
        public int avgRoomPerDay { get; set; }
        public int avgTimePerRoomWeekly { get; set; }
        public int avgRoomPerDayWeekly { get; set; }
        public int avgTimePerRoomMonthly { get; set; }
        public int avgRoomPerDayMonthly { get; set; }
    }
}
