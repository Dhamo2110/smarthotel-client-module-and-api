//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartHotelDataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class payment
    {
        public int PaymentID { get; set; }
        public int BusinessID { get; set; }
        public double Amount { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public string TransactionId { get; set; }
        public string State { get; set; }
        public string LApdfLink { get; set; }
    
        public virtual business business { get; set; }
    }
}
